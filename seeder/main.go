package main

import (
	"context"
	"encoding/json"
	"flag"
	"fmt"
	"math"
	"net/http/httputil"
	"time"

	"golang.org/x/exp/slog"

	randomdata "github.com/Pallinder/go-randomdata"
	"github.com/oapi-codegen/runtime/types"

	bvv "gitlab.com/digilab.overheid.nl/ecosystem/migratieketen/fictief-bvv-api"
	sigma "gitlab.com/digilab.overheid.nl/ecosystem/migratieketen/fictief-sigma-api"
)

type sliceStrFlag []string

func (i *sliceStrFlag) String() string {
	return "my string representation"
}

func (i *sliceStrFlag) Set(value string) error {
	*i = append(*i, value)
	return nil
}

func main() {
	bvvServer := flag.String("bvv-server", "http://np-bvv-backend-127.0.0.1.nip.io:8080/v0", "bvv endpoint to send seed data to")
	bvvSeedCount := flag.Int("bvv-seed-count", 100, "amount of vreemdelingen to create")
	sigmaSeedCount := flag.Int("sigma-seed-count", 5, "maximum amount of attributes to create per vreemdeling")

	var sigmaServers sliceStrFlag
	flag.Var(&sigmaServers, "sigma-servers", "sigma endpoint to send seed data to")
	flag.Parse()

	cfg, err := NewConfig("./config/global.yaml")
	if err != nil {
		panic(err)
	}

	bvvClient, err := bvv.NewClient(*bvvServer)
	if err != nil {
		panic(err)
	}

	sigmaClients := []*sigma.Client{}
	for _, server := range sigmaServers {
		sigmaClient, err := sigma.NewClient(server)
		if err != nil {
			panic(err)
		}

		sigmaClients = append(sigmaClients, sigmaClient)
	}

	for i := 1; i <= *bvvSeedCount; i++ {
		if err := func() error {
			id, err := createVreemdeling(bvvClient)
			if err != nil {
				return fmt.Errorf("Vreemdeling creation failed: %w", err)
			}

			for j := 0; j <= randomdata.Number(*sigmaSeedCount); j++ {
				if err := createAttribute(sigmaClients, cfg, id); err != nil {
					return fmt.Errorf("Attribute creation failed: %w", err)
				}
			}

			return nil
		}(); err != nil {
			slog.Error("Seed round failed", "err", err)
		}
	}
}

func createVreemdeling(client *bvv.Client) (string, error) {
	date := randomdata.FullDateInRange("1920-01-01", "2023-12-31")
	birthDate, err := time.Parse("Monday 2 Jan 2006", date)
	if err != nil {
		return "", err
	}

	body := bvv.CreateVreemdelingJSONBody{
		Data: bvv.VreemdelingWithoutId{
			Geboortedatum: &types.Date{
				Time: birthDate,
			},
			Naam: randomdata.FullName(randomdata.RandomGender),
		},
	}

	resp, err := client.CreateVreemdeling(context.Background(), bvv.CreateVreemdelingJSONRequestBody(body))
	if err != nil {
		return "", err
	}

	if resp.StatusCode != 201 {
		return "", fmt.Errorf("failed to create vreemdeling %v %v", resp.StatusCode, resp.Status)
	}

	var model bvv.VreemdelingResponse
	if err := json.NewDecoder(resp.Body).Decode(&model); err != nil {
		return "", err
	}

	slog.Info("Created vreemdeling", "data", model)

	return model.Data.Id, nil
}

func createAttribute(clients []*sigma.Client, cfg *Config, id string) error {
	client := clients[randomdata.Number(len(clients))]

	attribute := getRandAttribute(cfg)
	data, err := getRandAttributeData(attribute)
	if err != nil {
		return err
	}

	body := sigma.CreateObservationByVreemdelingenIdJSONRequestBody{
		Data: sigma.ObservationCreate{
			Attribute: attribute.Name,
			Value:     *data,
		},
	}

	resp, err := client.CreateObservationByVreemdelingenId(context.Background(), id, body)
	if err != nil {
		return err
	}

	if resp.StatusCode != 201 {
		r, _ := httputil.DumpResponse(resp, true)
		return fmt.Errorf("failed to create attribute %d %s\n%s\n%+v", resp.StatusCode, resp.Status, r, body)
	}

	var model sigma.ObservationCreateResponse
	if err := json.NewDecoder(resp.Body).Decode(&model); err != nil {
		return err
	}

	slog.Info("Created observation", "data", model.Data)
	return nil
}

func getRandAttribute(cfg *Config) Attribute {
	return cfg.Rubrieken[randomdata.Number(len(cfg.Rubrieken))]
}

func getRandAttributeData(attribute Attribute) (*sigma.ObservationCreate_Value, error) {
	var err error
	var a sigma.ObservationCreate_Value

	switch attribute.Type {
	case "text":
		err = a.FromObservationCreateValue1(randomdata.Paragraph())
	case "checkboxes":
		a, err = getRandAttributeDataMultiple(attribute)
	case "datetime":
		a, err = getRandAttributeDataTimedate(attribute)
	case "number":
		a, err = getRandAttributeNumber(attribute)
	case "boolean":
		err = a.FromObservationCreateValue2(randomdata.Boolean())
	case "textarea":
		err = a.FromObservationCreateValue1(randomdata.Paragraph())
	case "radios":
		a, err = getRandAttributeDataSingle(attribute)
	case "select":
		if attribute.Multiple {
			a, err = getRandAttributeDataMultiple(attribute)
		} else {
			a, err = getRandAttributeDataSingle(attribute)
		}
	default:
		return nil, fmt.Errorf("attribute type unknown: %s", attribute.Type)
	}

	return &a, err
}

func getRandAttributeDataSingle(attribute Attribute) (sigma.ObservationCreate_Value, error) {
	index := randomdata.Number(len(attribute.Options))

	value := attribute.Options[index].Name

	var a sigma.ObservationCreate_Value
	err := a.FromObservationCreateValue3([]interface{}{value})

	return a, err
}

func getRandAttributeDataMultiple(attribute Attribute) (sigma.ObservationCreate_Value, error) {
	amount := randomdata.Number(int(math.Min(float64(len(attribute.Options)), 5)))

	options := attribute.Options
	value := make([]interface{}, 0, amount)

	for i := 0; i <= int(amount); i++ {
		index := randomdata.Number(len(options))
		option := options[index]
		value = append(value, option.Name)

		options = remove(options, index)
	}

	var a sigma.ObservationCreate_Value
	err := a.FromObservationCreateValue3(value)

	return a, err
}

func getRandAttributeNumber(attribute Attribute) (sigma.ObservationCreate_Value, error) {
	min := math.MinInt
	max := math.MaxInt

	if attribute.Min != nil {
		min = (*attribute.Min).(int)
	}

	if attribute.Max != nil {
		max = (*attribute.Max).(int)
	}

	var a sigma.ObservationCreate_Value
	err := a.FromObservationCreateValue4(float32(randomdata.Number(min, max)))

	return a, err
}

func getRandAttributeDataTimedate(attribute Attribute) (sigma.ObservationCreate_Value, error) {
	var a sigma.ObservationCreate_Value
	date := randomdata.FullDate()
	d, err := time.Parse("Monday 2 Jan 2006", date)
	if err != nil {
		return a, err
	}

	err = a.FromObservationCreateValue1(d.Format(time.RFC3339))
	return a, err
}

func remove[T any](s []T, i int) []T {
	s[i] = s[len(s)-1]
	return s[:len(s)-1]
}
