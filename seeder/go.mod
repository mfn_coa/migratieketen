module gitlab.com/digilab.overheid.nl/ecosystem/migratieketen/seeder

go 1.21.5

replace gitlab.com/digilab.overheid.nl/ecosystem/migratieketen/fictief-bvv-api => ../fictief-bvv-api

replace gitlab.com/digilab.overheid.nl/ecosystem/migratieketen/fictief-bvv-sigma => ../fictief-bvv-sigma

require (
	github.com/Pallinder/go-randomdata v1.2.0
	github.com/iamolegga/enviper v1.4.2
	github.com/oapi-codegen/runtime v1.1.0
	github.com/spf13/viper v1.18.2
	gitlab.com/digilab.overheid.nl/ecosystem/migratieketen/fictief-bvv-api v0.0.0-20240103140739-0a8c697ef459
	gitlab.com/digilab.overheid.nl/ecosystem/migratieketen/fictief-sigma-api v0.0.0-20240103140739-0a8c697ef459
	golang.org/x/exp v0.0.0-20231226003508-02704c960a9b
	gopkg.in/yaml.v3 v3.0.1
)

require (
	github.com/apapsch/go-jsonmerge/v2 v2.0.0 // indirect
	github.com/fsnotify/fsnotify v1.7.0 // indirect
	github.com/getkin/kin-openapi v0.122.0 // indirect
	github.com/go-chi/chi/v5 v5.0.11 // indirect
	github.com/go-openapi/jsonpointer v0.20.2 // indirect
	github.com/go-openapi/swag v0.22.7 // indirect
	github.com/google/uuid v1.5.0 // indirect
	github.com/hashicorp/hcl v1.0.0 // indirect
	github.com/invopop/yaml v0.2.0 // indirect
	github.com/josharian/intern v1.0.0 // indirect
	github.com/magiconair/properties v1.8.7 // indirect
	github.com/mailru/easyjson v0.7.7 // indirect
	github.com/mitchellh/mapstructure v1.5.0 // indirect
	github.com/mohae/deepcopy v0.0.0-20170929034955-c48cc78d4826 // indirect
	github.com/pelletier/go-toml/v2 v2.1.1 // indirect
	github.com/perimeterx/marshmallow v1.1.5 // indirect
	github.com/sagikazarmark/locafero v0.4.0 // indirect
	github.com/sagikazarmark/slog-shim v0.1.0 // indirect
	github.com/sourcegraph/conc v0.3.0 // indirect
	github.com/spf13/afero v1.11.0 // indirect
	github.com/spf13/cast v1.6.0 // indirect
	github.com/spf13/pflag v1.0.5 // indirect
	github.com/subosito/gotenv v1.6.0 // indirect
	go.uber.org/multierr v1.11.0 // indirect
	golang.org/x/sys v0.15.0 // indirect
	golang.org/x/text v0.14.0 // indirect
	gopkg.in/ini.v1 v1.67.0 // indirect
)
