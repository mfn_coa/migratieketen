package main

import (
	"log/slog"
)

type NotificationServer struct {
	logger          *slog.Logger
	natsClient      *NatsClient
	websocketServer *WebsocketServer
	shutdownChannel chan bool
}

func NewNotificationServer(config *Config, logger slog.Logger) (*NotificationServer, error) {
	// Setup nats
	natsClient, err := NewNatsClient(config, logger)
	if err != nil {
		return nil, err
	}

	// Setup websockets
	websocketServer := NewWebsocketServer(logger, config)

	// Setup notification server
	notificationServer := NotificationServer{
		logger:          &logger,
		natsClient:      natsClient,
		websocketServer: websocketServer,
		shutdownChannel: make(chan bool),
	}

	return &notificationServer, nil
}

func (n NotificationServer) Run() error {

	err := n.natsClient.Run()
	if err != nil {
		return err
	}

	n.websocketServer.Run()

	go n.forwardMessages()

	return nil
}

func (n NotificationServer) forwardMessages() {
BackgroundLoop:
	for {
		select {
		case <-n.shutdownChannel:
			break BackgroundLoop
		case natsNotification := <-n.natsClient.notificationChannel:
			n.websocketServer.PushNotification(natsNotification)
		case postNotification := <-n.websocketServer.notificationChannel:
			err := n.natsClient.PublishNotification(postNotification)
			if err != nil {
				n.logger.Error("failed to post notification", "err", err)
			}
		}
	}
}

func (n NotificationServer) Shutdown() error {
	n.shutdownChannel <- true
	err := n.natsClient.Shutdown()
	if err != nil {
		return err
	}

	err = n.websocketServer.Shutdown()
	if err != nil {
		return err
	}

	return nil
}
