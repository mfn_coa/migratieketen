package main

import (
	"log"
	"log/slog"
	"os"

	env "github.com/caarlos0/env/v10"
)

type Config struct {
	ServerAddress string `env:"MK_ADDRESS" envDefault:":8081"`
	NatsAddress   string `env:"MK_NATS_ADDRESS" envDefault:"ws://np-notifications-nats:8080"`
}

func main() {
	log.SetFlags(0)

	err := run()
	if err != nil {
		log.Fatal(err)
	}
}

func run() error {
	logger := slog.New(slog.NewJSONHandler(os.Stdout, &slog.HandlerOptions{})).With("application", "notification_backend")
	logger.Info("starting up")

	config := Config{}
	if err := env.Parse(&config); err != nil {
		logger.Error("Failed to read config %+v\n", err)
		return err
	}

	proc := NewProcess()

	notificationServer, err := NewNotificationServer(&config, *logger)
	if err != nil {
		return err
	}

	err = notificationServer.Run()
	if err != nil {
		return err
	}

	proc.Wait()

	return notificationServer.Shutdown()
}
