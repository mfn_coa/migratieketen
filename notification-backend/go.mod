module gitlab.com/digilab.overheid.nl/ecosystem/migratieketen/notification-backend

go 1.21.5

replace gitlab.com/digilab.overheid.nl/ecosystem/migratieketen/notification-api => ../notification-api

require (
	github.com/caarlos0/env/v10 v10.0.0
	github.com/go-chi/chi v1.5.5
	github.com/go-chi/chi/v5 v5.0.11
	github.com/nats-io/nats.go v1.31.0
	gitlab.com/digilab.overheid.nl/ecosystem/migratieketen/notification-api v0.0.0-20240103123649-3afe69c75b47
	nhooyr.io/websocket v1.8.10
)

require (
	github.com/getkin/kin-openapi v0.122.0 // indirect
	github.com/go-openapi/jsonpointer v0.20.2 // indirect
	github.com/go-openapi/swag v0.22.7 // indirect
	github.com/invopop/yaml v0.2.0 // indirect
	github.com/josharian/intern v1.0.0 // indirect
	github.com/klauspost/compress v1.17.4 // indirect
	github.com/mailru/easyjson v0.7.7 // indirect
	github.com/mohae/deepcopy v0.0.0-20170929034955-c48cc78d4826 // indirect
	github.com/nats-io/nkeys v0.4.7 // indirect
	github.com/nats-io/nuid v1.0.1 // indirect
	github.com/perimeterx/marshmallow v1.1.5 // indirect
	golang.org/x/crypto v0.17.0 // indirect
	golang.org/x/sys v0.15.0 // indirect
	gopkg.in/yaml.v3 v3.0.1 // indirect
)
