package main

import (
	"log/slog"

	"github.com/nats-io/nats.go"
)

type NatsClient struct {
	natsConnection      *nats.Conn
	natsSubscription    *nats.Subscription
	notificationChannel chan string
	natsChannel         chan *nats.Msg
	shutdownChannel     chan bool
	logger              slog.Logger
}

func NewNatsClient(config *Config, logger slog.Logger) (*NatsClient, error) {
	logger.Info("connecting to nats", "addr", config.NatsAddress)
	natsConnection, _ := nats.Connect(config.NatsAddress)

	natsChannel := make(chan *nats.Msg)
	natsSubscription, err := natsConnection.ChanSubscribe("notifications", natsChannel)
	if err != nil {
		return nil, err
	}

	client := NatsClient{
		natsConnection:      natsConnection,
		natsSubscription:    natsSubscription,
		notificationChannel: make(chan string),
		shutdownChannel:     make(chan bool),
		natsChannel:         natsChannel,
		logger:              logger,
	}

	return &client, nil
}

func (client *NatsClient) Run() error {
	go client.runClientChannel()
	return nil
}

func (client *NatsClient) PublishNotification(data []byte) error {
	msg := nats.NewMsg("notifications")
	msg.Data = data
	err := client.natsConnection.PublishMsg(msg)
	return err
}

func (client *NatsClient) runClientChannel() {
BackgroundLoop:
	for {
		select {
		case <-client.shutdownChannel:
			break BackgroundLoop
		case data := <-client.natsChannel:
			msg := string(data.Data)
			client.logger.Info("nats client channel got msg", "msg", msg)
			client.notificationChannel <- msg
		}
	}

}

func (n *NatsClient) Shutdown() error {
	n.natsConnection.Close()
	n.shutdownChannel <- true
	return nil
}
