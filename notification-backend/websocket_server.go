package main

import (
	"context"
	"encoding/json"
	"io"
	"log/slog"
	"net/http"
	"sync"

	"github.com/go-chi/chi/middleware"
	"github.com/go-chi/chi/v5"
	"nhooyr.io/websocket"

	api "gitlab.com/digilab.overheid.nl/ecosystem/migratieketen/notification-api"
)

type WebsocketServer struct {
	http.Server
	sockets             sync.Map
	logger              slog.Logger
	notificationChannel chan []byte
	shutdownChannel     chan bool
}

func NewWebsocketServer(logger slog.Logger, config *Config) *WebsocketServer {
	logger.Info("Starting server on", "addr", config.ServerAddress)

	router := chi.NewRouter()

	router.Use(middleware.RequestID)
	router.Use(middleware.RealIP)
	router.Use(middleware.Logger)

	ws := WebsocketServer{
		Server: http.Server{
			Addr: config.ServerAddress,
		},
		sockets:             sync.Map{},
		logger:              logger,
		notificationChannel: make(chan []byte),
		shutdownChannel:     make(chan bool),
	}

	router.Mount("/api/v0", api.Handler(&ws))

	ws.Server.Handler = router
	return &ws
}

func (s *WebsocketServer) Run() {
	go func() {
		err := s.ListenAndServe()
		s.logger.Error("server encountered an error", "err", err)
	}()

	go s.readMessages()
}

// PublishNotification implements api.ServerInterface.
func (s *WebsocketServer) PublishNotification(w http.ResponseWriter, r *http.Request) {
	// Read and parse request
	data, err := io.ReadAll(r.Body)
	if err != nil {
		s.SendErrorResponse(w, err, "could not read notification request", 400)
		return
	}
	r.Body.Close()

	notification := &api.NotificationPublishRequest{}
	err = json.Unmarshal(data, &notification)
	if err != nil {
		s.SendErrorResponse(w, err, "could not parse notification request", 400)
		return
	}

	notificationData, err := json.Marshal(notification.Data)
	if err != nil {
		s.SendErrorResponse(w, err, "could not encode notification data", 400)
		return
	}

	// Publish notification
	s.notificationChannel <- notificationData

	// Respond
	err = json.NewEncoder(w).Encode(api.EmptyResponse{})
	if err != nil {
		s.SendErrorResponse(w, err, "could not encode response", 500)
		return
	}
}

// SubscribeNotification implements api.ServerInterface.
func (s *WebsocketServer) SubscribeNotification(w http.ResponseWriter, r *http.Request) {
	s.logger.Info("Recieved websocket connection")

	// TODO: Remove insecure
	connection, err := websocket.Accept(w, r, &websocket.AcceptOptions{
		InsecureSkipVerify: true,
	})

	if err != nil {
		s.SendErrorResponse(w, err, "could not setup websocket connection", 500)
		return
	}

	s.sockets.Store(r.RemoteAddr, connection)
}

func (s *WebsocketServer) SendErrorResponse(w http.ResponseWriter, err error, message string, code int) {
	s.logger.Error(message, "err", err)

	w.WriteHeader(code)
	_, err = w.Write([]byte(err.Error()))
	if err != nil {
		s.logger.Error("Failed to send error response", "err", err)
	}
}

func (s *WebsocketServer) PushNotification(notification string) {
	s.sockets.Range(func(key, value any) bool {
		connection := value.(*websocket.Conn)
		err := connection.Write(context.Background(), websocket.MessageText, []byte(notification))
		if err != nil {
			s.logger.Error("failed to write", "err", err, "dest", key)
			s.sockets.Delete(key)
		}
		return true
	})
}

func (s *WebsocketServer) readMessages() {
BackgroundLoop:
	for {
		select {
		case <-s.shutdownChannel:
			break BackgroundLoop
		default:
			s.sockets.Range(func(key, value any) bool {
				connection := value.(*websocket.Conn)
				// TODO: This blocks, move to go func or timeout?
				_, msgData, err := connection.Read(context.Background())
				if err != nil {
					s.logger.Error("failed to read", "err", err, "key", key)
					s.sockets.Delete(key)
				}
				s.logger.Warn("Got websocket message, discarding", "source", key, "msg", string(msgData))
				return true
			})
		}
	}

}

func (s *WebsocketServer) Shutdown() error {
	s.shutdownChannel <- true

	err := s.Server.Shutdown(context.Background())
	if err != nil {
		return err
	}

	s.sockets.Range(func(key, value any) bool {
		connection := value.(*websocket.Conn)
		err := connection.Close(http.StatusGone, "shutting down")
		if err != nil {
			s.logger.Error("failed to close socket", "err", err, "dest", key)
			s.sockets.Delete(key)
		}
		return true
	})

	return nil
}
