-- name: VreemdelingCreate :exec
INSERT INTO bvv_backend.vreemdeling (
    id, vreemdelingnummer, name, searchname, birthdate
)
VALUES ($1, $2, $3, $4, $5);

-- name: VreemdelingUpdateById :exec
UPDATE bvv_backend.vreemdeling
SET vreemdelingnummer = $2, name = $3, searchname = $4, birthdate = $5
WHERE id = $1;

-- name: VreemdelingUpdateByNummer :exec
UPDATE bvv_backend.vreemdeling
SET vreemdelingnummer = $2, name = $3, searchname = $4, birthdate = $5
WHERE vreemdelingnummer = $1;

-- name: VreemdelingDeleteById :exec
UPDATE bvv_backend.vreemdeling
SET deleted_at = NOW()
WHERE id = $1;

-- name: VreemdelingDeleteByNummer :exec
UPDATE bvv_backend.vreemdeling
SET deleted_at = NOW()
WHERE vreemdelingnummer = $1;

-- name: VreemdelingGetByNummer :one
SELECT *
FROM bvv_backend.vreemdeling
WHERE vreemdelingnummer = $1 AND deleted_at IS NULL;

-- name: VreemdelingGetById :one
SELECT *
FROM bvv_backend.vreemdeling
WHERE id = $1 AND deleted_at IS NULL;

-- name: VreemdelingSearchByNameAndBirthdate :many
SELECT *
FROM bvv_backend.vreemdeling
WHERE (searchname ILIKE '%' || sqlc.narg('name') || '%' OR sqlc.narg('name') IS NULL)
AND (birthdate = sqlc.narg('birthdate') OR sqlc.narg('birthdate') IS NULL)
AND deleted_at IS NULL;

-- name: VreemdelingCount :one
SELECT count(id)
FROM bvv_backend.vreemdeling;