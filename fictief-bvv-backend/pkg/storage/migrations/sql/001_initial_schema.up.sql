BEGIN transaction;

CREATE SCHEMA IF NOT EXISTS bvv_backend;

CREATE TABLE bvv_backend.vreemdeling (
    id uuid NOT NULL,
    vreemdelingnummer TEXT NOT NULL UNIQUE,
    name TEXT,
    searchname TEXT,
    birthdate TIMESTAMP,
    created_at TIMESTAMPTZ NOT NULL DEFAULT NOW(), 
    updated_at TIMESTAMPTZ NOT NULL DEFAULT NOW(),
    deleted_at TIMESTAMPTZ,

    PRIMARY KEY(id)
);

CREATE TABLE bvv_backend.vreemdeling_mapping (
    vreemdelingnummer TEXT NOT NULL UNIQUE,
    duplicate_vreemdelingnummers TEXT[],
    PRIMARY KEY(vreemdelingnummer)
);

COMMIT;
