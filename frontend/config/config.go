package config

import (
	"fmt"
	"os"

	"gopkg.in/yaml.v3"
)

type Role struct {
	Name        string `yaml:"name"`
	Label       string `yaml:"label"`
	Permissions struct {
		BVVWriteAccess bool `yaml:"bvvWriteAccess"`

		Rubrieken struct {
			Get []string `yaml:"get"` // List of field names, also below
			Set []string `yaml:"set"`
		} `yaml:"rubrieken"`

		Processen map[string]struct { // Map keys are the proces names/IDs
			Get []string `yaml:"get"`
			Set []string `yaml:"set"`
		} `yaml:"processen"`
	} `yaml:"permissions"`
}

type Attribute struct {
	Name     string      `yaml:"name"`
	Label    string      `yaml:"label"`
	Type     string      `yaml:"type"`
	Multiple bool        `yaml:"multiple"` // For types 'select' and 'file'
	Default  interface{} `yaml:"default"`

	// For number types
	Min  interface{} `yaml:"min"`
	Max  interface{} `yaml:"max"`
	Step interface{} `yaml:"step"`

	// For select, radios, and checkboxes types
	Options []struct {
		Name  string `yaml:"name"`
		Label string `yaml:"label"`
	} `yaml:"options"`
}

type Proces struct {
	Name     string `yaml:"name"`
	Label    string `yaml:"label"`
	Statuses []struct {
		Name  string `yaml:"name"`
		Label string `yaml:"label"`
	} `yaml:"statuses"`

	Attributes []Attribute `yaml:"attributes"`
}

type GlobalConfig struct {
	Rubrieken []Attribute `yaml:"rubrieken"`

	Processen []Proces `yaml:"processen"`
}

type OrganizationConfig struct {
	Organization struct {
		Name                string   `yaml:"name"`
		Label               string   `yaml:"label"`
		Theme               string   `yaml:"theme"`
		BVVWriteAccess      bool     `yaml:"bvvWriteAccess"`
		ProcesNotifications []string `yaml:"procesNotifications"` // Contains the names/IDs of the processes that the organization is notified about when enabled on vreemdeling-level
	} `yaml:"organization"`

	Roles []Role

	ListenAddress string `yaml:"listenAddress"`
}

type FscConfig map[string]struct {
	Endpoint     string `yaml:"endpoint"`
	FscGrantHash string `yaml:"FscGrantHash"`
}

type Config struct {
	GlobalConfig
	OrganizationConfig
	FscConfig
}

// New composes a config with values from the specified paths
func New(globalConfigPath string, organizationConfigPath string, fscConfigPath string) (cfg Config, err error) {
	var yamlFile []byte
	if yamlFile, err = os.ReadFile(globalConfigPath); err != nil {
		err = fmt.Errorf("error reading global config file: %w", err)
		return
	}

	if err = yaml.Unmarshal(yamlFile, &cfg.GlobalConfig); err != nil {
		err = fmt.Errorf("error unmarshalling global config: %w", err)
		return
	}

	if yamlFile, err = os.ReadFile(organizationConfigPath); err != nil {
		err = fmt.Errorf("error reading organization config file: %w", err)
		return
	}

	if err = yaml.Unmarshal(yamlFile, &cfg.OrganizationConfig); err != nil {
		err = fmt.Errorf("error unmarshalling organization config: %w", err)
		return
	}

	if yamlFile, err = os.ReadFile(fscConfigPath); err != nil {
		err = fmt.Errorf("error reading FSC config file: %w", err)
		return
	}

	if err = yaml.Unmarshal(yamlFile, &cfg.FscConfig); err != nil {
		err = fmt.Errorf("error unmarshalling FSC config: %w", err)
		return
	}

	if cfg.ListenAddress == "" {
		cfg.ListenAddress = "0.0.0.0:80" // Default port if not set in config
	}

	return
}
