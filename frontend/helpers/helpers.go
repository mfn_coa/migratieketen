package helpers

import (
	"errors"
	"fmt"
	"log"
	"strconv"
	"strings"
	"time"

	openapi_types "github.com/oapi-codegen/runtime/types"
	"golang.org/x/text/language"
	"golang.org/x/text/message"
	"golang.org/x/text/number"
)

var printer = message.NewPrinter(language.Dutch)

var monthsReplacer = strings.NewReplacer( // Use a strings replacer to format the time in Dutch language. See time.shortMonthNames. IMPROVE: neater solution using a library
	"Jan", "januari",
	"Feb", "februari",
	"Mar", "maart",
	"Apr", "april",
	"May", "mei",
	"Jun", "juni",
	"Jul", "juli",
	"Aug", "augustus",
	"Sep", "september",
	"Oct", "oktober",
	"Nov", "november",
	"Dec", "december",
)

// amsLocation refers to the Dutch time zone / location
var amsLocation *time.Location

// FormattedDate is used to unmarshal date values in Dutch format directly from POST bodies
type FormattedDate openapi_types.Date

// FormattedTime is used to unmarshal datetime values in Dutch format directly from POST bodies
type FormattedTime time.Time

func init() {
	var err error
	if amsLocation, err = time.LoadLocation("Europe/Amsterdam"); err != nil {
		log.Fatalf("error getting time zone: %v", err)
	}
}

func (fd *FormattedDate) UnmarshalText(b []byte) error {
	val := strings.TrimSpace(string(b)) // Get rid of possible whitespace
	if val == "" {
		return nil
	}

	// Parse the time
	t, err := time.ParseInLocation("02-01-2006", val, amsLocation)
	if err != nil {
		return err
	}

	// Set the result
	fd.Time = t
	return nil
}

func (fd FormattedDate) IsZero() bool {
	return fd.Time.IsZero()
}

func (ft *FormattedTime) UnmarshalText(b []byte) error {
	val := strings.TrimSpace(string(b)) // Get rid of possible whitespace
	if val == "" {
		return nil
	}

	// Parse the time
	t, err := time.ParseInLocation("02-01-2006 15:04", val, amsLocation)
	if err != nil {
		return err
	}

	// Set the result
	*ft = FormattedTime(t)
	return nil
}

func (ft FormattedTime) IsZero() bool {
	return time.Time(ft).IsZero()
}

func FormatObservationValue(val any) string {
	switch v := val.(type) {
	case []string:
		return strings.Join(v, ", ")
	}

	return fmt.Sprintf("%v", val)
}

// FormatTime formats the specified time in the Dutch time zone and language
func FormatTime(t *time.Time) string {
	if t == nil || t.IsZero() {
		return "(Onbekend)"
	}

	return monthsReplacer.Replace(t.In(amsLocation).Format("2 Jan 2006 15:04 uur"))
}

// FormatDate formats the specified time in the Dutch time zone and language
func FormatDate(t *time.Time) string {
	if t == nil || t.IsZero() {
		return "(Onbekend)"
	}

	return monthsReplacer.Replace(t.In(amsLocation).Format("2 Jan 2006"))
}

// FormatYear returns the year of the specief time.Time instance in the Dutch time zone
func FormatYear(t *time.Time) string {
	if t == nil || t.IsZero() {
		return "(Onbekend)"
	}

	return strconv.Itoa(t.In(amsLocation).Year())
}

// NumberFormat formats the specified number as string. IMPROVE: access an interface argument (or generic) and support multiple number types
func NumberFormat(v interface{}) string {
	if v == nil {
		return ""
	}

	switch v.(type) {
	case int, int8, int16, int32, int64, uint, uint16, uint32, uint64:
		// Integer: show the number without decimals
		return printer.Sprint(v)
	default:
		// Otherwise (e.g. float): assume the number is a currency, so show the number with two fixed digits
		return printer.Sprint(number.Decimal(v, number.MinFractionDigits(2), number.MaxFractionDigits(2)))
	}

	// IMPROVE: use a library like github.com/shopspring/decimal for decimal numbers instead of float64, see https://github.com/shopspring/decimal#why-dont-you-just-use-float64
}

// Dict creates a new map from the given parameters by treating values as key-value pairs. The number of values must be even. See https://github.com/Masterminds/sprig/blob/master/dict.go and https://github.com/gohugoio/hugo/blob/master/tpl/collections/collections.go
func Dict(v ...interface{}) (map[string]interface{}, error) {
	if len(v)%2 != 0 {
		return nil, errors.New("invalid dict call, the number of arguments must be even")
	}

	dict := make(map[string]interface{}, len(v)/2)
	for i := 0; i < len(v); i += 2 {
		key, ok := v[i].(string)
		if !ok {
			return nil, errors.New("invalid dict key, must be string")
		}

		dict[key] = v[i+1]
	}

	return dict, nil
}
