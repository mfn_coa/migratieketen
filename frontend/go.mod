module gitlab.com/digilab.overheid.nl/ecosystem/migratieketen/frontend

go 1.21.5

// replace gitlab.com/digilab.overheid.nl/ecosystem/migratieketen/fictief-bvv-api => ../fictief-bvv-api

require (
	github.com/gofiber/contrib/websocket v1.3.0
	github.com/gofiber/fiber/v2 v2.51.0
	github.com/gofiber/template/html/v2 v2.0.5
	github.com/gorilla/websocket v1.5.1
	github.com/oapi-codegen/runtime v1.1.0
	github.com/spf13/cobra v1.8.0
	gitlab.com/digilab.overheid.nl/ecosystem/migratieketen/fictief-bvv-api v0.0.0-20240103145725-4d387763c26c
	gitlab.com/digilab.overheid.nl/ecosystem/migratieketen/fictief-loket-api v0.0.0-20240103145725-4d387763c26c
	gitlab.com/digilab.overheid.nl/ecosystem/migratieketen/fictief-sigma-api v0.0.0-20240103145725-4d387763c26c
	gitlab.com/digilab.overheid.nl/ecosystem/migratieketen/notification-api v0.0.0-20240103145725-4d387763c26c
	gopkg.in/yaml.v3 v3.0.1
)

require (
	github.com/apapsch/go-jsonmerge/v2 v2.0.0 // indirect
	github.com/fasthttp/websocket v1.5.7 // indirect
	github.com/getkin/kin-openapi v0.122.0 // indirect
	github.com/go-chi/chi/v5 v5.0.11 // indirect
	github.com/go-openapi/jsonpointer v0.20.2 // indirect
	github.com/go-openapi/swag v0.22.7 // indirect
	github.com/invopop/yaml v0.2.0 // indirect
	github.com/josharian/intern v1.0.0 // indirect
	github.com/mailru/easyjson v0.7.7 // indirect
	github.com/mohae/deepcopy v0.0.0-20170929034955-c48cc78d4826 // indirect
	github.com/perimeterx/marshmallow v1.1.5 // indirect
	github.com/savsgio/gotils v0.0.0-20230208104028-c358bd845dee // indirect
	golang.org/x/net v0.19.0 // indirect
)

require (
	github.com/andybalholm/brotli v1.0.6 // indirect
	github.com/gofiber/template v1.8.2 // indirect
	github.com/gofiber/utils v1.1.0 // indirect
	github.com/google/uuid v1.5.0 // indirect
	github.com/inconshreveable/mousetrap v1.1.0 // indirect
	github.com/klauspost/compress v1.17.4 // indirect
	github.com/mattn/go-colorable v0.1.13 // indirect
	github.com/mattn/go-isatty v0.0.20 // indirect
	github.com/mattn/go-runewidth v0.0.15 // indirect
	github.com/rivo/uniseg v0.4.4 // indirect
	github.com/spf13/pflag v1.0.5 // indirect
	github.com/valyala/bytebufferpool v1.0.0 // indirect
	github.com/valyala/fasthttp v1.51.0 // indirect
	github.com/valyala/tcplisten v1.0.0 // indirect
	golang.org/x/sys v0.15.0 // indirect
	golang.org/x/text v0.14.0
)
