package application

import (
	"context"
	"errors"
	"fmt"
	"log"
	"time"

	"github.com/gofiber/fiber/v2"
	openapi_types "github.com/oapi-codegen/runtime/types"

	bvv "gitlab.com/digilab.overheid.nl/ecosystem/migratieketen/fictief-bvv-api"
	loket "gitlab.com/digilab.overheid.nl/ecosystem/migratieketen/fictief-loket-api"
	sigma "gitlab.com/digilab.overheid.nl/ecosystem/migratieketen/fictief-sigma-api"
	"gitlab.com/digilab.overheid.nl/ecosystem/migratieketen/frontend/config"
	"gitlab.com/digilab.overheid.nl/ecosystem/migratieketen/frontend/helpers"
)

type dataAttributes []struct {
	Name  string   `form:"name"`
	Value []string `form:"value"` // Note: can only parse this as string (slice) due to the Fiber implementation
}

type createOrUpdateVreemdelingData struct {
	Naam          string                `form:"naam"`
	Geboortedatum helpers.FormattedDate `form:"geboortedatum"`

	// Decode the attributes that are passed as array
	Attributes dataAttributes `form:"attributes"`
}

type fieldModification struct {
	Organization string
	LastModified time.Time
}

type mockData struct {
	Value        interface{}
	Observations []loket.Observation
}

func (a *Application) SearchVreemdeling(c *fiber.Ctx) error {
	// Parse the request body
	var data struct {
		Vreemdelingnummer string                `form:"vreemdelingnummer"`
		Name              string                `form:"name"`
		Birthdate         helpers.FormattedDate `form:"birthdate"`
	}
	if err := c.BodyParser(&data); err != nil {
		return fmt.Errorf("unexpected request body: %w", err)
	}

	// Search in the BVV
	client, err := a.getBVVClient()
	if err != nil {
		return fmt.Errorf("error creating BVV client: %w", err)
	}

	ctx := context.Background()

	var results []bvv.Vreemdeling
	if data.Vreemdelingnummer != "" {
		// If the vreemdelingnummer is set, get the vreemdeling by ID
		var resp *bvv.ReadVreemdelingByIdResponse
		if resp, err = client.ReadVreemdelingByIdWithResponse(ctx, data.Vreemdelingnummer); err != nil {
			return fmt.Errorf("error fetching from BVV: %w", err)
		}

		if resp.JSON410 == nil { // In case no results are found, just skip
			if res := resp.JSON200; res != nil {
				results = []bvv.Vreemdeling{
					res.Data,
				}
			} else {
				return fmt.Errorf("unexpected response while fetching from BVV, http code %d, body: %s", resp.StatusCode(), resp.Body)
			}
		}
	} else {
		// If no vreemdelingnummer is set, find the vreemdeling by name and/or birthdate
		var params bvv.FindVreemdelingParams
		if data.Name != "" {
			params.Naam = &data.Name
		}
		if !data.Birthdate.IsZero() {
			bd := openapi_types.Date(data.Birthdate)
			params.Geboortedatum = &bd
		}

		resp, err := client.FindVreemdelingWithResponse(ctx, &params)
		if err != nil {
			return fmt.Errorf("error searching in BVV: %w", err)
		}

		// In case of an error, return
		if resp.JSON400 != nil {
			log.Printf("error fetching person from BVV: %v", resp.JSON400.Errors)
			return fmt.Errorf("fout bij ophalen van de persoon: %v", resp.JSON400.Errors)
		}

		// Make sure the response is not nil to avoid panics
		if resp.JSON200 == nil {
			return fmt.Errorf("onbekende fout bij zoeken van de persoon. response body: %s", resp.Body)
		}

		results = resp.JSON200.Data
	}

	return c.Render("search", fiber.Map{
		"results": results,
	})
}

func (a *Application) NewVreemdeling(c *fiber.Ctx) error {
	if !a.Cfg.Organization.BVVWriteAccess {
		return errors.New("this organization does not have BVV write access")
	}

	// Get the default Sigma values from the config. Note: currently not useful to allow setting BVV default values
	sigmaData := make(map[string]any)
	for _, attr := range a.Cfg.Rubrieken {
		if attr.Default != nil {
			sigmaData[attr.Name] = attr.Default
		}
	}

	return c.Render("vreemdeling-new", fiber.Map{
		"rubrieken": a.Cfg.Rubrieken,
		"sigmaData": sigmaData,
	})
}

func (a *Application) CreateVreemdeling(c *fiber.Ctx) error {
	// Parse the request body
	var data createOrUpdateVreemdelingData
	if err := c.BodyParser(&data); err != nil {
		return fmt.Errorf("unexpected request body: %w", err)
	}

	// Insert the vreemdeling into the BVV
	bvvClient, err := a.getBVVClient()
	if err != nil {
		return fmt.Errorf("error creating BVV client: %w", err)
	}

	ctx := context.Background()

	bd := openapi_types.Date(data.Geboortedatum)
	bvvResp, err := bvvClient.CreateVreemdelingWithResponse(ctx, bvv.CreateVreemdelingJSONRequestBody{
		Data: bvv.VreemdelingWithoutId{
			Geboortedatum: &bd,
			Naam:          data.Naam,
		},
	})
	if err != nil {
		return fmt.Errorf("error inserting into BVV: %w", err)
	}

	// In case of an error, return
	if bvvResp.JSON400 != nil {
		log.Printf("error creating person in BVV: %v", bvvResp.JSON400.Errors)
		return fmt.Errorf("fout bij aanmaken van de persoon: %v", bvvResp.JSON400.Errors)
	}

	// Make sure the response is not nil to avoid panics
	if bvvResp.JSON201 == nil {
		return fmt.Errorf("onbekende fout bij aanmaken van de persoon. response body: %s", bvvResp.Body)
	}

	vreemdelingNummer := bvvResp.JSON201.Data.Id

	// Insert the remaining attributes into Sigma
	if err = a.setSigmaRubrieken(ctx, vreemdelingNummer, data.Attributes); err != nil {
		return fmt.Errorf("error setting attributes in Sigma: %w", err)
	}

	// Show a page that the person has been inserted
	return c.Render("vreemdeling-new-finished", fiber.Map{
		"vreemdelingNummer": vreemdelingNummer,
	})
}

func (a *Application) GetVreemdeling(c *fiber.Ctx) error {
	// Get the vreemdeling from the BVV
	bvvClient, err := a.getBVVClient()
	if err != nil {
		return fmt.Errorf("error creating BVV client: %w", err)
	}

	vreemdelingNummer := c.Params("vreemdelingNummer")

	ctx := context.Background()

	var bvvResp *bvv.ReadVreemdelingByIdResponse
	if bvvResp, err = bvvClient.ReadVreemdelingByIdWithResponse(ctx, vreemdelingNummer); err != nil {
		return fmt.Errorf("error fetching from BVV: %w", err)
	}

	// In case no results are found, return an error
	if bvvResp.JSON410 != nil {
		return errors.New("deze vreemdeling bestaat niet")
	}

	var bvvData bvv.Vreemdeling
	if res := bvvResp.JSON200; res != nil {
		bvvData = res.Data
	} else {
		return fmt.Errorf("unexpected response while fetching from BVV, http code %d, body: %s", bvvResp.StatusCode(), bvvResp.Body)
	}

	// // Get additional data from the Sigma loket
	// loketClient, err := a.getLoketClient()
	// if err != nil {
	// 	return fmt.Errorf("error creating Sigma loket client: %w", err)
	// }

	// // Get the user role
	// role, ok := c.Locals("role").(config.Role)
	// if !ok {
	// 	return errors.New("error obtaining user role")
	// }

	// // Send the request to the Sigma loket
	// loketResp, err := loketClient.ReadVreemdelingByIdWithResponse(ctx, vreemdelingNummer, &loket.ReadVreemdelingByIdParams{
	// 	Attributes: role.Permissions.Rubrieken.Get, // Pass the user's Get permissions as fields we want to fetch
	// })
	// if err != nil {
	// 	return fmt.Errorf("error fetching data from Sigma loket: %w", err)
	// }

	// // In case of an error, return
	// if loketResp.JSON400 != nil {
	// 	log.Printf("error fetching person from Sigma loket: %v", loketResp.JSON400.Errors)
	// 	return fmt.Errorf("fout bij ophalen van de persoon uit Sigma loket: %v", loketResp.JSON400.Errors)
	// }

	// if loketResp.JSON410 != nil {
	// 	log.Printf("failed to fetch non-existing person from Sigma loket: %v", loketResp.JSON410.Errors)
	// 	return fmt.Errorf("deze persoon bestaat niet volgens het Sigma loket: %v", loketResp.JSON410.Errors)
	// }

	// // Make sure the response is not nil to avoid panics
	// if loketResp.JSON200 == nil {
	// 	return fmt.Errorf("onbekende fout bij ophalen van de persoon uit Sigma loket. response body: %s", loketResp.Body)
	// }

	// log.Println("--> loket response:", loketResp) // TODO: use the response once implemented in the backend

	// Get the processen from the Sigma loket
	// TODO: add this once implemented in the loket

	return c.Render("vreemdeling", fiber.Map{
		"bvvData":   bvvData,
		"rubrieken": a.Cfg.Rubrieken,
		// "data": loket.VreemdelingWithObservationsResponse{
		// 	Data: loket.VreemdelingWithObservations{
		// 		Spreektaal: &loket.SpreektaalWithObservations{
		// 			Value: []string{},
		// 			Observations: []loket.Observation{},
		// 		},
		// 	},
		// },

		// TODO: convert the raw data to full content using the fields config. Use lookup maps?
		"data": map[string]any{ // IMPROVE: rename to sigmaData or so
			"alias": mockData{ // Note: multiple values possible from multiple organizations
				Value: "Mr. X",
				Observations: []loket.Observation{
					{
						Value:     "Mr. X",
						Source:    "Fictief Nationale Politie",
						CreatedAt: time.Now(),
					},
				},
			},
			"spreektaal": mockData{
				Value: []string{"Engels"},
				Observations: []loket.Observation{
					{
						Value:     []string{"Engels"},
						Source:    "Fictief DJI",
						CreatedAt: time.Now(),
					},
					{
						Value:     []string{"Engels", "Nederlands"},
						Source:    "Fictief DT&V",
						CreatedAt: time.Now(),
					},
				},
			},
			"numberTest": mockData{
				Value: "1,5",
				Observations: []loket.Observation{
					{
						Value:     "1,5",
						Source:    "Fictief DJI",
						CreatedAt: time.Now(),
					},
				},
			},
			"booleanTest": mockData{
				Value: "Ja",
				Observations: []loket.Observation{
					{
						Value:     "Ja",
						Source:    "Fictief DJI",
						CreatedAt: time.Now(),
					},
				},
			},
			"textareaTest": mockData{
				Value: "A\n\nB",
				Observations: []loket.Observation{
					{
						Value:     "A\n\nB",
						Source:    "Fictief DJI",
						CreatedAt: time.Now(),
					},
				},
			},
			"lengteCategorie": mockData{
				Value: "Middel",
				Observations: []loket.Observation{
					{
						Value:     "Middel",
						Source:    "Fictief DJI",
						CreatedAt: time.Now(),
					},
					{
						Value:     "Groot",
						Source:    "Fictief DT&V",
						CreatedAt: time.Now(),
					},
				},
			},
			"landFamilielid": mockData{
				Value: "Afghanistan",
				Observations: []loket.Observation{
					{
						Value:     "Afghanistan",
						Source:    "Fictief DJI",
						CreatedAt: time.Now(),
					},
					{
						Value:     "Åland",
						Source:    "Fictief DT&V",
						CreatedAt: time.Now(),
					},
				},
			},
		},
	})
}

func (a *Application) EditVreemdeling(c *fiber.Ctx) error {
	vreemdelingNummer := c.Params("vreemdelingNummer")

	// Get the user role
	role, ok := c.Locals("role").(config.Role)
	if !ok {
		return errors.New("error obtaining user role")
	}

	// If the organization and the user have BVV write access, get the current values from the BVV
	var bvvData bvv.Vreemdeling
	if a.Cfg.Organization.BVVWriteAccess && role.Permissions.BVVWriteAccess {
		bvvClient, err := a.getBVVClient()
		if err != nil {
			return fmt.Errorf("error creating BVV client: %w", err)
		}

		vreemdelingNummer := c.Params("vreemdelingNummer")

		ctx := context.Background()

		var bvvResp *bvv.ReadVreemdelingByIdResponse
		if bvvResp, err = bvvClient.ReadVreemdelingByIdWithResponse(ctx, vreemdelingNummer); err != nil {
			return fmt.Errorf("error fetching from BVV: %w", err)
		}

		// In case no results are found, return an error
		if bvvResp.JSON410 != nil {
			return errors.New("deze vreemdeling bestaat niet")
		}

		if res := bvvResp.JSON200; res != nil {
			bvvData = res.Data
		} else {
			return fmt.Errorf("unexpected response while fetching from BVV, http code %d, body: %s", bvvResp.StatusCode(), bvvResp.Body)
		}
	}

	// Get the current values from Sigma loket
	// TODO: add this once implemented in the backend

	return c.Render("vreemdeling-edit", fiber.Map{
		"vreemdelingNummer": vreemdelingNummer,
		"rubrieken":         a.Cfg.Rubrieken,
		"bvvData":           bvvData,
		"sigmaData": map[string]any{
			"numberTest":      1.5, // Note: the default values are only used for creating, not for editing/updating
			"booleanTest":     false,
			"spreektaal":      []string{"en"},
			"lengteCategorie": "s",
			"landFamilielid":  []string{"af", "ax"},
		},
	})
}

func (a *Application) UpdateVreemdeling(c *fiber.Ctx) error {
	// Parse the request body
	var data createOrUpdateVreemdelingData
	if err := c.BodyParser(&data); err != nil {
		return fmt.Errorf("unexpected request body: %w", err)
	}

	vreemdelingNummer := c.Params("vreemdelingNummer")

	// Get the user role
	role, ok := c.Locals("role").(config.Role)
	if !ok {
		return errors.New("error obtaining user role")
	}

	ctx := context.Background()

	// If the organization and the user has BVV write access, update the vreemdeling in the BVV
	if a.Cfg.Organization.BVVWriteAccess && role.Permissions.BVVWriteAccess {
		bvvClient, err := a.getBVVClient()
		if err != nil {
			return fmt.Errorf("error creating BVV client: %w", err)
		}

		bd := openapi_types.Date(data.Geboortedatum)
		bvvResp, err := bvvClient.UpdateVreemdelingByIdWithResponse(ctx, vreemdelingNummer, bvv.UpdateVreemdelingByIdJSONRequestBody{
			Data: bvv.Vreemdeling{
				Id:            vreemdelingNummer,
				Geboortedatum: &bd,
				Naam:          data.Naam,
			},
		})
		if err != nil {
			return fmt.Errorf("error updating in BVV: %w", err)
		}

		// In case of an error, return
		if bvvResp.JSON400 != nil {
			log.Printf("error updating person in BVV: %v", bvvResp.JSON400.Errors)
			return fmt.Errorf("fout bij aanpassen van de persoon: %v", bvvResp.JSON400.Errors)
		}

		if bvvResp.JSON410 != nil {
			log.Printf("error updating person in BVV: %v", bvvResp.JSON410.Errors)
			return fmt.Errorf("fout bij aanpassen van de persoon: %v", bvvResp.JSON410.Errors)
		}

		// Make sure the response is not nil to avoid panics
		if bvvResp.JSON200 == nil {
			return fmt.Errorf("onbekende fout bij aanpassen van de persoon. response body: %s", bvvResp.Body)
		}
	}

	// Update the remaining attributes into Sigma
	if err := a.setSigmaRubrieken(ctx, vreemdelingNummer, data.Attributes); err != nil {
		return fmt.Errorf("error setting attributes in Sigma: %w", err)
	}

	// Show a page that the person has been inserted
	return c.Render("vreemdeling-edit-finished", fiber.Map{
		"vreemdelingNummer": vreemdelingNummer,
	})
}

func (a *Application) setSigmaRubrieken(ctx context.Context, vreemdelingNummer string, attributes dataAttributes) error {
	// Create a Sigma client
	sigmaClient, err := a.getSigmaClient()
	if err != nil {
		return fmt.Errorf("error creating Sigma client: %w", err)
	}

	// Range over all submitted attributes
	for _, dataAttr := range attributes {
		// Get the corresponding rubriek from the config
		found := false

		var attr *config.Attribute
		for i, at := range a.Cfg.Rubrieken {
			if at.Name == dataAttr.Name {
				attr = &a.Cfg.Rubrieken[i] // Note: instead of &at, which would be incorrect due to the loop
				found = true
				break
			}
		}

		if !found {
			continue // Skip if the attribute name/key is not found in the config
		}

		val, err := a.parseAttribute(dataAttr.Value, attr)
		if err != nil {
			return fmt.Errorf("error parsing attribute '%s' (value: %v). error: %w", attr.Name, dataAttr.Value, err)
		}

		sigmaResp, err := sigmaClient.AddObservationWithResponse(ctx, vreemdelingNummer, sigma.AddObservationJSONRequestBody{
			Data: sigma.ObservationCreate{
				Attribute: attr.Name,
				Value:     val,
			},
		})
		if err != nil {
			return fmt.Errorf("error inserting into Sigma: %w", err)
		}

		log.Printf("--> sigma response: %s", sigmaResp.Body) // TODO: use or ignore
	}

	return nil
}
