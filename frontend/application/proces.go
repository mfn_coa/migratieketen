package application

import (
	"context"
	"errors"
	"fmt"
	"log"

	"github.com/gofiber/fiber/v2"

	sigma "gitlab.com/digilab.overheid.nl/ecosystem/migratieketen/fictief-sigma-api"
	"gitlab.com/digilab.overheid.nl/ecosystem/migratieketen/frontend/config"
)

type createOrUpdateProcesData struct {
	Type   string `form:"type"`
	Status string `form:"status"`

	// Decode the attributes that are passed as array
	Attributes dataAttributes `form:"attributes"`
}

func (a *Application) NewProces(c *fiber.Ctx) error {
	return c.Render("proces-new", fiber.Map{
		"vreemdelingNummer": c.Params("vreemdelingNummer"),
		"processen":         a.Cfg.Processen,
	})
}

func (a *Application) NewProcesDetails(c *fiber.Ctx) error {
	// Parse the request body
	processType := c.Query("type")
	if processType == "" {
		return errors.New("missing proces type")
	}

	// Find the proces in the config, based on its name/type
	var proces config.Proces
	for _, pr := range a.Cfg.Processen {
		if pr.Name == processType {
			proces = pr
			break
		}
	}

	// In case no proces was found in the config, return an error
	if proces.Name == "" { // Note: empty / default value
		return fmt.Errorf("unrecognized proces type: '%s'", processType)
	}

	// Get the proces attributes default values from the config
	data := make(map[string]any)
	for _, attr := range proces.Attributes {
		if attr.Default != nil {
			data[attr.Name] = attr.Default
		}
	}

	return c.Render("proces-new-details", fiber.Map{
		"vreemdelingNummer": c.Params("vreemdelingNummer"),
		"proces":            proces,
		"data":              data,
	}, "")
}

func (a *Application) CreateProces(c *fiber.Ctx) error {
	// Parse the request body
	var data createOrUpdateProcesData
	if err := c.BodyParser(&data); err != nil {
		return fmt.Errorf("unexpected request body: %w", err)
	}

	// Insert the proces into Sigma
	sigmaClient, err := a.getSigmaClient()
	if err != nil {
		return fmt.Errorf("error creating Sigma client: %w", err)
	}

	ctx := context.Background()

	vreemdelingNummer := c.Params("vreemdelingNummer")

	attrs, err := a.getProcesAttributes(data)
	if err != nil {
		return fmt.Errorf("error getting proces attributes: %w", err)
	}

	sigmaResp, err := sigmaClient.AddProcesWithResponse(ctx, vreemdelingNummer, sigma.AddProcesJSONRequestBody{
		Data: sigma.ProcesCreate{
			Attributes: attrs,
			Status:     data.Status,
			Type:       data.Type,
		},
	})
	if err != nil {
		return fmt.Errorf("error adding proces in Sigma: %w", err)
	}

	// In case of an error, return
	if sigmaResp.JSON400 != nil {
		log.Printf("error creating proces in Sigma: %v", sigmaResp.JSON400.Errors)
		return fmt.Errorf("fout bij aanmaken van het proces: %v", sigmaResp.JSON400.Errors)
	}

	if sigmaResp.JSON410 != nil {
		log.Printf("error creating proces in Sigma: %v", sigmaResp.JSON410.Errors)
		return fmt.Errorf("fout bij aanmaken van het proces: %v", sigmaResp.JSON410.Errors)
	}

	// Make sure the response is not nil to avoid panics
	if sigmaResp.JSON201 == nil {
		return fmt.Errorf("onbekende fout bij aanmaken van het proces. response body: %s", sigmaResp.Body)
	}

	procesID := sigmaResp.JSON201.Data.Id

	// Show a page that the person has been inserted
	return c.Render("proces-new-finished", fiber.Map{
		"vreemdelingNummer": vreemdelingNummer,
		"procesID":          procesID,
	})
}

func (a *Application) GetProces(c *fiber.Ctx) error {
	// Get the proces from Sigma
	sigmaClient, err := a.getSigmaClient()
	if err != nil {
		return fmt.Errorf("error creating Sigma client: %w", err)
	}

	ctx := context.Background()

	procesID := c.Params("procesID")

	sigmaResp, err := sigmaClient.ReadProcesByIdWithResponse(ctx, procesID)
	if err != nil {
		return fmt.Errorf("error fetching proces from Sigma: %w", err)
	}

	// In case of an error, return
	if sigmaResp.JSON400 != nil {
		log.Printf("error fetching proces from Sigma: %v", sigmaResp.JSON400.Errors)
		return fmt.Errorf("fout bij ophalen van proces: %v", sigmaResp.JSON400.Errors)
	}

	if sigmaResp.JSON410 != nil {
		log.Printf("error updating proces from Sigma: %v", sigmaResp.JSON410.Errors)
		return fmt.Errorf("fout bij ophalen van proces: %v", sigmaResp.JSON410.Errors)
	}

	// Make sure the response is not nil to avoid panics
	if sigmaResp.JSON200 == nil {
		return fmt.Errorf("onbekende fout bij ophalen van proces. response body: %s", sigmaResp.Body)
	}

	data := sigmaResp.JSON200.Data

	// Find the proces in the config, based on its name/type. IMPROVE: return an error if not found?
	var proces config.Proces
	for _, pr := range a.Cfg.Processen {
		if pr.Name == data.Type {
			proces = pr
			break
		}
	}

	// Select the corresponding status label from the config
	var statusLabel string
	for _, status := range proces.Statuses {
		if status.Name == data.Status {
			statusLabel = status.Label
			break
		}
	}

	return c.Render("proces", fiber.Map{
		"vreemdelingNummer": c.Params("vreemdelingNummer"),
		"procesID":          procesID,
		"proces":            proces,
		"statusLabel":       statusLabel,
		"data":              data,
	})
}

func (a *Application) EditProces(c *fiber.Ctx) error {
	// Get the proces from Sigma
	sigmaClient, err := a.getSigmaClient()
	if err != nil {
		return fmt.Errorf("error creating Sigma client: %w", err)
	}

	ctx := context.Background()

	procesID := c.Params("procesID")

	sigmaResp, err := sigmaClient.ReadProcesByIdWithResponse(ctx, procesID)
	if err != nil {
		return fmt.Errorf("error fetching proces from Sigma: %w", err)
	}

	// In case of an error, return
	if sigmaResp.JSON400 != nil {
		log.Printf("error fetching proces from Sigma: %v", sigmaResp.JSON400.Errors)
		return fmt.Errorf("fout bij ophalen van proces: %v", sigmaResp.JSON400.Errors)
	}

	if sigmaResp.JSON410 != nil {
		log.Printf("error fetching proces from Sigma: %v", sigmaResp.JSON410.Errors)
		return fmt.Errorf("fout bij ophalen van proces: %v", sigmaResp.JSON410.Errors)
	}

	// Make sure the response is not nil to avoid panics
	if sigmaResp.JSON200 == nil {
		return fmt.Errorf("onbekende fout bij ophalen van proces. response body: %s", sigmaResp.Body)
	}

	return c.Render("proces-edit", fiber.Map{
		"vreemdelingNummer": c.Params("vreemdelingNummer"),
		"proces":            a.Cfg.Processen[0], // TODO: not hardcoded index 0
		"data":              sigmaResp.JSON200.Data,
	})
}

func (a *Application) UpdateProces(c *fiber.Ctx) error {
	// Parse the request body
	var data createOrUpdateProcesData
	if err := c.BodyParser(&data); err != nil {
		return fmt.Errorf("unexpected request body: %w", err)
	}

	// Create a Sigma client
	sigmaClient, err := a.getSigmaClient()
	if err != nil {
		return fmt.Errorf("error creating Sigma client: %w", err)
	}

	ctx := context.Background()

	procesID := c.Params("procesID")

	// Update the proces status
	sigmaResp, err := sigmaClient.UpdateProcesByIdWithResponse(ctx, procesID, sigma.UpdateProcesByIdJSONRequestBody{
		Data: sigma.ProcesUpdate{
			Status: data.Status,
		},
	})
	if err != nil {
		return fmt.Errorf("error inserting proces: %w", err)
	}

	// Update the proces attributes by adding new ones that overrule the existing ones
	attrs, err := a.getProcesAttributes(data)
	if err != nil {
		return fmt.Errorf("error getting proces attributes: %w", err)
	}

	for _, attr := range attrs {
		if _, err = sigmaClient.AddAttributeByProcesIdWithResponse(ctx, procesID, sigma.AddAttributeByProcesIdJSONRequestBody{
			Data: attr,
		}); err != nil {
			return fmt.Errorf("error storing proces attributes: %w", err)
		}
	}

	// In case of an error, return
	if sigmaResp.JSON400 != nil {
		log.Printf("error updating proces in Sigma: %v", sigmaResp.JSON400.Errors)
		return fmt.Errorf("fout bij aanpassen van het proces: %v", sigmaResp.JSON400.Errors)
	}

	if sigmaResp.JSON410 != nil {
		log.Printf("error updating proces in Sigma: %v", sigmaResp.JSON410.Errors)
		return fmt.Errorf("fout bij aanpassen van het proces: %v", sigmaResp.JSON410.Errors)
	}

	// Make sure the response is not nil to avoid panics
	if sigmaResp.JSON204 == nil {
		return fmt.Errorf("onbekende fout bij aanpassen van het proces. response body: %s", sigmaResp.Body)
	}

	// Show a page that the person has been inserted
	return c.Render("proces-new-finished", fiber.Map{
		"vreemdelingNummer": c.Params("vreemdelingNummer"),
		"procesID":          procesID,
	})
}

func (a *Application) getProcesAttributes(data createOrUpdateProcesData) ([]sigma.ObservationCreate, error) {
	// Find the corresponding proces in the config
	found := false

	var proces config.Proces
	for _, pr := range a.Cfg.Processen {
		if pr.Name == data.Type {
			proces = pr
			found = true
			break
		}
	}

	if !found {
		return nil, fmt.Errorf("cannot find proces with name '%s'", data.Type)
	}

	// Range over all submitted attributes
	attrs := make([]sigma.ObservationCreate, 0, len(data.Attributes))
	for _, dataAttr := range data.Attributes {
		// Get the corresponding rubriek from the config
		found := false

		var attr *config.Attribute
		for i, at := range proces.Attributes {
			if at.Name == dataAttr.Name {
				attr = &proces.Attributes[i] // Note: instead of &at, which would be incorrect due to the loop
				found = true
				break
			}
		}

		if !found {
			continue // Skip if the attribute name/key is not found in the config
		}

		val, err := a.parseAttribute(dataAttr.Value, attr)
		if err != nil {
			return nil, fmt.Errorf("error parsing attribute '%s' (value: %v). error: %w", attr.Name, dataAttr.Value, err)
		}

		attrs = append(attrs, sigma.ObservationCreate{
			Attribute: attr.Name,
			Value:     val,
		})
	}

	return attrs, nil
}
