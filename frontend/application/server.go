package application

import (
	"bytes"
	"encoding/json"
	"html/template"
	"log"
	"slices"
	"sync"
	"time"

	"github.com/gofiber/contrib/websocket"
	"github.com/gofiber/fiber/v2"
	"github.com/gofiber/fiber/v2/middleware/logger"
	"github.com/gofiber/template/html/v2"
	gorillaWebsocket "github.com/gorilla/websocket"

	"gitlab.com/digilab.overheid.nl/ecosystem/migratieketen/frontend/helpers"
	notificationAPI "gitlab.com/digilab.overheid.nl/ecosystem/migratieketen/notification-api"
)

var connections = make(map[*websocket.Conn]struct{})
var connectionsMu sync.RWMutex // Note: instead of using sync.Map, since we prefer typed keys

func (a *Application) Listen() error {
	// Template engine
	engine := html.New("./views", ".html")

	// Connect to the notification server via WebSockets. Note: not doing this directly from the frontend, since 1) we want to be able to add role-based authentication, 2) this BFF uses views to pass the notifications via HTMX and 3) we want to add FSC in between later
	conn, _, err := gorillaWebsocket.DefaultDialer.Dial("ws://np-notification-backend-svc/api/v0/subscribe", nil)
	if err == nil {
		defer conn.Close()

		go func() {
			for {
				_, msg, err := conn.ReadMessage()
				if err != nil {
					log.Printf("error reading WebSocket message from notification server: %v", err)
					return
				}

				// Decode as CloudEvent
				var event notificationAPI.CloudEvent // Note: instead of using the CloudEvent type from github.com/cloudevents/sdk-go directly, since we want to make sure that CloudEvent version we unmarshal to matches the CloudEvent version used by the notification API
				if err := json.Unmarshal(msg, &event); err != nil {
					log.Printf("error unmarshalling WebSocket message from notification server as CloudEvent: %v", err)
					continue
				}

				// Send a message via WebSockets to all connected clients
				connectionsMu.RLock()
				for c := range connections {
					// Render the notification partial. IMPROVE: do this only once for each route suffix?
					var buffer bytes.Buffer
					if err := engine.Render(&buffer, "partials/notification", fiber.Map{
						"event":       event,
						"routeSuffix": c.Locals("routeSuffix"),
					}, ""); err != nil {
						log.Printf("error rendering notification: %v", err)
						continue
					}

					if err = c.WriteMessage(websocket.TextMessage, buffer.Bytes()); err != nil {
						log.Println("error writing WebSocket message to visitor:", err)
					}
				}
				connectionsMu.RUnlock()
			}
		}()
	} else {
		// Only log the error, do not return
		log.Printf("error connecting to notification server: %v", err)
	}

	// Add functions
	engine.AddFuncMap(template.FuncMap{
		"configOrganization": func() any { return a.Cfg.Organization },
		"configRoles":        func() any { return a.Cfg.Roles },
		"numberFormat":       helpers.NumberFormat,
		"formatTime":         helpers.FormatTime,
		"formatTimeValue": func(t time.Time) string {
			return helpers.FormatTime(&t)
		},
		"formatDate":             helpers.FormatDate,
		"formatYear":             helpers.FormatYear,
		"formatObservationValue": helpers.FormatObservationValue,
		"dict":                   helpers.Dict,
		"stringIn":               slices.Contains[[]string], // Note: similar to Hugo's 'in' function, but limited to string slices
	})

	// Fiber instance
	app := fiber.New(fiber.Config{
		Views:             engine,
		ViewsLayout:       "layouts/main",
		PassLocalsToViews: true, // Pass all variables set using c.Locals() to all views

		// Override default error handler
		ErrorHandler: a.ErrorHandler,
	})

	// Middleware
	app.Use(logger.New(logger.Config{
		Format: "${time} | ${status} | ${latency} | ${method} | ${path}   ${error}\n", // Do not log IP addresses. Note: see https://docs.gofiber.io/api/middleware/logger#constants for more logger variables
	}))

	// Routes
	app.Static("/", "./public")

	// Healthz endpoint
	app.Get("/healthz", func(c *fiber.Ctx) error {
		return c.SendString(".") // Send a period as response body, similar to chi's Heartbeat middleware
	})

	// security.txt redirect
	app.Get("/.well-known/security.txt", func(c *fiber.Ctx) error {
		return c.Redirect("https://www.ncsc.nl/.well-known/security.txt", fiber.StatusFound) // StatusFound is HTTP code 302
	})

	// Login page
	app.Get("/login", func(c *fiber.Ctx) error {
		return c.Render("login", fiber.Map{
			"intendedURL": c.Query("next", "/"),
		})
	})

	// For the remaining routes, ensure the visitor is logged in
	app.Use(a.RequireLogin)

	// Index page
	app.Get("/", func(c *fiber.Ctx) error {
		return c.Render("index", nil)
	})

	// Websocket middleware and route for notifications
	app.Use("/ws", func(c *fiber.Ctx) error {
		// IsWebSocketUpgrade returns true if the client requested upgrade to the WebSocket protocol
		if websocket.IsWebSocketUpgrade(c) {
			// c.Locals("allowed", true)
			return c.Next()
		}
		return fiber.ErrUpgradeRequired
	})

	app.Get("/ws", websocket.New(func(c *websocket.Conn) {
		// Add the connection to a map of connected clients
		connectionsMu.Lock()
		connections[c] = struct{}{}
		connectionsMu.Unlock()

		for {
			mt, msg, err := c.ReadMessage()
			if err != nil {
				if !websocket.IsCloseError(err, websocket.CloseNormalClosure, websocket.CloseGoingAway) {
					log.Println("read error:", err)
				}

				// Remove the connection from the map of connected clients
				connectionsMu.Lock()
				delete(connections, c)
				connectionsMu.Unlock()

				break
			}

			log.Printf("received WebSocket message of type %d: %s", mt, msg)
		}
	}))

	// Routes for vreemdelingen
	app.Post("/search", a.SearchVreemdeling)

	vreemdelingen := app.Group("/vreemdelingen")
	vreemdelingen.Get("/new", a.NewVreemdeling) // Note: since the vreemdelingNummer is a string representation of a number with a specific length constraints, it should never be equal to 'new'
	vreemdelingen.Post("/", a.CreateVreemdeling)
	vreemdelingen.Get("/:vreemdelingNummer", a.GetVreemdeling)
	vreemdelingen.Get("/:vreemdelingNummer/edit", a.EditVreemdeling)
	vreemdelingen.Post("/:vreemdelingNummer", a.UpdateVreemdeling)

	// Routes for processen
	processen := vreemdelingen.Group("/:vreemdelingNummer/processen")
	processen.Get("/new", a.NewProces)
	processen.Get("/new-details", a.NewProcesDetails)
	processen.Post("/", a.CreateProces)
	processen.Get("/:procesID", a.GetProces)
	processen.Get("/:procesID/edit", a.EditProces)
	processen.Post("/:procesID", a.UpdateProces)

	// Routes for merging vreemdelingen
	app.Get("/merge", func(c *fiber.Ctx) error {
		return c.Render("merge", fiber.Map{
			// TODO
		})
	})

	// Routes for notifications
	app.Get("/notifications", func(c *fiber.Ctx) error {
		return c.Render("notifications", fiber.Map{
			// TODO
		})
	})

	// Start the server
	return app.Listen(a.Cfg.ListenAddress)
}
