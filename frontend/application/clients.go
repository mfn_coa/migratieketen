package application

import (
	"context"
	"fmt"
	"net/http"

	bvv "gitlab.com/digilab.overheid.nl/ecosystem/migratieketen/fictief-bvv-api"
	loket "gitlab.com/digilab.overheid.nl/ecosystem/migratieketen/fictief-loket-api"
	sigma "gitlab.com/digilab.overheid.nl/ecosystem/migratieketen/fictief-sigma-api"
)

func (a *Application) getBVVClient() (client *bvv.ClientWithResponses, err error) {
	fscConfig := a.Cfg.FscConfig["fictief-np-bvv-backend"] // Note: the BVV is deployed centrally, within the NP organization

	client, err = bvv.NewClientWithResponses(
		fscConfig.Endpoint,
		bvv.WithRequestEditorFn(func(ctx context.Context, req *http.Request) error {
			req.Header.Set("Fsc-Grant-Hash", fscConfig.FscGrantHash)
			return nil
		}))

	return
}

func (a *Application) getSigmaClient() (client *sigma.ClientWithResponses, err error) {
	fscConfig := a.Cfg.FscConfig[fmt.Sprintf("fictief-%s-sigma-backend", a.Cfg.Organization.Name)]

	client, err = sigma.NewClientWithResponses(
		fscConfig.Endpoint,
		sigma.WithRequestEditorFn(func(ctx context.Context, req *http.Request) error {
			req.Header.Set("Fsc-Grant-Hash", fscConfig.FscGrantHash)
			return nil
		}))

	return
}

func (a *Application) getLoketClient() (client *loket.ClientWithResponses, err error) {
	fscConfig := a.Cfg.FscConfig[fmt.Sprintf("fictief-%s-loket-backend", a.Cfg.Organization.Name)]

	client, err = loket.NewClientWithResponses(
		fscConfig.Endpoint,
		loket.WithRequestEditorFn(func(ctx context.Context, req *http.Request) error {
			req.Header.Set("Fsc-Grant-Hash", fscConfig.FscGrantHash)
			return nil
		}))

	return
}
