openapi: 3.0.3
info:
  title: notification
  description: |-
    This is a notification API that serves as an example for the "Common Ground Migratieketen Fieldlab".
    It represents allows for publishing and subscribing to process notifications.
  contact:
    email: digilab@vng.nl
  license:
    name: EUPL 1.2
    url: https://eupl.eu/1.2/en/
  version: 1.0.0
servers:
  - url: https://notification.apps.digilab.network/api/v0
  - url: http://np-notification-backend-127.0.0.1.nip.io:8080/api/v0
tags:
  - name: notification
    description: Everything about notificationsDutch.
paths:
  /subscribe:
    get:
      tags:
        - notification
      summary: Subscribe to notifications
      description: Open a websocket to get notifications.
      operationId: subscribeNotification
      responses:
        "200":
          $ref: "#/components/responses/EmptyResponse"
  /publish:
    post:
      tags:
        - notification
      summary: Publish a notification
      description: Notifications are published to the central nats.
      operationId: publishNotification
      requestBody:
        $ref: "#/components/requestBodies/NotificationPublishRequest"
      responses:
        "200":
          $ref: "#/components/responses/EmptyResponse"
components:
  schemas:
    CloudEvent:
      required:
        - id
        - source
        - specversion
        - type
      type: object
      properties:
        specversion:
          type: string
          description: "CloudEvents 'specversion' attribute. The version of the CloudEvents\r\nspecification which the event uses. This enables the interpretation of the context."
        id:
          type: string
          description: " Identifies the event. Producers MUST ensure that source + id is unique for each distinct event. If a duplicate event is re-sent (e.g. due to a network error) it MAY have the same id. Consumers MAY assume that Events with identical source and id are duplicates.\r\nExamples:\r\n An event counter maintained by the producer\r\n A UUID\r\nConstraints:\r\n REQUIRED\r\n MUST be a non-empty string\r\n MUST be unique within the scope of the producer"
        source:
          type: string
          description: "Identifies the context in which an event happened. Often this will include information such as the type of the event source, the organization publishing the event or the process that produced the event. The exact syntax and semantics behind the data encoded in the URI is defined by the event producer.\r\n            \r\nProducers MUST ensure that source + id is unique for each distinct event.\r\n            \r\nAn application MAY assign a unique source to each distinct producer, which makes it easy to produce unique IDs since no other producer will have the same source. The application MAY use UUIDs, URNs, DNS authorities or an application-specific scheme to create unique source identifiers.\r\n            \r\nA source MAY include more than one producer. In that case the producers MUST collaborate to ensure that source + id is unique for each distinct event.\r\n            \r\nConstraints:\r\n            \r\n    REQUIRED\r\n    MUST be a non-empty URI-reference\r\n    An absolute URI is RECOMMENDED\r\n            \r\nExamples\r\n            \r\n    Internet-wide unique URI with a DNS authority.\r\n        https://github.com/cloudevents\r\n        mailto:cncf-wg-serverless @lists.cncf.io\r\n    Universally-unique URN with a UUID:\r\n        urn:uuid:6e8bc430-9c3a-11d9-9669-0800200c9a66\r\n    Application-specific identifiers\r\n        /cloudevents/spec/pull/123\r\n        /sensors/tn-1234567/alerts\r\n        1-555-123-4567"
          format: uri
        subject:
          type: string
          description: "CloudEvents 'subject' attribute. This describes the subject of the event in the context\r\nof the event producer (identified by source). In publish-subscribe scenarios, a subscriber\r\nwill typically subscribe to events emitted by a source, but the source identifier alone\r\nmight not be sufficient as a qualifier for any specific event if the source context has\r\ninternal sub-structure."
          nullable: true
        type:
          type: string
          description: "CloudEvents 'type' attribute. Type of occurrence which has happened.\r\nOften this attribute is used for routing, observability, policy enforcement, etc."
        time:
          type: string
          description: CloudEvents 'time' attribute. Timestamp of when the event happened.
          format: date-time
          nullable: true
        datacontenttype:
          type: string
          description: "Type: String per RFC 2046\r\nDescription: Content type of data value.This attribute enables data to carry any type of content, whereby format and encoding might differ from that of the chosen event format.For example, an event rendered using the JSON envelope format might carry an XML payload in data, and the consumer is informed by this attribute being set to \"application/xml\". The rules for how data content is rendered for different datacontenttype values are defined in the event format specifications; for example, the JSON event format defines the relationship in section 3.1.\r\n            \r\nFor some binary mode protocol bindings, this field is directly mapped to the respective protocol's content-type metadata property. Normative rules for the binary mode and the content-type metadata mapping can be found in the respective protocol\r\n            \r\nIn some event formats the datacontenttype attribute MAY be omitted.For example, if a JSON format event has no datacontenttype attribute, then it is implied that the data is a JSON value conforming to the \"application/json\" media type. In other words: a JSON-format event with no datacontenttype is exactly equivalent to one with datacontenttype = \"application/json\".\r\n     \r\n            \r\nWhen translating an event message with no datacontenttype attribute to a different format or protocol binding, the target datacontenttype SHOULD be set explicitly to the implied datacontenttype of the source.\r\n            \r\n            \r\nConstraints:\r\n            \r\n    OPTIONAL\r\n    If present, MUST adhere to the format specified in RFC 2046\r\n            \r\nFor Media Type examples see IANA Media Types"
          nullable: true
        dataschema:
          type: string
          description: "Type: URI\r\nDescription: Identifies the schema that data adheres to.Incompatible changes to the schema SHOULD be reflected by a different URI.See Versioning of Attributes in the Primer for more information.\r\nConstraints:\r\n    OPTIONAL\r\n    If present, MUST be a non-empty URI"
          format: uri
          nullable: true
        data:
          type: object
          additionalProperties: false
          description: "CloudEvent 'data' content.  The event payload. The payload depends on the type\r\nand the 'schemaurl'. It is encoded into a media format which is specified by the\r\n'contenttype' attribute (e.g. application/json)."
          nullable: true
      additionalProperties: false
    Error:
      description: The error that occured while processing this request.
      type: object
      properties:
        message:
          type: string
          example: foo went wrong
  requestBodies:
    NotificationPublishRequest:
      description: A notification publish request.
      required: true
      content:
        application/json:
          schema:
            type: object
            properties:
              data:
                $ref: "#/components/schemas/CloudEvent"
            required:
              - data
  responses:
    EmptyResponse:
      description: Nothing needs to be returned.
      content:
        application/json:
          schema:
            type: object
    BadRequestErrorResponse:
      description: Invalid input supplied.
      content:
        application/json:
          schema:
            type: object
            properties:
              errors:
                type: array
                items:
                  $ref: "#/components/schemas/Error"
            required:
              - errors
