-- name: ObservationCreate :exec
INSERT INTO sigma.observations
(id, vreemdeling_id, attribute, value)
VALUES (
    $1, $2, $3, $4
);

-- name: ObservationInvalidate :exec
UPDATE sigma.observations
SET deleted_at=NOW()
WHERE id=$1;

-- name: ObservationList :many
SELECT id, vreemdeling_id, attribute, value, created_at, deleted_at
FROM sigma.observations
WHERE vreemdeling_id=$1 AND attribute = ANY($2::TEXT[]);

-- name: ObservationGet :one
SELECT id, vreemdeling_id, attribute, value, created_at, deleted_at
FROM sigma.observations
WHERE id=$1;

-- name: ProcessGet :one
SELECT id, process_id, vreemdeling_id, type, status, created_at, deleted_at
FROM sigma.processes
WHERE process_id=$1;

-- name: ProcessCreate :exec
INSERT INTO sigma.processes
(id, process_id, vreemdeling_id, type, status)
VALUES (
    $1, $2, $3, $4, $5
);

-- name: ProcessUpdate :exec
UPDATE sigma.processes
SET status=$1
WHERE process_id=$2;

-- name: ProcessCount :one
SELECT COUNT(0) FROM sigma.processes;

-- name: AttributeCreate :exec
INSERT INTO sigma.process_attributes
(id, process_id, attribute, value)
VALUES (
    $1, $2, $3, $4
);

-- name: AttributeList :many
SELECT id, process_id, attribute, value, created_at, deleted_at
FROM sigma.process_attributes
WHERE process_id=$1;

-- name: AttributeSoftDelete :exec
UPDATE sigma.process_attributes
SET deleted_at=NOW()
WHERE id=$1;
