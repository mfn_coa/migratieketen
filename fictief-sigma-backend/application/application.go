package application

import (
	"context"
	"fmt"
	"log/slog"
	"net/http"

	"github.com/go-chi/chi/v5"
	"github.com/go-chi/chi/v5/middleware"
	"github.com/google/uuid"
	api "gitlab.com/digilab.overheid.nl/ecosystem/migratieketen/fictief-sigma-api"
	"gitlab.com/digilab.overheid.nl/ecosystem/migratieketen/fictief-sigma-backend/application/storage"
	"gitlab.com/digilab.overheid.nl/ecosystem/migratieketen/fictief-sigma-backend/config"
	notification "gitlab.com/digilab.overheid.nl/ecosystem/migratieketen/notification-api"
)

type Proces struct {
	config.Proces
	Attributes map[string]config.Attribute
	Statuses   map[string]config.ProcesStatus
}

type Application struct {
	*http.Server
	logger     *slog.Logger
	db         *storage.Database
	cfg        *config.Config
	source     string
	attributes map[string]config.Attribute
	processes  map[string]Proces
	notifier   *notification.Client
}

func New(logger *slog.Logger, cfg *config.Config, db *storage.Database) Application {
	attributes := map[string]config.Attribute{}

	for _, attribute := range cfg.GlobalConfig.Rubrieken {
		attributes[attribute.Name] = attribute
	}

	processes := map[string]Proces{}
	for _, process := range cfg.GlobalConfig.Processen {
		attributes := map[string]config.Attribute{}

		for _, attribute := range process.Attributes {
			attributes[attribute.Name] = attribute
		}

		statusen := map[string]config.ProcesStatus{}
		for _, status := range process.Statuses {
			statusen[status.Name] = status
		}

		processes[process.Name] = Proces{
			Proces:     process,
			Attributes: attributes,
			Statuses:   statusen,
		}
	}

	notifier, err := notification.NewClient(cfg.NotifierDomain.Endpoint)
	if err != nil {
		panic(err)
	}

	return Application{
		Server: &http.Server{
			Addr: cfg.BackendListenAddress,
		},
		logger:     logger,
		cfg:        cfg,
		db:         db,
		source:     cfg.Organization.Label,
		attributes: attributes,
		processes:  processes,
		notifier:   notifier,
	}
}

func (app *Application) Router() {
	r := chi.NewRouter()

	r.Use(middleware.Recoverer)
	r.Use(middleware.Heartbeat("/healthz"))
	r.Use(middleware.Logger)
	r.Use(middleware.SetHeader("Content-Type", "application/json"))

	apiHandler := api.NewStrictHandler(app, nil)
	r.Mount("/v0/", api.Handler(apiHandler))

	r.Group(func(r chi.Router) {
		r.Use(middleware.SetHeader("Content-Type", "application/yaml"))
		r.Get("/openapi.yaml", app.OpenAPI)
	})

	app.Server.Handler = r
}

type NotifyType string

const (
	ntCreate NotifyType = "create"
	ntUpdate NotifyType = "update"
)

func (app *Application) Notify(ntype NotifyType, data map[string]any) error {
	t := ""
	switch ntype {
	case ntCreate:
		t = "com.migratieketen.proces.toevoegen"
	case ntUpdate:
		t = "com.migratieketen.proces.wijzigen"
	}

	dct := "application/json"

	resp, err := app.notifier.PublishNotification(context.Background(), notification.PublishNotificationJSONRequestBody{
		Data: notification.CloudEvent{
			Data:            &data,
			Datacontenttype: &dct,
			Id:              uuid.NewString(),
			Source:          fmt.Sprintf("migratieketen/%s", app.cfg.Organization.Name),
			Specversion:     "1.0",
			Type:            t,
		},
	})

	if err != nil {
		return err
	}

	if resp.StatusCode != 200 {
		return fmt.Errorf("publish notification failed: statuscode %d status %s", resp.StatusCode, resp.Status)
	}

	return nil
}
