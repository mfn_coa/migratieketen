package application

import (
	"context"
	"encoding/json"
	"fmt"
	"time"

	"github.com/google/uuid"
	api "gitlab.com/digilab.overheid.nl/ecosystem/migratieketen/fictief-sigma-api"
	queries "gitlab.com/digilab.overheid.nl/ecosystem/migratieketen/fictief-sigma-backend/application/storage/queries/generated"
	"gitlab.com/digilab.overheid.nl/ecosystem/migratieketen/fictief-sigma-backend/config"
)

// Compile time check if generated ObservationCreateValue order is not changed.
// If one or more of the lines below dont compile some order has been mixed up in the OpenAPI spec
var _ map[string]interface{} = api.ObservationCreateValue0{}
var _ string = api.ObservationCreateValue1("")
var _ bool = api.ObservationCreateValue2(false)
var _ []interface{} = api.ObservationCreateValue3{}
var _ float32 = api.ObservationCreateValue4(0.0)

func validateDate(attribute config.Attribute, value api.ObservationCreate_Value) error {
	v, err := value.AsObservationCreateValue1()
	if err != nil {
		return fmt.Errorf("value cannot be parsed")
	}

	if _, err := time.Parse("2006-01-02", v); err != nil {
		return fmt.Errorf("value could not be parsed: %s", err)
	}

	return nil
}

func validateDatetime(attribute config.Attribute, value api.ObservationCreate_Value) error {
	v, err := value.AsObservationCreateValue1()
	if err != nil {
		return fmt.Errorf("value cannot be parsed")
	}

	if _, err := time.Parse(time.RFC3339, v); err != nil {
		return fmt.Errorf("value could not be parsed: %s", err)
	}

	return nil
}

func validateString(attribute config.Attribute, value api.ObservationCreate_Value) error {
	if _, err := value.AsObservationCreateValue1(); err != nil {
		return fmt.Errorf("value cannot be parsed")
	}

	return nil
}

func validateNumber(attribute config.Attribute, value api.ObservationCreate_Value) error {
	v, err := value.AsObservationCreateValue4()
	if err != nil {
		return fmt.Errorf("value cannot be parsed")
	}

	if attribute.Min != nil {
		if v < float32((*attribute.Min).(int)) {
			return fmt.Errorf("value less then allowed: %v", *attribute.Min)
		}
	}

	if attribute.Max != nil {
		if v > float32((*attribute.Max).(int)) {
			return fmt.Errorf("value more than allowed: %v", *attribute.Max)
		}
	}

	return nil
}

func validateBoolean(attribute config.Attribute, value api.ObservationCreate_Value) error {
	if _, err := value.AsObservationCreateValue2(); err != nil {
		return fmt.Errorf("value cannot be parsed")
	}

	return nil
}

func validateCheckboxes(attribute config.Attribute, value api.ObservationCreate_Value) error {
	values, err := value.AsObservationCreateValue3()
	if err != nil {
		return fmt.Errorf("value cannot be parsed")
	}

	for _, value := range values {
		found := false

		for _, option := range attribute.Options {
			if option.Name == value {
				found = true
				break
			}
		}

		if !found {
			return fmt.Errorf("value is not valid: %s", value)
		}
	}

	return nil
}

func validateRadio(attribute config.Attribute, value api.ObservationCreate_Value) error {
	values, err := value.AsObservationCreateValue3()
	if err != nil {
		return fmt.Errorf("value cannot be parsed")
	}

	for _, value := range values {
		found := false

		for _, option := range attribute.Options {
			if option.Name == value {
				found = true
				break
			}
		}

		if !found {
			return fmt.Errorf("value is not valid: %s", value)
		}
	}

	return nil
}

func validateType(attribute config.Attribute, value api.ObservationCreate_Value) error {
	switch attribute.Type {
	case "text", "textarea":
		return validateString(attribute, value)
	case "number":
		return validateNumber(attribute, value)
	case "boolean":
		return validateBoolean(attribute, value)
	case "checkboxes":
		return validateCheckboxes(attribute, value)
	case "radios":
		return validateRadio(attribute, value)
	case "select":
		if attribute.Multiple {
			return validateCheckboxes(attribute, value)
		} else {
			return validateRadio(attribute, value)
		}
	case "date":
		return validateDate(attribute, value)
	case "datetime":
		return validateDatetime(attribute, value)
	default:
		return fmt.Errorf("unknown datatype")
	}
}

func validateObservation(data api.ObservationCreate, attributes map[string]config.Attribute) error {
	attribute, ok := attributes[data.Attribute]
	if !ok {
		return fmt.Errorf("attribute does not exists")
	}

	if err := validateType(attribute, data.Value); err != nil {
		return err
	}

	return nil
}

// CreateObservationByVreemdelingenId implements api.StrictServerInterface.
func (app *Application) CreateObservationByVreemdelingenId(ctx context.Context, request api.CreateObservationByVreemdelingenIdRequestObject) (api.CreateObservationByVreemdelingenIdResponseObject, error) {
	if err := validateObservation(request.Body.Data, app.attributes); err != nil {
		errors := fmt.Sprintf("validation failed: %s", err.Error())

		return api.CreateObservationByVreemdelingenId400JSONResponse{
			BadRequestErrorResponseJSONResponse: api.BadRequestErrorResponseJSONResponse{
				Errors: []api.Error{
					{
						Message: &errors,
					},
				},
			},
		}, nil
	}

	value, err := json.Marshal(request.Body.Data.Value)
	if err != nil {
		return nil, fmt.Errorf("marshal attribute value failed: %w", err)
	}

	id := uuid.New()

	params := &queries.ObservationCreateParams{
		ID:            id,
		VreemdelingID: request.VreemdelingId,
		Attribute:     request.Body.Data.Attribute,
		Value:         value,
	}

	if err := app.db.Queries.ObservationCreate(ctx, params); err != nil {
		return nil, fmt.Errorf("attributes create failed: %w", err)
	}

	observation, err := app.FindObservation(ctx, id)
	if err != nil {
		return nil, err
	}

	return api.CreateObservationByVreemdelingenId201JSONResponse{
		ObservationCreateResponseJSONResponse: api.ObservationCreateResponseJSONResponse{
			Data: *observation,
		},
	}, nil
}

// DeleteObservationById implements api.StrictServerInterface.
func (app *Application) DeleteObservationById(ctx context.Context, request api.DeleteObservationByIdRequestObject) (api.DeleteObservationByIdResponseObject, error) {
	if err := app.db.Queries.ObservationInvalidate(ctx, request.ObservationId); err != nil {
		return nil, fmt.Errorf("attribute invalidate failed: %w", err)
	}

	return api.DeleteObservationById204JSONResponse{}, nil
}

// ReadVreemdelingById implements api.StrictServerInterface.
func (app *Application) ReadVreemdelingById(ctx context.Context, request api.ReadVreemdelingByIdRequestObject) (api.ReadVreemdelingByIdResponseObject, error) {
	vreemdeling, err := app.FindVreemdeling(ctx, request.VreemdelingId, request.Params.Attributes...)
	if err != nil {
		return nil, err
	}

	return api.ReadVreemdelingById200JSONResponse{
		VreemdelingResponseJSONResponse: api.VreemdelingResponseJSONResponse{
			Data: vreemdeling,
		},
	}, nil

}

func (app *Application) FindVreemdeling(ctx context.Context, vreemdelingId string, attributes ...string) (api.Vreemdeling, error) {
	params := &queries.ObservationListParams{
		VreemdelingID: vreemdelingId,
		Column2:       attributes,
	}

	records, err := app.db.Queries.ObservationList(ctx, params)
	if err != nil {
		return nil, fmt.Errorf("attribute list failed: %w", err)
	}

	vreemdeling := api.Vreemdeling{}
	for idx := range records {
		observation, err := ToObservation(records[idx], app.source)
		if err != nil {
			return nil, err
		}

		v, ok := vreemdeling[*observation.Attribute]
		if !ok {
			v.Observations = &[]api.Observation{}
		}

		*v.Observations = append(*v.Observations, *observation)
		v.Value = observation.Value

		vreemdeling[*observation.Attribute] = v
	}

	return vreemdeling, nil
}

func (app *Application) FindObservation(ctx context.Context, id uuid.UUID) (*api.Observation, error) {
	record, err := app.db.Queries.ObservationGet(ctx, id)
	if err != nil {
		return nil, fmt.Errorf("attribute get failed: %w", err)
	}

	observation, err := ToObservation(record, app.source)
	if err != nil {
		return nil, err
	}

	return observation, nil
}

func ToObservation(record *queries.SigmaObservation, source string) (*api.Observation, error) {
	value := new(interface{})
	if err := json.Unmarshal(record.Value, value); err != nil {
		return nil, fmt.Errorf("unmarshal attribute failed: %w", err)
	}

	var invalidatedAt *time.Time
	if record.DeletedAt.Valid {
		invalidatedAt = &record.DeletedAt.Time
	}

	return &api.Observation{
		Attribute:     &record.Attribute,
		CreatedAt:     &record.CreatedAt,
		Id:            &record.ID,
		InvalidatedAt: invalidatedAt,
		Source:        &source,
		Value:         value,
	}, nil
}
