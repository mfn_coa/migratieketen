package application

import (
	"context"
	"encoding/json"
	"fmt"
	"time"

	"github.com/google/uuid"
	api "gitlab.com/digilab.overheid.nl/ecosystem/migratieketen/fictief-sigma-api"
	queries "gitlab.com/digilab.overheid.nl/ecosystem/migratieketen/fictief-sigma-backend/application/storage/queries/generated"
)

func validateAttribute(data api.ObservationCreate, process Proces) error {
	attribute, ok := process.Attributes[data.Attribute]
	if !ok {
		return fmt.Errorf("attribute '%s' does not exists in process '%s'", data.Attribute, process.Label)
	}

	if err := validateType(attribute, data.Value); err != nil {
		return err
	}

	return nil
}

// CreateAttributeByProcesId implements api.StrictServerInterface.
func (app *Application) CreateAttributeByProcesId(ctx context.Context, request api.CreateAttributeByProcesIdRequestObject) (api.CreateAttributeByProcesIdResponseObject, error) {
	process, err := app.db.Queries.ProcessGet(ctx, request.ProcesId)
	if err != nil {
		return nil, fmt.Errorf("process get failed: %w", err)
	}

	if err := validateAttribute(request.Body.Data, app.processes[process.Type]); err != nil {
		errors := fmt.Sprintf("validation failed: %s", err.Error())

		return api.CreateAttributeByProcesId400JSONResponse{
			BadRequestErrorResponseJSONResponse: api.BadRequestErrorResponseJSONResponse{
				Errors: []api.Error{{
					Message: &errors,
				}},
			},
		}, nil
	}

	value, err := json.Marshal(request.Body.Data.Value)
	if err != nil {
		return nil, fmt.Errorf("attribute marshal failed: %w", err)
	}

	if err := app.db.Queries.AttributeCreate(ctx, &queries.AttributeCreateParams{
		ID:        uuid.New(),
		ProcessID: process.ID,
		Attribute: request.Body.Data.Attribute,
		Value:     value,
	}); err != nil {
		return nil, fmt.Errorf("attribute create failed: %w", err)
	}

	return api.CreateAttributeByProcesId201JSONResponse{}, nil
}

func validateProcess(data api.ProcesCreate, processes map[string]Proces) error {
	process, ok := processes[data.Type]
	if !ok {
		return fmt.Errorf("process '%s' does not exists", data.Type)
	}

	if _, ok := process.Statuses[data.Status]; !ok {
		return fmt.Errorf("status '%s' does not exists in process '%s'", data.Status, process.Label)
	}

	for _, attribute := range data.Attributes {
		attr, ok := process.Attributes[attribute.Attribute]
		if !ok {
			return fmt.Errorf("attribute '%s' does not exists in process '%s'", attribute.Attribute, process.Label)
		}

		if err := validateType(attr, attribute.Value); err != nil {
			return err
		}
	}
	return nil
}

// CreateProcesByVreemdelingenId implements api.StrictServerInterface.
func (app *Application) CreateProcesByVreemdelingenId(ctx context.Context, request api.CreateProcesByVreemdelingenIdRequestObject) (api.CreateProcesByVreemdelingenIdResponseObject, error) {
	if err := validateProcess(request.Body.Data, app.processes); err != nil {
		errors := fmt.Sprintf("validation failed: %s", err.Error())

		return api.CreateProcesByVreemdelingenId400JSONResponse{
			BadRequestErrorResponseJSONResponse: api.BadRequestErrorResponseJSONResponse{
				Errors: []api.Error{
					{
						Message: &errors,
					},
				},
			},
		}, nil
	}

	count, err := app.db.Queries.ProcessCount(ctx)
	if err != nil {
		return nil, fmt.Errorf("process count failed: %s", err)
	}

	processID := fmt.Sprintf("%010d", count+1)

	id := uuid.New()
	if err := app.db.Queries.ProcessCreate(ctx, &queries.ProcessCreateParams{
		ID:            id,
		ProcessID:     processID,
		VreemdelingID: request.VreemdelingId,
		Type:          request.Body.Data.Type,
		Status:        request.Body.Data.Status,
	}); err != nil {
		return nil, fmt.Errorf("process create failed: %w", err)
	}

	for _, attribute := range request.Body.Data.Attributes {
		value, err := json.Marshal(attribute.Value)
		if err != nil {
			return nil, fmt.Errorf("attribute marshal failed: %w", err)
		}

		if err := app.db.Queries.AttributeCreate(ctx, &queries.AttributeCreateParams{
			ID:        uuid.New(),
			ProcessID: id,
			Attribute: attribute.Attribute,
			Value:     value,
		}); err != nil {
			return nil, fmt.Errorf("attribute create failed: %w", err)
		}
	}

	data := map[string]any{
		"procesId":      processID,
		"vreemdelingId": request.VreemdelingId,
	}

	if err := app.Notify(ntCreate, data); err != nil {
		return nil, fmt.Errorf("notify failed: %w", err)
	}

	return api.CreateProcesByVreemdelingenId201JSONResponse{}, nil
}

func validateProcesUpdate(data api.ProcesUpdate, process Proces) error {
	if _, ok := process.Statuses[data.Status]; !ok {
		return fmt.Errorf("status '%s' does not exists in process '%s'", data.Status, process.Label)
	}

	return nil
}

// UpdateProcesById implements api.StrictServerInterface.
func (app *Application) UpdateProcesById(ctx context.Context, request api.UpdateProcesByIdRequestObject) (api.UpdateProcesByIdResponseObject, error) {
	process, err := app.db.Queries.ProcessGet(ctx, request.ProcesId)
	if err != nil {
		return nil, err
	}

	if err := validateProcesUpdate(request.Body.Data, app.processes[process.Type]); err != nil {
		return nil, err
	}

	if err := app.db.Queries.ProcessUpdate(ctx, &queries.ProcessUpdateParams{
		Status:    request.Body.Data.Status,
		ProcessID: request.ProcesId,
	}); err != nil {
		return nil, err
	}

	data := map[string]any{
		"procesId":      process.ProcessID,
		"vreemdelingId": process.VreemdelingID,
	}

	if err := app.Notify(ntUpdate, data); err != nil {
		return nil, fmt.Errorf("notify failed: %w", err)
	}

	return api.UpdateProcesById204JSONResponse{}, err
}

// DeleteAttributeById implements api.StrictServerInterface.
func (app *Application) DeleteAttributeById(ctx context.Context, request api.DeleteAttributeByIdRequestObject) (api.DeleteAttributeByIdResponseObject, error) {
	if err := app.db.Queries.AttributeSoftDelete(ctx, request.AttributeId); err != nil {
		return nil, fmt.Errorf("process attributes soft delete failed: %w", err)
	}
	return api.DeleteAttributeById204JSONResponse{}, nil
}

// ReadProcesById implements api.StrictServerInterface.
func (app *Application) ReadProcesById(ctx context.Context, request api.ReadProcesByIdRequestObject) (api.ReadProcesByIdResponseObject, error) {
	record, err := app.db.Queries.ProcessGet(ctx, request.ProcesId)
	if err != nil {
		return nil, err
	}

	attributeRecords, err := app.db.Queries.AttributeList(ctx, record.ID)
	if err != nil {
		return nil, err
	}

	process := ToProcess(record)
	process.Attributes = ToAttributes(attributeRecords)

	return api.ReadProcesById200JSONResponse{
		ProcesResponseJSONResponse: api.ProcesResponseJSONResponse{
			Data: *process,
		},
	}, nil
}

func ToProcess(record *queries.SigmaProcess) *api.Proces {
	var deletedAt *time.Time
	if record.DeletedAt.Valid {
		deletedAt = &record.DeletedAt.Time
	}

	return &api.Proces{
		CreatedAt: record.CreatedAt,
		DeletedAt: deletedAt,
		Id:        record.ID,
		Status:    record.Status,
		Type:      record.Type,
	}
}

func ToAttribute(record *queries.SigmaProcessAttribute) *api.Attribute {
	var deletedAt *time.Time
	if record.DeletedAt.Valid {
		deletedAt = &record.DeletedAt.Time
	}

	return &api.Attribute{
		Attribute:     record.Attribute,
		CreatedAt:     record.CreatedAt,
		Id:            record.ID,
		InvalidatedAt: deletedAt,
		Value:         record.Value,
	}
}

func ToAttributes(records []*queries.SigmaProcessAttribute) api.ProcesAttribute {
	attributes := api.ProcesAttribute{}

	for idx := range records {
		attribute := ToAttribute(records[idx])

		v, ok := attributes[attribute.Attribute]
		if !ok {
			v.Attributes = []api.Attribute{}
		}

		v.Attributes = append(v.Attributes, *attribute)
		if attribute.InvalidatedAt == nil {
			v.Value = &attribute.Value
		}

		attributes[attribute.Attribute] = v
	}

	return attributes
}
