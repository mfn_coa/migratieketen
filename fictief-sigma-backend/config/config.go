package config

import (
	"fmt"
	"os"
	"path/filepath"
	"strings"

	"github.com/iamolegga/enviper"
	"github.com/spf13/viper"
	"gopkg.in/yaml.v3"
)

type Role struct {
	Name        string `yaml:"name"`
	Label       string `yaml:"label"`
	Permissions struct {
		BVVWriteAccess bool `yaml:"bvvWriteAccess"`

		Rubrieken struct {
			Get []string `yaml:"get"` // List of field names, also below
			Set []string `yaml:"set"`
		} `yaml:"rubrieken"`

		Processen map[string]struct { // Map keys are the proces names/IDs
			Get []string `yaml:"get"`
			Set []string `yaml:"set"`
		} `yaml:"processen"`
	} `yaml:"permissions"`
}

type Attribute struct {
	Name     string      `yaml:"name"`
	Label    string      `yaml:"label"`
	Type     string      `yaml:"type"`
	Multiple bool        `yaml:"multiple"` // For types 'select' and 'file'
	Default  interface{} `yaml:"default"`

	// For number types
	Min  *interface{} `yaml:"min"`
	Max  *interface{} `yaml:"max"`
	Step *interface{} `yaml:"step"`

	// For select, radios, and checkboxes types
	Options []struct {
		Name  string `yaml:"name"`
		Label string `yaml:"label"`
	} `yaml:"options"`
}

type ProcesStatus struct {
	Name  string `yaml:"name"`
	Label string `yaml:"label"`
}

type Proces struct {
	Name     string         `yaml:"name"`
	Label    string         `yaml:"label"`
	Statuses []ProcesStatus `yaml:"statuses"`

	Attributes []Attribute `yaml:"attributes"`
}

type GlobalConfig struct {
	Rubrieken []Attribute `yaml:"rubrieken"`

	Processen []Proces `yaml:"processen"`
}

type OrganizationConfig struct {
	Organization struct {
		Name           string `yaml:"name"`
		Label          string `yaml:"label"`
		BVVWriteAccess bool   `yaml:"bvvWriteAccess"`
	} `yaml:"organization"`

	Roles []Role

	ListenAddress string `yaml:"listenAddress"`
}
type Config struct {
	GlobalConfig         `mapstructure:",squash"`
	OrganizationConfig   `mapstructure:",squash"`
	BackendListenAddress string
	PostgresDsn          string `mapstructure:"POSTGRES_DSN"`
	NotifierDomain       Domain
}

type Domain struct {
	Endpoint     string
	FscGrantHash string
}

// New composes a config with values from the specified paths
func New(globalConfigPath string, configPath string) (*Config, error) {
	e := enviper.New(viper.New())
	e.SetEnvPrefix("MK")
	e.SetDefault("BACKENDLISTENADDRESS", ":8080")

	path, file := getPathAndFile(configPath)
	e.AddConfigPath(path)
	e.SetConfigName(file)

	if err := e.ReadInConfig(); err != nil {
		panic(err)
	}

	config := new(Config)
	if err := e.Unmarshal(config); err != nil {
		panic("unable to decode into config struct")
	}

	if err := readConfig(globalConfigPath, &config.GlobalConfig); err != nil {
		return nil, err
	}

	return config, nil
}

func getPathAndFile(path string) (string, string) {
	p := filepath.Dir(path)
	base := filepath.Base(path)
	ext := filepath.Ext(base)
	file, _ := strings.CutSuffix(base, ext)

	return p, file
}

func readConfig(path string, cfg any) error {
	file, err := os.ReadFile(path)
	if err != nil {
		return fmt.Errorf("error reading config file: %w", err)
	}

	if err := yaml.Unmarshal(file, cfg); err != nil {
		return fmt.Errorf("unmarshalling config failed: %w", err)
	}

	return nil
}
