package cmd

import (
	"log/slog"
	"os"

	"github.com/spf13/cobra"
	"gitlab.com/digilab.overheid.nl/ecosystem/migratieketen/fictief-sigma-backend/application"
	"gitlab.com/digilab.overheid.nl/ecosystem/migratieketen/fictief-sigma-backend/application/storage"
	"gitlab.com/digilab.overheid.nl/ecosystem/migratieketen/fictief-sigma-backend/config"
)

const (
	PSQLSchemaName = "sigma"
)

var serveOpts struct { //nolint:gochecknoglobals // this is the recommended way to use cobra
	ConfigPath string
}

func init() { //nolint:gochecknoinits,gocyclo // this is the recommended way to use cobra
	flags := serveCommand.Flags()
	flags.StringVarP(&serveOpts.ConfigPath, "mk-config-path", "", "", "Location of the config")

	if err := serveCommand.MarkFlagRequired("mk-config-path"); err != nil {
		panic(err)
	}
}

var serveCommand = &cobra.Command{ //nolint:gochecknoglobals // this is the recommended way to use cobra
	Use:   "serve",
	Short: "Start the api",
	Run: func(cmd *cobra.Command, args []string) {
		logger := slog.New(slog.NewJSONHandler(os.Stdout, &slog.HandlerOptions{})).With("application", "http_server")

		logger.Info("Starting fictief sigma backend")

		cfg, err := config.New("../config/global.yaml", serveOpts.ConfigPath)
		if err != nil {
			logger.Error("config new failed", "err", err)
			return
		}

		logger.Info("Starting with config", "config", cfg)

		if err := migrateInit(cfg.PostgresDsn); err != nil {
			logger.Error("migrate init failed", "err", err)
			return
		}

		if err := storage.PostgresPerformMigrations(cfg.PostgresDsn, PSQLSchemaName); err != nil {
			logger.Error("failed to migrate db", "err", err)
			return
		}

		db, err := storage.New(cfg.PostgresDsn)
		if err != nil {
			logger.Error("failed to connect to the database", "err", err)
			return
		}

		app := application.New(logger, cfg, db)

		app.Router()

		if err := app.ListenAndServe(); err != nil {
			logger.Error("listen and serve failed", "err", err)
			return
		}

		os.Exit(0)
	},
}
