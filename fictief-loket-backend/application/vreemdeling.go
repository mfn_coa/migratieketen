package application

import (
	"context"
	"fmt"
	"net/http"
	"slices"
	"time"

	"github.com/google/uuid"
	bvv_api "gitlab.com/digilab.overheid.nl/ecosystem/migratieketen/fictief-bvv-api"
	api "gitlab.com/digilab.overheid.nl/ecosystem/migratieketen/fictief-loket-api"
	sigma_api "gitlab.com/digilab.overheid.nl/ecosystem/migratieketen/fictief-sigma-api"
	"golang.org/x/exp/maps"
)

// contains just the value and metadata of an observation, to be used in a situation where attribute and source are known.
type bareObservationValue struct {
	Id            uuid.UUID
	Value         any
	CreatedAt     time.Time
	InvalidatedAt *time.Time
}

var supportedAttributes = map[string]bool{
	"alias":      true,
	"spreektaal": true,
}

// Implements api.StrictServerInterface.
func (app *Application) ReadVreemdelingById(ctx context.Context, request api.ReadVreemdelingByIdRequestObject) (api.ReadVreemdelingByIdResponseObject, error) {
	// Verify and parse request:
	vnr := request.VreemdelingId
	attributes := map[string]bool{} // Set of requested attributes
	for _, f := range request.Params.Attributes {
		if !supportedAttributes[f] {
			return api.ReadVreemdelingById400JSONResponse{
				BadRequestErrorResponseJSONResponse: api.BadRequestErrorResponseJSONResponse{
					Errors: []api.Error{{Message: fmt.Sprintf("Attribute '%s' not supported in attributes parameter", f)}},
				},
			}, nil
		}
		attributes[f] = true
	}
	var sources []string
	if request.Params.Sources == nil || slices.Equal(*request.Params.Sources, api.QuerySources{""}) { // RH: ATTN: note that `?sources=` creates a slice with a single empty string, NOT an empty slice.
		sources = maps.Keys(app.cfg.SigmaSourceUrls)
	} else {
		sources = *request.Params.Sources
	}

	bvv, err := bvv_api.NewClientWithResponses(app.cfg.BvvApiServerUrl)
	if err != nil {
		panic(err)
	}
	vrResp, err := bvv.ReadVreemdelingByIdWithResponse(ctx, vnr)
	if err != nil {
		panic(err)
	}
	if vrResp.StatusCode() == http.StatusGone {
		return api.ReadVreemdelingById410JSONResponse{
			GoneErrorResponseJSONResponse: api.GoneErrorResponseJSONResponse{
				Errors: []api.Error{{Message: "VreemdelingID not found"}},
			},
		}, nil
	}
	if vrResp.JSON200 == nil {
		panic("failed to call BVV")
	}

	// Query sigma sources:
	obssByAttrAndSrc := map[string]map[string][]bareObservationValue{}
	for _, src := range sources {
		// RH: TODO: parallelize in goroutines
		sigma, err := sigma_api.NewClientWithResponses(app.cfg.SigmaSourceUrls[src])
		if err != nil {
			panic(err)
		}
		vrResp, err := sigma.ReadVreemdelingByIdWithResponse(ctx, vnr, &sigma_api.ReadVreemdelingByIdParams{
			Attributes: maps.Keys(attributes),
		})
		if err != nil {
			panic(err)
		}
		if vrResp.JSON200 == nil {
			panic(fmt.Sprintf("failed to call Sigma source '%s': %d", src, vrResp.StatusCode()))
		}
		for attr := range attributes {
			if _, exists := obssByAttrAndSrc[attr]; !exists {
				obssByAttrAndSrc[attr] = map[string][]bareObservationValue{}
			}
			if vrAttr, exists := vrResp.JSON200.Data[attr]; exists {
				for _, obs := range *vrAttr.Observations {
					obssByAttrAndSrc[attr][src] = append(obssByAttrAndSrc[attr][src], bareObservationValue{
						Id:            *obs.Id,
						Value:         obs.Value,
						CreatedAt:     *obs.CreatedAt,
						InvalidatedAt: obs.InvalidatedAt,
					})
				}
			}
		}
	}

	vreemdeling := &api.VreemdelingWithObservations{
		Id:                   vnr,
		AdditionalProperties: make(map[string]api.AttributeWithObservations, len(attributes)),
	}
	// Merge attribute observations from different sources:
	// RH: ATTN: this can't be completely generic, because merge logic might be different for different attributes.
	for attr := range attributes {
		if obssBySrc, exists := obssByAttrAndSrc[attr]; exists {
			attrWithObs := api.AttributeWithObservations{}
			for src, obss := range obssBySrc {
				for _, obs := range obss {
					attrWithObs.Observations = append(attrWithObs.Observations, api.Observation{
						Id:            obs.Id,
						Attribute:     attr,
						Value:         obs.Value,
						CreatedAt:     obs.CreatedAt,
						InvalidatedAt: obs.InvalidatedAt,
						Source:        src,
					})
					// RH: TODO: merge logic. Currently just takes the "last" one of whatever source is returned last by the map.
					attrWithObs.Value = obs.Value
				}
			}
			vreemdeling.AdditionalProperties[attr] = attrWithObs
		}
	}
	return api.ReadVreemdelingById200JSONResponse{
		VreemdelingWithObservationsResponseJSONResponse: api.VreemdelingWithObservationsResponseJSONResponse{
			Data: *vreemdeling,
		},
	}, nil
}
