package application

import (
	"log/slog"
	"net/http"

	"github.com/go-chi/chi/v5"
	"github.com/go-chi/chi/v5/middleware"

	api "gitlab.com/digilab.overheid.nl/ecosystem/migratieketen/fictief-loket-api"
	"gitlab.com/digilab.overheid.nl/ecosystem/migratieketen/fictief-loket-backend/config"
)

type Application struct {
	*http.Server
	logger *slog.Logger
	cfg    *config.Config
}

func New(logger *slog.Logger, cfg *config.Config) Application {
	return Application{
		Server: &http.Server{
			Addr: cfg.BackendListenAddress,
		},
		logger: logger,
		cfg:    cfg,
	}
}

func (app *Application) Router() {
	r := chi.NewRouter()

	r.Use(middleware.Recoverer)
	r.Use(middleware.Heartbeat("/healthz"))
	r.Use(middleware.Logger)
	r.Use(middleware.SetHeader("Content-Type", "application/json"))

	apiHandler := api.NewStrictHandler(app, nil)
	r.Mount("/v0/", api.Handler(apiHandler))

	r.Group(func(r chi.Router) {
		r.Use(middleware.SetHeader("Content-Type", "application/yaml"))
		r.Get("/openapi.yaml", app.OpenAPI)
	})

	app.Server.Handler = r
}
