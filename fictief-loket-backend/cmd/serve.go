package cmd

import (
	"log/slog"
	"os"

	"github.com/spf13/cobra"
	"github.com/spf13/viper"
	"gitlab.com/digilab.overheid.nl/ecosystem/migratieketen/fictief-loket-backend/application"
	"gitlab.com/digilab.overheid.nl/ecosystem/migratieketen/fictief-loket-backend/config"
)

var serveOpts struct { //nolint:gochecknoglobals // this is the recommended way to use cobra
	ConfigPath string
}

func init() { //nolint:gochecknoinits,gocyclo // this is the recommended way to use cobra
	viper.AutomaticEnv()
	flags := serveCommand.Flags()

	flags.StringVarP(&serveOpts.ConfigPath, "config-path", "", "./config/config.yaml", "Location of the config")
	viper.BindPFlag("config_path", flags.Lookup("config-path"))
}

var serveCommand = &cobra.Command{ //nolint:gochecknoglobals // this is the recommended way to use cobra
	Use:   "serve",
	Short: "Start the api",
	Run: func(cmd *cobra.Command, args []string) {
		logger := slog.New(slog.NewJSONHandler(os.Stdout, &slog.HandlerOptions{})).With("application", "http_server")

		logger.Info("Starting fictief loket backend")

		cfg, err := config.New(serveOpts.ConfigPath)
		if err != nil {
			logger.Error("config new failed", "err", err)
			return
		}

		logger.Info("Starting with config", "config", cfg)

		app := application.New(logger, cfg)

		app.Router()

		if err := app.ListenAndServe(); err != nil {
			logger.Error("listen and serve failed", "err", err)
			return
		}

		os.Exit(0)
	},
}
