package config

import (
	"github.com/iamolegga/enviper"
	"github.com/spf13/viper"
)

type Config struct {
	BackendListenAddress string
	BvvApiServerUrl      string
	SigmaSourceUrls      map[string]string
}

func New(configPath string) (*Config, error) {
	e := enviper.New(viper.New())
	e.AllowEmptyEnv(true)
	e.AutomaticEnv()
	e.SetEnvPrefix("APP")
	e.SetDefault("BACKENDLISTENADDRESS", ":8080")
	// RH: TODO: don't hardcode defaults, this should come from the config file.
	e.SetDefault("SIGMASOURCEURLS", map[string]string{
		"coa": "http://np-sigma-backend-svc/v0",
		"ind": "http://np-sigma-backend-svc/v0",
	})
	config := new(Config)
	if err := e.Unmarshal(config); err != nil {
		panic("unable to decode into config struct")
	}
	if !e.IsSet("BVVAPISERVERURL") {
		panic("missing mandatory APP_BVVAPISERVERURL config")
	}
	if !e.IsSet("SIGMASOURCEURLS") {
		panic("missing mandatory APP_SIGMASOURCEURLS config")
	}
	return config, nil
}
