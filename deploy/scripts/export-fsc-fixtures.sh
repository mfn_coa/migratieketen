#!/usr/bin/env bash

set -euo pipefail

kubectl -n rrd exec -it -c postgres rrd-common-db-1 -- /usr/bin/pg_dump -xO nlx_manager > deploy/fixtures/rrd-fsc-manager.sql
kubectl -n rrd exec -it -c postgres rrd-common-db-1 -- /usr/bin/pg_dump -xO nlx_controller > deploy/fixtures/rrd-fsc-controller.sql
kubectl -n migratieketen exec -it -c postgres shared-common-db-1 -- /usr/bin/pg_dump -xO nlx_manager > deploy/fixtures/shared-fsc-manager.sql
kubectl -n migratieketen exec -it -c postgres shared-common-db-1 -- /usr/bin/pg_dump -xO nlx_controller > deploy/fixtures/shared-fsc-controller.sql
