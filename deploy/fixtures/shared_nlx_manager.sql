--
-- PostgreSQL database dump
--

-- Dumped from database version 16.0 (Debian 16.0-1.pgdg110+1)
-- Dumped by pg_dump version 16.0 (Debian 16.0-1.pgdg110+1)

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: contracts; Type: SCHEMA; Schema: -; Owner: -
--

CREATE SCHEMA contracts;


--
-- Name: peers; Type: SCHEMA; Schema: -; Owner: -
--

CREATE SCHEMA peers;


--
-- Name: content_hash_algorithm; Type: TYPE; Schema: contracts; Owner: -
--

CREATE TYPE contracts.content_hash_algorithm AS ENUM (
    'sha3_512'
);


--
-- Name: signature_type; Type: TYPE; Schema: contracts; Owner: -
--

CREATE TYPE contracts.signature_type AS ENUM (
    'accept',
    'reject',
    'revoke'
);


SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- Name: content; Type: TABLE; Schema: contracts; Owner: -
--

CREATE TABLE contracts.content (
    hash character varying(150) NOT NULL,
    hash_algorithm contracts.content_hash_algorithm NOT NULL,
    id uuid NOT NULL,
    group_id character varying(255) NOT NULL,
    valid_not_before timestamp with time zone NOT NULL,
    valid_not_after timestamp with time zone NOT NULL,
    created_at timestamp with time zone NOT NULL
);


--
-- Name: contract_signatures; Type: TABLE; Schema: contracts; Owner: -
--

CREATE TABLE contracts.contract_signatures (
    content_hash character varying(150) NOT NULL,
    signature_type contracts.signature_type NOT NULL,
    peer_id character varying(20) NOT NULL,
    certificate_thumbprint character varying(512) NOT NULL,
    signature character varying(2048) NOT NULL,
    signed_at timestamp with time zone NOT NULL
);


--
-- Name: grants_delegated_service_connection; Type: TABLE; Schema: contracts; Owner: -
--

CREATE TABLE contracts.grants_delegated_service_connection (
    hash character varying(150) NOT NULL,
    content_hash character varying(150) NOT NULL,
    outway_peer_id character varying(20) NOT NULL,
    delegator_peer_id character varying(20) NOT NULL,
    service_peer_id character varying(20) NOT NULL,
    service_name character varying(255) NOT NULL,
    certificate_thumbprint character varying(512) NOT NULL
);


--
-- Name: grants_delegated_service_publication; Type: TABLE; Schema: contracts; Owner: -
--

CREATE TABLE contracts.grants_delegated_service_publication (
    hash character varying(150) NOT NULL,
    content_hash character varying(150) NOT NULL,
    directory_peer_id character varying(20) NOT NULL,
    delegator_peer_id character varying(20) NOT NULL,
    service_peer_id character varying(20) NOT NULL,
    service_name character varying(255) NOT NULL,
    service_protocol character varying(255) NOT NULL
);


--
-- Name: grants_peer_registration; Type: TABLE; Schema: contracts; Owner: -
--

CREATE TABLE contracts.grants_peer_registration (
    hash character varying(150) NOT NULL,
    content_hash character varying(150) NOT NULL,
    directory_peer_id character varying(20) NOT NULL,
    peer_id character varying(20) NOT NULL,
    peer_name character varying(255) NOT NULL
);


--
-- Name: grants_service_connection; Type: TABLE; Schema: contracts; Owner: -
--

CREATE TABLE contracts.grants_service_connection (
    hash character varying(150) NOT NULL,
    content_hash character varying(150) NOT NULL,
    consumer_peer_id character varying(20) NOT NULL,
    service_peer_id character varying(20) NOT NULL,
    service_name character varying(255) NOT NULL,
    certificate_thumbprint character varying(512) NOT NULL
);


--
-- Name: grants_service_publication; Type: TABLE; Schema: contracts; Owner: -
--

CREATE TABLE contracts.grants_service_publication (
    hash character varying(150) NOT NULL,
    content_hash character varying(150) NOT NULL,
    directory_peer_id character varying(20) NOT NULL,
    service_peer_id character varying(20) NOT NULL,
    service_name character varying(255) NOT NULL,
    service_protocol character varying(255) NOT NULL
);


--
-- Name: valid_contracts; Type: VIEW; Schema: contracts; Owner: -
--

CREATE VIEW contracts.valid_contracts AS
 SELECT hash
   FROM contracts.content content
  WHERE ((valid_not_before < now()) AND (valid_not_after > now()) AND (( SELECT count(*) AS count
           FROM contracts.contract_signatures s
          WHERE ((s.signature_type = 'reject'::contracts.signature_type) AND ((s.content_hash)::text = (content.hash)::text))) = 0) AND (( SELECT count(*) AS count
           FROM contracts.contract_signatures s
          WHERE ((s.signature_type = 'revoke'::contracts.signature_type) AND ((s.content_hash)::text = (content.hash)::text))) = 0) AND (( SELECT count(DISTINCT p.id) AS count
           FROM ( SELECT grants_peer_registration.directory_peer_id AS id
                   FROM contracts.grants_peer_registration
                  WHERE ((grants_peer_registration.content_hash)::text = (content.hash)::text)
                UNION
                 SELECT grants_peer_registration.peer_id AS id
                   FROM contracts.grants_peer_registration
                  WHERE ((grants_peer_registration.content_hash)::text = (content.hash)::text)
                UNION
                 SELECT grants_service_publication.directory_peer_id AS id
                   FROM contracts.grants_service_publication
                  WHERE ((grants_service_publication.content_hash)::text = (content.hash)::text)
                UNION
                 SELECT grants_service_publication.service_peer_id AS id
                   FROM contracts.grants_service_publication
                  WHERE ((grants_service_publication.content_hash)::text = (content.hash)::text)
                UNION
                 SELECT grants_service_connection.consumer_peer_id AS id
                   FROM contracts.grants_service_connection
                  WHERE ((grants_service_connection.content_hash)::text = (content.hash)::text)
                UNION
                 SELECT grants_service_connection.service_peer_id AS id
                   FROM contracts.grants_service_connection
                  WHERE ((grants_service_connection.content_hash)::text = (content.hash)::text)
                UNION
                 SELECT grants_service_connection.service_peer_id AS id
                   FROM contracts.grants_service_connection
                  WHERE ((grants_service_connection.content_hash)::text = (content.hash)::text)
                UNION
                 SELECT grants_delegated_service_connection.service_peer_id AS id
                   FROM contracts.grants_delegated_service_connection
                  WHERE ((grants_delegated_service_connection.content_hash)::text = (content.hash)::text)
                UNION
                 SELECT grants_delegated_service_connection.outway_peer_id AS id
                   FROM contracts.grants_delegated_service_connection
                  WHERE ((grants_delegated_service_connection.content_hash)::text = (content.hash)::text)
                UNION
                 SELECT grants_delegated_service_connection.delegator_peer_id AS id
                   FROM contracts.grants_delegated_service_connection
                  WHERE ((grants_delegated_service_connection.content_hash)::text = (content.hash)::text)
                UNION
                 SELECT grants_delegated_service_publication.service_peer_id AS id
                   FROM contracts.grants_delegated_service_publication
                  WHERE ((grants_delegated_service_publication.content_hash)::text = (content.hash)::text)
                UNION
                 SELECT grants_delegated_service_publication.directory_peer_id AS id
                   FROM contracts.grants_delegated_service_publication
                  WHERE ((grants_delegated_service_publication.content_hash)::text = (content.hash)::text)
                UNION
                 SELECT grants_delegated_service_publication.delegator_peer_id AS id
                   FROM contracts.grants_delegated_service_publication
                  WHERE ((grants_delegated_service_publication.content_hash)::text = (content.hash)::text)) p) = ( SELECT count(*) AS count
           FROM contracts.contract_signatures s
          WHERE ((s.signature_type = 'accept'::contracts.signature_type) AND ((s.content_hash)::text = (content.hash)::text)))));


--
-- Name: certificates; Type: TABLE; Schema: peers; Owner: -
--

CREATE TABLE peers.certificates (
    certificate_thumbprint character varying(512) NOT NULL,
    peer_id character varying(20) NOT NULL,
    certificate bytea NOT NULL
);


--
-- Name: peers; Type: TABLE; Schema: peers; Owner: -
--

CREATE TABLE peers.peers (
    id character varying(20) NOT NULL,
    name character varying(60),
    manager_address character varying(255)
);


--
-- Name: schema_migrations; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.schema_migrations (
    version bigint NOT NULL,
    dirty boolean NOT NULL
);


--
-- Data for Name: content; Type: TABLE DATA; Schema: contracts; Owner: -
--

COPY contracts.content (hash, hash_algorithm, id, group_id, valid_not_before, valid_not_after, created_at) FROM stdin;
$1$1$EvDjWbYqRoWOUArY8v64jjGoA4nbA4tniX-Saa3tk_GRZO_hlMJWYwNwXWOtgFTnQOVM9D6geIgxPbVVrivrWQ	sha3_512	018c7db8-a89a-7a5b-a6af-f2acfc332a08	fsc-demo	2023-12-18 00:00:00+00	2023-12-19 00:00:00+00	2023-12-18 16:18:23+00
$1$1$QH89503RLswUpsCJ-2ZNcwxrKl3ppiX643Apy-G0qU0qolWt4kV9aIKIzrfRenaSVrVneloBJHzJhTAyANlbVg	sha3_512	018c7db9-729f-7a5b-a47a-06719915377e	fsc-demo	2023-12-18 00:00:00+00	2023-12-19 00:00:00+00	2023-12-18 16:19:14+00
$1$1$5v4JRb7Mn_Ac-tkrS7cbiYDLcU-Op0rQjy5SmQG3_U8rEwji6Arf638cSNNtox1Y5EwywO4bekhF1TX527Vq7A	sha3_512	018c7dc0-afbf-7a5b-87da-d0c379a97088	fsc-demo	2023-12-18 00:00:00+00	2024-12-19 00:00:00+00	2023-12-18 16:27:09+00
$1$1$4BEQlLR2zaiVpYgju94T70jKLKXcSQybCH1eRlTujgVJMEX3asJhyxM1VzFVnlMraEcR-jzV05AJ6ETrQsGSYA	sha3_512	018c7dc0-d53b-7a5b-93b2-5f60f0620d67	fsc-demo	2023-12-18 00:00:00+00	2024-12-19 00:00:00+00	2023-12-18 16:27:18+00
$1$1$dV2tscHYXn0ILfBEQljRvsZqKcq98HyZQ6PeEsDCQFDBrhSRwWwTBCGKL73aGpbFaS8Rhs-l2ybLUFJpOotGsg	sha3_512	018c7dc0-e86f-7a5b-866a-db98b0ce4dff	fsc-demo	2023-12-18 00:00:00+00	2024-12-19 00:00:00+00	2023-12-18 16:27:23+00
$1$1$DNApszMpCbx2V_ESm76YyOjATkLoBQWGL6myPVZQtQFJpeqtO6NzhDnZoL6PxbhEtsPdIuCFShpRIdYZZKglFw	sha3_512	018c7dc0-fd03-7a5b-826d-6c23204d383d	fsc-demo	2023-12-18 00:00:00+00	2024-12-19 00:00:00+00	2023-12-18 16:27:28+00
$1$1$Ygx91nyNS7sy3bVljmd_AL7LLRBdSfumx8LRt46XZF3YLJwirggWgppIf9a5BVjWnATHvFHLyR9GjkYWbNMkAg	sha3_512	018c7dc1-1036-7a5b-bba1-a5490f2f370b	fsc-demo	2023-12-18 00:00:00+00	2024-12-19 00:00:00+00	2023-12-18 16:27:33+00
$1$1$ngu35eMhPjhASj2w9xRq57cqp_670FmHDDSKQUtd-XyRcYAJRdfy6eJ5NiPV5F_ADX9E5Tsqdjz_RzuW0s2X4w	sha3_512	018c7dc1-22a3-7a5b-95f5-a2c8068a8407	fsc-demo	2023-12-18 00:00:00+00	2024-12-19 00:00:00+00	2023-12-18 16:27:38+00
$1$1$HRpD3UHDfQ8hLlp0P0e93Jw71aOntMIx2pYmVvs9-tDCn4REotFC2Ipcp8IlJPnWhM9ZD4Jbr3c5xJwxnjltLg	sha3_512	018c7dc1-35c7-7a5b-a6c9-753bb6ed972c	fsc-demo	2023-12-18 00:00:00+00	2024-12-19 00:00:00+00	2023-12-18 16:27:43+00
$1$1$_lNvG7GvHjnIMMlLgQyHYu9pDh15EFXZ4Lq4llgdzTHSDrIufFVirUp5nysf7bNqMLnmOP9PZQx7P-dNWzt75Q	sha3_512	018c7dc1-497f-7a5b-a76d-2d21100b9065	fsc-demo	2023-12-18 00:00:00+00	2024-12-19 00:00:00+00	2023-12-18 16:27:48+00
$1$1$9HlITyTXcn_OFnYc4dhe72LXnbQtl3r02Oo6qVJbL3WwkvUTKLllaE2tNlgvtgtlzyXTq8GGJSBTNHDL7iEYWw	sha3_512	018c7dc1-5a0a-7a5b-b2d9-227e58a47161	fsc-demo	2023-12-18 00:00:00+00	2024-12-19 00:00:00+00	2023-12-18 16:27:52+00
$1$1$UVM0sS9eypMFT7Naj_QdP6nwphcMPKSkTXrnA2VyI8J_mbutPwPJcY0IydGENvdpNovnSna3ZvYUyeEystbkTw	sha3_512	018c7dc2-1519-7a5b-a8c1-dec589f90b46	fsc-demo	2023-12-18 00:00:00+00	2024-12-19 00:00:00+00	2023-12-18 16:28:40+00
$1$1$JcSOrC-GgnLFNTQRysvTsJz5WlKS4qsQD4N5cz2TY0pnAhWKXGueILQ6CWMfqLxm9g5VppBSLKN9G50-7CGJGw	sha3_512	018c7dc2-2669-7a5b-b2b0-73407ce3fc98	fsc-demo	2023-12-18 00:00:00+00	2024-12-19 00:00:00+00	2023-12-18 16:28:45+00
$1$1$g9K0KKk2kWCV451HzQ_CPCcilLU1-1BcAAdAevf5gucsjVUYbSapBY2WNGsr-00PBM0uSRk3n__3XVkO_JpRBg	sha3_512	018c7dda-f70a-7a5b-9e5e-49c6f7a2d8cd	fsc-demo	2023-12-18 00:00:00+00	2024-12-19 00:00:00+00	2023-12-18 16:55:51+00
$1$1$BX9rNDcVm-7DF2CU8gRcJQDMGJS5bIuqHz9rj71a_BoNyeRKc17m9LuRqFm6aR2YYQlpjKgLMEYcpiUEpEftiw	sha3_512	018c7ddb-1317-7a5b-ab53-5fb9f91a1c70	fsc-demo	2023-12-18 00:00:00+00	2024-12-19 00:00:00+00	2023-12-18 16:55:58+00
$1$1$XjD3glV9m4-Qf3OCxbKo5MA4BMlJQ-zk1wKIs51O_KySFhwJgJg21BJ-KDl5Ir_l32nX1zteE35KszfOo-cdYA	sha3_512	018c7ddb-2884-7a5b-be08-4bfac912ff02	fsc-demo	2023-12-18 00:00:00+00	2024-12-19 00:00:00+00	2023-12-18 16:56:03+00
$1$1$Zvb29SLWVveoQuE5PHRLnCqa-wQXF33lP0DpEnUYVl01_bra0Kk2aqFL39tNxp2kwN-0oIe13thCkwjoiDXtBQ	sha3_512	018c7ddb-4099-7a5b-add3-99293d937ea4	fsc-demo	2023-12-18 00:00:00+00	2024-12-19 00:00:00+00	2023-12-18 16:56:10+00
$1$1$Nn5Dmv6qtSbJgh9jfgen_CVzbHrRNyGH-40dpMflAfGwrl21lNbw7Lyty93acnSNHw38Nifq9xdAGi9GkJVErQ	sha3_512	018c7ddb-5271-7a5b-8c9c-2e3a1fb0909f	fsc-demo	2023-12-18 00:00:00+00	2024-12-19 00:00:00+00	2023-12-18 16:56:14+00
$1$1$VNQO2H2jnGJXqPiwkK9WZtyvUCPwtA8-goq_MjQanGP94NDhr0sWsW1u1VZyRqUv05Q26ouMX2gNIiziRWF-zg	sha3_512	018c7ddb-66a0-7a5b-bae1-57a389588ce3	fsc-demo	2023-12-18 00:00:00+00	2024-12-19 00:00:00+00	2023-12-18 16:56:19+00
$1$1$vzwG3L60x-8dsA2PZR26mGvB_9CY0LHQZnwXxen2DNyM4kTdyAUFekissCet53ANOW_xAX28OmR_lNS6a3xhMw	sha3_512	018c7ddb-83a8-7a5b-847e-1a2f9facc4a1	fsc-demo	2023-12-18 00:00:00+00	2024-12-19 00:00:00+00	2023-12-18 16:56:27+00
$1$1$9ED-_sP5l6EgUfzgm4NGzmDHeuWpOCmI3SYQ9daKG1wlaZSx9GDLZVnH2VI0AkadRce33AogqYFpUGp5AqcTEQ	sha3_512	018c7ddb-9f15-7a5b-a45d-1e2bbd915a55	fsc-demo	2023-12-18 00:00:00+00	2024-12-19 00:00:00+00	2023-12-18 16:56:34+00
\.


--
-- Data for Name: contract_signatures; Type: TABLE DATA; Schema: contracts; Owner: -
--

COPY contracts.contract_signatures (content_hash, signature_type, peer_id, certificate_thumbprint, signature, signed_at) FROM stdin;
$1$1$EvDjWbYqRoWOUArY8v64jjGoA4nbA4tniX-Saa3tk_GRZO_hlMJWYwNwXWOtgFTnQOVM9D6geIgxPbVVrivrWQ	accept	01702331477921460562	r1KCu084pa7bEM1zQSCJe5pehX85Bi4_s7PVtpzNpos	eyJhbGciOiJSUzUxMiIsIng1dCNTMjU2IjoicjFLQ3UwODRwYTdiRU0xelFTQ0plNXBlaFg4NUJpNF9zN1BWdHB6TnBvcyJ9.eyJjb250ZW50SGFzaCI6IiQxJDEkRXZEaldiWXFSb1dPVUFyWTh2NjRqakdvQTRuYkE0dG5pWC1TYWEzdGtfR1JaT19obE1KV1l3TndYV090Z0ZUblFPVk05RDZnZUlneFBiVlZyaXZyV1EiLCJ0eXBlIjoiYWNjZXB0Iiwic2lnbmVkQXQiOjE3MDI5MTYzMDN9.StqNFIu6Fk7vXSPgRV5Fh1mW_NrpPuUjvGV0tak9eYF-WsnCPn8yZjgEcibTJD20tgoGMGo1ODa75Vdr_dRTeZKbeEemCgajsIpCySPecjnvc6ZUq4TvcYkyZhF6faX7FfXLeuViyvGoUtmKKme9gxxjcv9Dyfc6bFvwzTHTk8H0iHQN4mO4KR4PWiOPMx-nYHxVvEmablMED7cn7fR4SsKrPrk68I7b1-xsvvOe_knIoz1cWvAhqpLHgUWnwsIZquqJ2E4foMys9vPyxYrmAB52ryIgRPqkNf28ilnBwNoy0CWF0EIzvvUUnhM4bUjd88O3FiqAgwFwKj2kUwhxmi7m9uym-0h5iRFEsTWfmQTXpI8WdoNGQXXQ73Q1xg760nvt6ia50mpnTPr1Mdn86cQIVFBrOYehDBzoo8WB-Jt7B2p-E9w9bKPrKzTv9zJCfe5B-nhsFVvuJpGWO9SVJoERktvdd8aHHK5mrURBxeAWln_prudSr83zYF30JdpF4x9-XY5D5HQg4zYSraLkD4rG-TCLqhCbb78ef4qhM2hk7iiSls6-nMDh_dLX9Eh5bzXr6vIY9j9hrwK7Peu5SU7-eMmLgX_2WqFnn5eK37L-z3463USDq63T7YRW9Ceu0zA1C3NOEMwF_xq7-qZ4rAbOnTgfsrSRZEa5T_RKHbc	2023-12-18 16:18:23.011177+00
$1$1$EvDjWbYqRoWOUArY8v64jjGoA4nbA4tniX-Saa3tk_GRZO_hlMJWYwNwXWOtgFTnQOVM9D6geIgxPbVVrivrWQ	accept	12345678901234567899	U8Q1EXydxE--72ESaGOMHy-4PJpJ9LA2lYdHuOPjJqk	eyJhbGciOiJSUzUxMiIsIng1dCNTMjU2IjoiVThRMUVYeWR4RS0tNzJFU2FHT01IeS00UEpwSjlMQTJsWWRIdU9QakpxayJ9.eyJjb250ZW50SGFzaCI6IiQxJDEkRXZEaldiWXFSb1dPVUFyWTh2NjRqakdvQTRuYkE0dG5pWC1TYWEzdGtfR1JaT19obE1KV1l3TndYV090Z0ZUblFPVk05RDZnZUlneFBiVlZyaXZyV1EiLCJ0eXBlIjoiYWNjZXB0Iiwic2lnbmVkQXQiOjE3MDI5MTYzMDN9.VTwFGERGOqP4r0YUsSftecQRrqfb20t-JmdH3TlrYZNyV_4-2fuYSKjJ-OTjnWHWCxcO6WDtTXLu9TZN5vBQIjE7LvNxz83GUa1D3WwCIKcq_NiWRcNeeQ4DgiF6FQimDMnO7lGW02HiVnepxVABL9rCOIkSPP6lov-_MnSbxZyPT77d6EDtNSuGKN9qXfX3SlKMSkoCzsHZdbTo3Jq7dz6JWxskB4LYsYfOLBadEF80zHwSKuQc2mn1kMbAPahuJz0YscD20OoVd_WnffCb4bO7NqGxsmXbivybK3iGCXjTJJr1EOT_g_i5iEU6NF21msUd7T9An6TAG0nR92Xg3U9W5mfnMshxnKblGXbz7V8cu8Uo7z-r79QYeVLi2Wa0XJjD09CjoT11YZ3lJ0n98iaB8AYePz09KU8E0Myj5RC-tANBGXXh6b2nsXJzNNc5Km5Jtzu6NwlCS0VEvMJ2G0TEqJiDKNmWuZ8dnRtiMsNtTcrOpjqL8f6gb1pp0aHteoOuECBSrHv7vZJVQHmt1F_ZLK6tAJJgxC47x6hi9PGFLtWkHv3FYWenJdOUDyGeH1TdaKBZfMN58nvYnXxImRuCE25jF95aA5fWxGIhDcpl5tSVc3Qu0ATh6TB3TbRJwnHZdI3MCzBSMmhlqBihvzgus4-_4eqM0m2x9QU9xRQ	2023-12-18 16:18:23+00
$1$1$QH89503RLswUpsCJ-2ZNcwxrKl3ppiX643Apy-G0qU0qolWt4kV9aIKIzrfRenaSVrVneloBJHzJhTAyANlbVg	accept	01702331477921460562	r1KCu084pa7bEM1zQSCJe5pehX85Bi4_s7PVtpzNpos	eyJhbGciOiJSUzUxMiIsIng1dCNTMjU2IjoicjFLQ3UwODRwYTdiRU0xelFTQ0plNXBlaFg4NUJpNF9zN1BWdHB6TnBvcyJ9.eyJjb250ZW50SGFzaCI6IiQxJDEkUUg4OTUwM1JMc3dVcHNDSi0yWk5jd3hyS2wzcHBpWDY0M0FweS1HMHFVMHFvbFd0NGtWOWFJS0l6cmZSZW5hU1ZyVm5lbG9CSkh6SmhUQXlBTmxiVmciLCJ0eXBlIjoiYWNjZXB0Iiwic2lnbmVkQXQiOjE3MDI5MTYzNTR9.QY_FeLQJb46Pf5fzPJexv2TH2uWr-b4PV2ZQhshzl3BtqAvwJUCkywg2ChuzHNLe73YFZIE5BaVPa_LewmYUbNP8JRB96n-6JvQ7YdNn0-8KZzDx3AT7CAvGiqtNOO_L3rT9Fufa_XH2L1Fy_J7LPtcQL_u2QE82GXlD9TXI9Ol1hCocCPC7IOVOBtXdE6kD6rnisLRkFqiWkc3aNAq2MMUo1t-jdn-IATJidB-9zU2DpbJRhl0POcyO3ASi6DeroWahzfi7oTfrT0foa8RDXXZmO5O57yeV0wcYffJ4e9g8qvd9dBzOGkyK9dVP3YFNPXMzUKpGxehnTqZ_hCUK4zq-YSyLviZxq_cGq9ciol1EOqKegYe8nCXsDFrEC4qB7N2Q-G5yykj9nm5nyoForoA4HK6G6JqQxlh_SawSlnsBbdSzuOL5fD_TGy0RCTZiyMeoelBgA3oAmXsT0-36TfowrTQli-6aUjTEHsGpR6rRjpj8Olf7jrb6toDHrU7LlsJ_oPzgXEMsz2l5JJE-E9-RmGW-zJXQqVyAPQC-Mc0Vj966EEBv5I2kIqH1_Q4zXgHYzO8M8z4KdAo2sUdpRNklHLxeVxJpmhu81ElQEQiDhODU82iYpEr8Und0EWiCO-05dQ59oHWXJ_Q40CstJoFxTSYI9o0YobHempZPUMI	2023-12-18 16:19:14.720137+00
$1$1$QH89503RLswUpsCJ-2ZNcwxrKl3ppiX643Apy-G0qU0qolWt4kV9aIKIzrfRenaSVrVneloBJHzJhTAyANlbVg	accept	12345678901234567899	U8Q1EXydxE--72ESaGOMHy-4PJpJ9LA2lYdHuOPjJqk	eyJhbGciOiJSUzUxMiIsIng1dCNTMjU2IjoiVThRMUVYeWR4RS0tNzJFU2FHT01IeS00UEpwSjlMQTJsWWRIdU9QakpxayJ9.eyJjb250ZW50SGFzaCI6IiQxJDEkUUg4OTUwM1JMc3dVcHNDSi0yWk5jd3hyS2wzcHBpWDY0M0FweS1HMHFVMHFvbFd0NGtWOWFJS0l6cmZSZW5hU1ZyVm5lbG9CSkh6SmhUQXlBTmxiVmciLCJ0eXBlIjoiYWNjZXB0Iiwic2lnbmVkQXQiOjE3MDI5MTYzNTR9.fUse1Eiw58lFqpO7ruKZLZ_8ct-zC7I1IPQgEmDQBjNxpclETeMpDSH1-kDnh4OyhQtdnZaWvtyZJJQ0ZY7aSBM8d41DuMg5yjA1FutAp85ozcj63xFjm0_ZzGgPs46IwSO-XPfGsaswxnoyiymQzdUIR_GDgBBelf41mndCjarA9gg7Ew6pDbvppGfJIQan5OdrlcfI_Lh1YDZ36e9R00WfvW3YSMUDW_P6hlbz08sKdAKRnxXQfCWM7DJlD91NTnPXSGNc9UHmwWz__puqZ15wNxIgw0ez7JIYHMrC8xgEVB8fRgC3KA6wNAIU7fGGrThm17sFLZXQPHdmxTE-5m2_RlS7FDLazfYcRLTQdJ9z-YZXBzMZDCpkE7omGw7Fs1n3KbjtSenCYeBl69Zi6qgxcqjwGWZ5kwrpq3_U57QoHOIQ4fJNX71-HeKF1Q56vUTcoAo7i2wjt5RqL_qEBVoI_HW9-vYMyWICi97sqMyPVqM3L8yqH6q4jE51GJwfQeT5Hygqjq9zqlbdviAcVlwSR4RY97nn15QF3-uNH0D6jKG4b5eOsB4PrzMO9ia4RXIkBGGAb1xG9rNUMAWMXkjnj-0UDnXzMPKJLWAEiJ2lBCNpEig5D-Lnzg65BFE5YBB4SDYAT7uKbIdcZQXe1kkUXr113bJ_BjSWeWWU82Q	2023-12-18 16:19:14+00
$1$1$5v4JRb7Mn_Ac-tkrS7cbiYDLcU-Op0rQjy5SmQG3_U8rEwji6Arf638cSNNtox1Y5EwywO4bekhF1TX527Vq7A	accept	01702331477921460562	r1KCu084pa7bEM1zQSCJe5pehX85Bi4_s7PVtpzNpos	eyJhbGciOiJSUzUxMiIsIng1dCNTMjU2IjoicjFLQ3UwODRwYTdiRU0xelFTQ0plNXBlaFg4NUJpNF9zN1BWdHB6TnBvcyJ9.eyJjb250ZW50SGFzaCI6IiQxJDEkNXY0SlJiN01uX0FjLXRrclM3Y2JpWURMY1UtT3AwclFqeTVTbVFHM19VOHJFd2ppNkFyZjYzOGNTTk50b3gxWTVFd3l3TzRiZWtoRjFUWDUyN1ZxN0EiLCJ0eXBlIjoiYWNjZXB0Iiwic2lnbmVkQXQiOjE3MDI5MTY4Mjl9.kOEvMDWBnk66-j7GNDoh9sd0UawQNHWDMlGbaJhTdcXvwNEuOBKFYuHraEEjG7siVBNsQE-dnB7EdNodj09s7iMhASti83Se5ZC4W3BUpHrektwA9kieVJJQ3VyHMFCS0NjuJITSdLnn999HtVwsvjp9Y0lkITOf8HTw-QHCHbw979E5tRYqsGcJ3Vu8KwZ0MneOZkQt1Z-p5dmGK1csu5UIMr_dB4gknNstxWScw6KrGbvLMlcx5jwmikHnoeEX2bUucIA5N2yfQIQdzJgyS3SHEvjoyFyIpSYt_d80r_35YB6uOvzYjSiwmVe0Ij-WC9waljT43kF0aF5FRz8gvpIBtAMaUHAFED54Kbj7T4L8RorHngdXRBtDLSfGNfGp2ji_NQ_shDVI4UuDVvtWIrKdL6oTi__1oz564mBWsx4xKCqTA1UNopuLd2Q6xtLefr_njd1864OtCvpl18b4tJDYINGmH2xeWlxzrDn4wpOJgc3d9cjHAX_koHBCPqmrWxJSD3QOMGUnYrGDPf86StxQcWgVYZmY04FAQXn2gbzvTLQt4Zpf6H4KSGwr9mGqCjaD9T7YlidlJLj3jbIKoTfb8UptoFdEARKRrcPkYqPeOdCBE1LGL1LiwI4hzJx-xz01Hq7aDHzsi2sOKIrnp9vz5Y_bctYybhnHFn7zbC0	2023-12-18 16:27:09.120474+00
$1$1$4BEQlLR2zaiVpYgju94T70jKLKXcSQybCH1eRlTujgVJMEX3asJhyxM1VzFVnlMraEcR-jzV05AJ6ETrQsGSYA	accept	01702331477921460562	r1KCu084pa7bEM1zQSCJe5pehX85Bi4_s7PVtpzNpos	eyJhbGciOiJSUzUxMiIsIng1dCNTMjU2IjoicjFLQ3UwODRwYTdiRU0xelFTQ0plNXBlaFg4NUJpNF9zN1BWdHB6TnBvcyJ9.eyJjb250ZW50SGFzaCI6IiQxJDEkNEJFUWxMUjJ6YWlWcFlnanU5NFQ3MGpLTEtYY1NReWJDSDFlUmxUdWpnVkpNRVgzYXNKaHl4TTFWekZWbmxNcmFFY1ItanpWMDVBSjZFVHJRc0dTWUEiLCJ0eXBlIjoiYWNjZXB0Iiwic2lnbmVkQXQiOjE3MDI5MTY4Mzh9.2BlOm9NES7zk-R4Al6DpR1ekTpBtDv2Yy6dkDNQ1rI5x92nGy3fJR0eBP4Yt7Psf8TDRgT0EbwgA54X7Zz3RGNi6VSLx2QOMmfcXbhGlOIRadWoPZt8RiLbIaiQ6qXUcYpkBh_-HUEvw4OR5Tb0WnnkgArnXRIzgjq4EKf4b6EkFH1K0jHPPbebWtf-N3j5ZaWY4ygGVWDJFIiOgChCaBMudE0vVa9hYrvhl2npTt4Fgg4SzcQ1G5Yb3nPMC3NSWGLe7yCZdtnFawpfXN7bxsS_j7S1KDrFq8nvknc814b-XjpVyDBSw3rA1hquUjOW-Yutbc0vVT3tHUMmtSxJ1Wh2S3cJxmgkOSONL90Ia1FNEXudDUnBH3zZ9Oda-_WLWSqmvOLs0c37yddVZ_ZEkQsJanpLnpINJTvb3hbQqTLZfGGhUOrVcSZUwCrT8Rgwt0TeyqUdP4Q3ZU1zZli5XGjtTl9msY7nAhjDcqX-NQDDyG0dt5qCbX7SYFjrGW6FSU_X2dgNPJ5guVEjujAPGHUmUEriozQ23bkyJ3WG4a9igqAi8x5XgsLLbTbZFFUHuGqEfQept11dmWr_ijKiMeCbTE8jWh1Tp6I_LM4vDV8j1ag9hiWBgyf7d2XD1z4SfdNhswUPur2gr-ZlpbaTMHAeWBicRUpydD8_StwXN3m8	2023-12-18 16:27:18.716512+00
$1$1$dV2tscHYXn0ILfBEQljRvsZqKcq98HyZQ6PeEsDCQFDBrhSRwWwTBCGKL73aGpbFaS8Rhs-l2ybLUFJpOotGsg	accept	01702331477921460562	r1KCu084pa7bEM1zQSCJe5pehX85Bi4_s7PVtpzNpos	eyJhbGciOiJSUzUxMiIsIng1dCNTMjU2IjoicjFLQ3UwODRwYTdiRU0xelFTQ0plNXBlaFg4NUJpNF9zN1BWdHB6TnBvcyJ9.eyJjb250ZW50SGFzaCI6IiQxJDEkZFYydHNjSFlYbjBJTGZCRVFsalJ2c1pxS2NxOThIeVpRNlBlRXNEQ1FGREJyaFNSd1d3VEJDR0tMNzNhR3BiRmFTOFJocy1sMnliTFVGSnBPb3RHc2ciLCJ0eXBlIjoiYWNjZXB0Iiwic2lnbmVkQXQiOjE3MDI5MTY4NDN9.fu0kFQpz8St4KWZhGFYdGIMmt9QL6sZuLjgsACiEVRx2gQcJKIZo0nXRXe7pJoZ3wdVBemtCxuS3zI0G9VSno9BQRW_LUDHHRR1dWBk9OGAuH-tQ65ZhYq_TTIqiiwsUDbs9fRuCipNDP_ScLMRxFJTe4cMCbAiX0iadpR06ZBGgJX-niwBfD1UxUu-jbAYC0J5B1uXLeZa7s9nKH1lh_68_OIhhfbrhyQpTaA6CBTa90O7g3ZcEfUV0RtdmTHkS3WEdp1xwhJHyGO5I-XHDWWbDFdrjW8qwWT_Gyx_--ibjHLY50oBwdaYPxxbbN-8BRtJ4fNNKI1W5asufEE-ELmQdVr9Glw0cb2CJEWYUrq-sjyTr-isTJRNGVJ_qHfyA85gGiHunkNGbdI3ZefM6fAzkF97I5zzJ_uOY93jO33T2iBMe9qevThJEaxWUbh8KflFU5qr_8EIidaO1bSdm2Ymxiv2WmnH0-oMexnQQk-p6Xb6-dVZtVdn-R89cDbtlHdBk5WKSXRT4ywJMriMprARSrLNTQOFvYCM9iMFVqDhqfkbYUBYL0hLgoPduna-v3iJ_YinH_Zq6GqTUgBj7w2TidBqLPcIJPC4vFQWvds6fafwZAmPhq37xab5gJbbhJk0XtAfiyqW75iXUz4Kqq6_cNiphh12VdQxlFMJZdok	2023-12-18 16:27:23.632397+00
$1$1$DNApszMpCbx2V_ESm76YyOjATkLoBQWGL6myPVZQtQFJpeqtO6NzhDnZoL6PxbhEtsPdIuCFShpRIdYZZKglFw	accept	01702331477921460562	r1KCu084pa7bEM1zQSCJe5pehX85Bi4_s7PVtpzNpos	eyJhbGciOiJSUzUxMiIsIng1dCNTMjU2IjoicjFLQ3UwODRwYTdiRU0xelFTQ0plNXBlaFg4NUJpNF9zN1BWdHB6TnBvcyJ9.eyJjb250ZW50SGFzaCI6IiQxJDEkRE5BcHN6TXBDYngyVl9FU203Nll5T2pBVGtMb0JRV0dMNm15UFZaUXRRRkpwZXF0TzZOemhEblpvTDZQeGJoRXRzUGRJdUNGU2hwUklkWVpaS2dsRnciLCJ0eXBlIjoiYWNjZXB0Iiwic2lnbmVkQXQiOjE3MDI5MTY4NDh9.fWFRqRBjDKMkOveLlmWJbh0APYwr-qm0Me-s8U7mTeZWM3jiaXrOhR4cpD5PcJ1-L8-Nzj3r0pAvgtS-pikIf1-VZPFU4HTfugShN0puHLG-Gi0xzRW5ZAzaTglHjdqpgMTzUuNQq-SBAk_uoGN0fhqUplsdPl6pt5shWFfpCVOdofU_rNhzXpUZwt6UukttimhQy6CgAgjuTcWJ3p8BAIuepyji3ST8UtXp9ufxSiH3TI2VTdxoJa3D_kyjmD1ez66l9sre3ZLBX-NrfAiOUspYlj9bS4rGmgrmzuP7ROWPjc1ej0O-2dFAstD97C2lGF9zJFQR1nhfL-Jxxt9r2EOmGDgtIGm-OSnQQ84CmMy9DgI4bWXoGOwnwrWfHqbaYhsaQDkvEu3ZsWAKCxbG3TaH7g0QYY0yUzk7IwClR8NlTVCyazCDTxc1vofco6zoUBIHSEOp9fFfnhDciFV8wMfGXPZwm3wI4amhG3P_0f72-Wbe5KQMhpviMvwbo4Proqfzp2KQas-18WWlhNC-fwHjYTK78yWaOZqL_yThGA24WTF-LgXuMJeAaLg6DpED8aHwbaR3WmIK24exb8vfb654XJd4VeK_pPKrnhxGxKyUiOqZZT3IsT7djw4hcr00Dj6CszhTlinwiVMcms-keSztzBBEjkWkS_O1Z87qOIo	2023-12-18 16:27:28.900505+00
$1$1$Ygx91nyNS7sy3bVljmd_AL7LLRBdSfumx8LRt46XZF3YLJwirggWgppIf9a5BVjWnATHvFHLyR9GjkYWbNMkAg	accept	01702331477921460562	r1KCu084pa7bEM1zQSCJe5pehX85Bi4_s7PVtpzNpos	eyJhbGciOiJSUzUxMiIsIng1dCNTMjU2IjoicjFLQ3UwODRwYTdiRU0xelFTQ0plNXBlaFg4NUJpNF9zN1BWdHB6TnBvcyJ9.eyJjb250ZW50SGFzaCI6IiQxJDEkWWd4OTFueU5TN3N5M2JWbGptZF9BTDdMTFJCZFNmdW14OExSdDQ2WFpGM1lMSndpcmdnV2dwcElmOWE1QlZqV25BVEh2RkhMeVI5R2prWVdiTk1rQWciLCJ0eXBlIjoiYWNjZXB0Iiwic2lnbmVkQXQiOjE3MDI5MTY4NTN9.QowYUasxbX9J4_oUwfAic2XPPEcvHy-Cx0oGXByqOzbssxAoppg1x9PgUr6cvm4QKoAEWNhZOyJTBWGx-kVXJ9RxbNt9aOjfUZ3ywwjFFaN-HDI_RO43URXE7tXngbICxPzLDUJ9RHgLiommbik0dSNMvBrrUjobsfqF_e4QOt-dhWSHqSaWYNzL45gnTskzPM6GKVZcZ6_yRHXdmxxuWzcn3-NO9XBdwRN_z7R66WwAGBTgtBw7c87ZRzIsiyw6hHVzSXMX-vN4SXsmzOQs78fklg93mUH5lk2ZWWUWjJnzdItzVK2TqABGPxqT8m1R1Ena3SS169Kh-OMbDXykOxBcWpOMHN2RK_jEsonQzzAjzl8ky6avJ41eGKxLB8fo1nHY1hCeppnM4Fh0jqha8xDhL3f23cSzNJq1wpBElPvWWrxpu5LQJERatIOEZvYBjz4v_fLmgGn5PehlDD2cTHnMC_oqTjrRdufGTzqPjbgqsvTp8i2hZW0Tf7KksBv2cm2BieX4FhExwfIHaXFNKOWiyx_ti9U8iKA7IbNW2v6lG-I3BA0g_t0X2gQv__4vwmoWE_mqBtFtgXSNVEkUvki0lk9dBTQidKAvI-BQjWxNNVH068jcRmYyBfDeYIfwT5ByJA0mt6WqMgGJq-Ztq05fRHXKqKjxSi7LdHAizmM	2023-12-18 16:27:33.815286+00
$1$1$ngu35eMhPjhASj2w9xRq57cqp_670FmHDDSKQUtd-XyRcYAJRdfy6eJ5NiPV5F_ADX9E5Tsqdjz_RzuW0s2X4w	accept	01702331477921460562	r1KCu084pa7bEM1zQSCJe5pehX85Bi4_s7PVtpzNpos	eyJhbGciOiJSUzUxMiIsIng1dCNTMjU2IjoicjFLQ3UwODRwYTdiRU0xelFTQ0plNXBlaFg4NUJpNF9zN1BWdHB6TnBvcyJ9.eyJjb250ZW50SGFzaCI6IiQxJDEkbmd1MzVlTWhQamhBU2oydzl4UnE1N2NxcF82NzBGbUhERFNLUVV0ZC1YeVJjWUFKUmRmeTZlSjVOaVBWNUZfQURYOUU1VHNxZGp6X1J6dVcwczJYNHciLCJ0eXBlIjoiYWNjZXB0Iiwic2lnbmVkQXQiOjE3MDI5MTY4NTh9.sANauvL1q2G8Ouz0ypvh9DU18aMtrwyqA1g9fQXjVTerlrKxaQXltoRFVW-YZaDexHSLoZjnocC4tazcNfymy2ThQyW8qo00M-WU9Fnbg9gGPRYhs6n5xvneDv9JHN3KGyPk8ZTPZo9xBlTf9sAiWv60eoTdKk6wHDhsr3vcbHt7yF7mP-YYHFUrvSP088sm6Vek_MUo9DG-8JM7SP98nHUw_yZ4uV1woI5Za6DjJoRCdLPBN8h2dWLxIPGS5dSrFEcJJ4H8HsVKSszhbTHvokJ953ouOWaC3QHgmLy-5sSuIvX-4_iqaSBVhqldRHrQJ_0IobdBz0fsMamFgSInU_cdmBWnJecB4h4K-HlcQK3-03F5RT0fO1QH_Wpg8XGBmhyK4_MCpWBkXM_-EE85Le4ucFRmQ8DVuLU-hC8v3WO9neKxjl0brCmwvf01xJdXl1P9rnUPbOmecmOYD68VKPPmoRpKYJncvdfMQvLIYLZMi4z7fSBArG7grkebvkp7BzitEbHWONp-1XVxo7NlRqU7d9gpJpi_UswTHVgOVhzq-IFlE8gSgrNnTQIQtTDle-s2MQcjb4KQDJcLuiYpOPkHN0GQe0_hOtFvz5RPGDMdOlvOopI2dAPmWs0bNv3RLigkrZebXigtmxSR5rAqfB-arGMys9PwmlKA1YS3VzA	2023-12-18 16:27:38.53273+00
$1$1$HRpD3UHDfQ8hLlp0P0e93Jw71aOntMIx2pYmVvs9-tDCn4REotFC2Ipcp8IlJPnWhM9ZD4Jbr3c5xJwxnjltLg	accept	01702331477921460562	r1KCu084pa7bEM1zQSCJe5pehX85Bi4_s7PVtpzNpos	eyJhbGciOiJSUzUxMiIsIng1dCNTMjU2IjoicjFLQ3UwODRwYTdiRU0xelFTQ0plNXBlaFg4NUJpNF9zN1BWdHB6TnBvcyJ9.eyJjb250ZW50SGFzaCI6IiQxJDEkSFJwRDNVSERmUThoTGxwMFAwZTkzSnc3MWFPbnRNSXgycFltVnZzOS10RENuNFJFb3RGQzJJcGNwOElsSlBuV2hNOVpENEpicjNjNXhKd3huamx0TGciLCJ0eXBlIjoiYWNjZXB0Iiwic2lnbmVkQXQiOjE3MDI5MTY4NjN9.cZb8LVlp9fx3u16eEVr_QVwi7gqdCWgTnoZqIhLv04RAqmrPTDo89rmckYeNDenfG59cZ4Z_i59Fw1EPgHUwaLzLq-Rz55juabd9MuZ0d0ox_XnGB_IlQFe3uIBfzf7jrA1ITIEtRFsLAJP7ptDVhUQvVCO7KoXIeEXziOQiQV-UfrVU3-QwN1g_0PDQvtTE2DiUXRmFIifsZcq0uOug7TjGSESzuKCERR8p4B0oVE0Vr3Zz41wtHIW3lgQiJ9FIGYMvaI7XgiTdVuPW1nT2i-z7kwABOm4zWpuGi2SLfP_FDvRdXqo61GrT_-b8bI18zG3rVE8BrchsvyDOMZNv0qFB6X_dVWBCubDph6YypiZc2tjHJgCN-ABQ4RWy3XBAhf6l18J997OvgdddGkqw3FCyEWKAQa_0mBTcKllQhTaRQy3WXgyMooJZMOnr5viA0OG2GckPwKbKi9VrqANdFoeofSnjtAtUvtCYH27NqCRi6Y_hFYp3wODMZgUN88nBXQ4OfOrJEQphavDnvwKJ9MVZprTuDhE31tEyVlVJtGalroE1iXyjjx5RDLLgEu3PgU54EhSEfZzlMKO4PwuxDM3DECR2pDWGbPDjj_DCWvTZFKxmiav1WTMJ4YkioVgTQPqTIiXd8S-dmL0Y4PoJ8JtO09Q8qRJxfnRWtdqZp2g	2023-12-18 16:27:43.432362+00
$1$1$_lNvG7GvHjnIMMlLgQyHYu9pDh15EFXZ4Lq4llgdzTHSDrIufFVirUp5nysf7bNqMLnmOP9PZQx7P-dNWzt75Q	accept	01702331477921460562	r1KCu084pa7bEM1zQSCJe5pehX85Bi4_s7PVtpzNpos	eyJhbGciOiJSUzUxMiIsIng1dCNTMjU2IjoicjFLQ3UwODRwYTdiRU0xelFTQ0plNXBlaFg4NUJpNF9zN1BWdHB6TnBvcyJ9.eyJjb250ZW50SGFzaCI6IiQxJDEkX2xOdkc3R3ZIam5JTU1sTGdReUhZdTlwRGgxNUVGWFo0THE0bGxnZHpUSFNEckl1ZkZWaXJVcDVueXNmN2JOcU1Mbm1PUDlQWlF4N1AtZE5XenQ3NVEiLCJ0eXBlIjoiYWNjZXB0Iiwic2lnbmVkQXQiOjE3MDI5MTY4Njh9.o6WBJyrMzgcuvnYLi1PH0ynr3j50Jgbuy78USWI3qBmpP8LDjAZMvYRySBfM3y1g6l6IPDPb1JdKmQ3gjsF8Tci-UG9VK94dNmghIlqfh9NZ_x745Ol1Wnm8gYbjNo2saMH1dlMHVfaQMH8tz8MOPACXogU07HLxvh8Wc1f6xhFtenzOhXzQKIbUvSFfxqo0tI8aPShWZvyaG53r-KZdLTjd0vQ2UxO8XNbzEXvlhHi1mv0Qw3ADGWqKnTaezICTFTPMHIu1RhiK4Y888_ej7wCicqRy-V7u3TbfTyQmRiKy8pGfo9wfCa0cYrOXEyr-jF86ezJsC56Zhc4BFSBj6cfifIpL5BOWqREZ7RoP3GaO1WQB332qhG4QYERvGDRL_pL8vgj4jvR4xaELa2Lo93izMI1WFjjiGnh_RusFaIUbDbzqBqU1uilLiZuZo2eii-lR9Q9RfYMDzLEME7yX8s6L1sF4Uxd8Wuazo5TArkKO-i0ZW7WQIY-XGtuxjeRbK6KMv4Qbfn-Xmwk8ldMP_rz08HQviacmRsZVYakSc_-ui_aC9jvfZI1k5hWH6ZBKrvZAPBML4Ec9R-yNxgs14LOLteAyn_hq6ZUTb_BRyr7uTTUYVJMisu1j7pDwUGr6LGWTKSANH6jIswG59bXSmoPcvpKonQp8toUUjASWr54	2023-12-18 16:27:48.480416+00
$1$1$9HlITyTXcn_OFnYc4dhe72LXnbQtl3r02Oo6qVJbL3WwkvUTKLllaE2tNlgvtgtlzyXTq8GGJSBTNHDL7iEYWw	accept	01702331477921460562	r1KCu084pa7bEM1zQSCJe5pehX85Bi4_s7PVtpzNpos	eyJhbGciOiJSUzUxMiIsIng1dCNTMjU2IjoicjFLQ3UwODRwYTdiRU0xelFTQ0plNXBlaFg4NUJpNF9zN1BWdHB6TnBvcyJ9.eyJjb250ZW50SGFzaCI6IiQxJDEkOUhsSVR5VFhjbl9PRm5ZYzRkaGU3MkxYbmJRdGwzcjAyT282cVZKYkwzV3drdlVUS0xsbGFFMnRObGd2dGd0bHp5WFRxOEdHSlNCVE5IREw3aUVZV3ciLCJ0eXBlIjoiYWNjZXB0Iiwic2lnbmVkQXQiOjE3MDI5MTY4NzJ9.eFS9kFngIHuAKMalwZfqkT1cknPhHEGPMPeZjILeX2R41z8Tn3dtDB0xKVbGw3V7h33sMkHjLg1s3OUpntW7ULBuSn0ThF1MO69LG-CVMT4TW9KzOrpMb_p2l-wzwziBax7pZQcuPY58qUunNy5UhwvjeRjteK-6JGT3YaLqEJPKDWWJonyP65axhZhUdzMpraxx6J0eZoVQ3kf2S_TvTmkLJTm4rUKLKlcaVqdYQ7drZgp08B4K7nCdJlWVtdV-gVKyveS2wfvRhRjHgOfkilbmmZTsZPpr27Nl4TddKVUBpYuHUxUPGNz7n984D5eJtkLOiDkOY7vir-dD3H-EX4-ycAnkd5wIgJxrYlowIK_Gszn871c-kDBj80zwjSTbbuRxn_BCfA5MO4k8eDmrK9qMATBqTMmxZZGT963wtcvhOCPs-j4HxDV6UJiQu-FUr9kkpd4zHGEj9p2Pc8wkwI33uw6mq1MobNE9uiX_-YvcEP5_TTwXQJy1SrIC5VmLDdMFjJ0DsD0xj2zCOLFv-TlkIejzmuD31H2I2FwZ4e_5_Cm737O7X2hEgPR9JqL1wZ9Kd47O6KXpi6keeZ_g6R9yzJ-FsUn99rdwPeuFDlam_eFVbn8KaYDU1cvPqjNSaSvOFZEZMUBD4yCNY15hQrhadGFMr6XmOAFMSfW8xQs	2023-12-18 16:27:52.715272+00
$1$1$UVM0sS9eypMFT7Naj_QdP6nwphcMPKSkTXrnA2VyI8J_mbutPwPJcY0IydGENvdpNovnSna3ZvYUyeEystbkTw	accept	01702331477921460562	r1KCu084pa7bEM1zQSCJe5pehX85Bi4_s7PVtpzNpos	eyJhbGciOiJSUzUxMiIsIng1dCNTMjU2IjoicjFLQ3UwODRwYTdiRU0xelFTQ0plNXBlaFg4NUJpNF9zN1BWdHB6TnBvcyJ9.eyJjb250ZW50SGFzaCI6IiQxJDEkVVZNMHNTOWV5cE1GVDdOYWpfUWRQNm53cGhjTVBLU2tUWHJuQTJWeUk4Sl9tYnV0UHdQSmNZMEl5ZEdFTnZkcE5vdm5TbmEzWnZZVXllRXlzdGJrVHciLCJ0eXBlIjoiYWNjZXB0Iiwic2lnbmVkQXQiOjE3MDI5MTY5MjB9.l3665ZtZSgp0tE7TKmasMBPS6rbgR8ui5AcP0lD853sIcfGb9irsHYteCD4KBWmVe3vUvZThIG8w93GdzbNOj3uQeTKwTUtJymswROHf2XvkTdzTx-Y1ok7ZFKQiFJI6g4ClJ8Jtj0Utv0dLYOoDvmRMeBdDM7tpIrSDQhcSIGzsVlVgcbWQGb1W49h9jx-wYkWSIbqXWGA7rVpY-b5cYQH9YtG6PGALba78HtYzq1AOR3Mra5pHu8Bgum76cUiMEZTvdGcP3AH91zrFEQ60fs8YRODYugHg6wr73AXuKmgLpFQWa-4rKepkfigOScAu0yKsW9F8vAEqtrw0FQ-dTGKV7-QM_eemTxZvU89H4ZCnbje5xe-q_EoqOEJWS8KUCk31ChLXKW6MZO7JVuuNgWz8h_uo7_95OMu-52WGmEX57LI24XMihz-UTGqX4T_LvwVvQ9MvPmJKV0b3IVlnoYdIPf9HiW5GeWWdZr_Kt0oYzo7W5_PLxoleju2bWQb4WUEUQXSwz00t_OohPbKiFOHS0vJBJXTR61ynO_Ni0nuixPliEKtDjQqgMWpKm-td-NLEsjKQVH5J2nvp4aoBIj5j0ZDWfPQ9v3DrurD0LX5LFz95O69OKh3-840HrXSkL4C-DFfistjC7WdREIelg84o_CIaofFKTGNk9D7M3Ms	2023-12-18 16:28:40.602516+00
$1$1$JcSOrC-GgnLFNTQRysvTsJz5WlKS4qsQD4N5cz2TY0pnAhWKXGueILQ6CWMfqLxm9g5VppBSLKN9G50-7CGJGw	accept	01702331477921460562	r1KCu084pa7bEM1zQSCJe5pehX85Bi4_s7PVtpzNpos	eyJhbGciOiJSUzUxMiIsIng1dCNTMjU2IjoicjFLQ3UwODRwYTdiRU0xelFTQ0plNXBlaFg4NUJpNF9zN1BWdHB6TnBvcyJ9.eyJjb250ZW50SGFzaCI6IiQxJDEkSmNTT3JDLUdnbkxGTlRRUnlzdlRzSno1V2xLUzRxc1FENE41Y3oyVFkwcG5BaFdLWEd1ZUlMUTZDV01mcUx4bTlnNVZwcEJTTEtOOUc1MC03Q0dKR3ciLCJ0eXBlIjoiYWNjZXB0Iiwic2lnbmVkQXQiOjE3MDI5MTY5MjV9.HnnXh7u2ClIY5nGmfEQ5jv_E632ZWqQpdmi5lqo3lbLCD0Pn44R56GuQTUOKgUe3yxCPPXfvJ-1dL1SZk-3onnQ8cSqQtLD7XJeCi37Mo8Hs3mWvtfgj81XcXMUv8dOUoR56JxRhHaR-YPYtoDKP4havXbMipUgNunnlumZd8FnhplGwSr5N_WH75nttcT_2IBIoXrnLtR7rAVA3jSb9FzaQF19r9dAaBYPhUEg9vuCaGQ4ow71DyWn0Zd2M5PY5UX9a1Df_qVI12_2IFYqF75YlsrBqrn0mHknpb2TGCiqAHGqcJzSpBJ7gcUog2NJWnWVcaMSlq2T7wIDOzTL5Bv-TEhDKjQ8_70Ga-AR-e9O7DPx7CF0UL6LikdoVC8j3QENHqOyeOq82ohavsO3xZvZww_cObmx-G-IN-w9LdgPWB19wkexHtbqnf1A4xFh6A1b6koZDDFqjsjKU8_uX3AHn-UabXcZZAGw4HAtSiTA9uXHCNNDz7h-VDP6YOVO0_PDcVsuUjD4fPPQMjVvYwKCsw2wAbWxhsKVjWIcmhiGuZdmAr_n7xy0s4YSzRu4Foxypfc81G5b-HqFuay6JIYLEs1drochA_g6lUaD0xmzC_id8U8Hmvx2dTTFnbAMcRnpzM8WyKAAcsoklIdt5qBiyTYU5gX5xn9IEE-OhQHw	2023-12-18 16:28:45.034704+00
$1$1$g9K0KKk2kWCV451HzQ_CPCcilLU1-1BcAAdAevf5gucsjVUYbSapBY2WNGsr-00PBM0uSRk3n__3XVkO_JpRBg	accept	01702331477921460562	r1KCu084pa7bEM1zQSCJe5pehX85Bi4_s7PVtpzNpos	eyJhbGciOiJSUzUxMiIsIng1dCNTMjU2IjoicjFLQ3UwODRwYTdiRU0xelFTQ0plNXBlaFg4NUJpNF9zN1BWdHB6TnBvcyJ9.eyJjb250ZW50SGFzaCI6IiQxJDEkZzlLMEtLazJrV0NWNDUxSHpRX0NQQ2NpbExVMS0xQmNBQWRBZXZmNWd1Y3NqVlVZYlNhcEJZMldOR3NyLTAwUEJNMHVTUmszbl9fM1hWa09fSnBSQmciLCJ0eXBlIjoiYWNjZXB0Iiwic2lnbmVkQXQiOjE3MDI5MTg1NTF9.sQyRjz7glAdAChgXMYQr7xoia6sjuOvgJJqNV3Zs6nsL10t2pcg0RbMHC0OOQ-MkiFpkrDnW39WFG77mH32sBCW709sUlWcMamOSK9l2CynMqhW596Tt6I4EXAjK5WAtZ_6WUEqmpWvI9Jj9gg7p5i2Tf47jkyMLubrRPnb9WPOFy7tLjaT6MZfHpzD7kIueEwLuNEc_H7v2d-BHhV4TERA_ufDKufysiZkGXCPirNDCxEEXKbn5VHal88fp3QCbwwsS0kVaZ5USkb9Hd0DPMTrcw9jLjC1aR-mHa7Y91OkpkWzwQ_qsC_iPddtqfFzdAvuvv9AzYYjDlMFaKhX-LIV34UP61OFTZrKHuBcq3b5dk7IwfUgrVOMBDLFMtynXEmzGit-8xxLlxDlI978iVxMMdWf3vyx3EFZXqg4qTvheHl3gLBXMidpW8qxs42RVOWG7xefe0BgydrpdO1JQUzb5WCQAccg97wzycKIYzP_ujJPP_VC2i1tAiqSVNKklWh74mmrfkaiPkeezBjbsQJpWkcWlIfaF_VcnIphpZLX4zSHUi9J0NudG1qszqYZZcaP6ykCr3G_D3IIYMcR3xSgQmHo-P_gdf_feK4LZP2QmsV1B5b96hGAqBcrW4P8bORJF33AmKdX2YodiGuBi85hG-w8UrIiZpk4SMae12W0	2023-12-18 16:55:51.307544+00
$1$1$BX9rNDcVm-7DF2CU8gRcJQDMGJS5bIuqHz9rj71a_BoNyeRKc17m9LuRqFm6aR2YYQlpjKgLMEYcpiUEpEftiw	accept	01702331477921460562	r1KCu084pa7bEM1zQSCJe5pehX85Bi4_s7PVtpzNpos	eyJhbGciOiJSUzUxMiIsIng1dCNTMjU2IjoicjFLQ3UwODRwYTdiRU0xelFTQ0plNXBlaFg4NUJpNF9zN1BWdHB6TnBvcyJ9.eyJjb250ZW50SGFzaCI6IiQxJDEkQlg5ck5EY1ZtLTdERjJDVThnUmNKUURNR0pTNWJJdXFIejlyajcxYV9Cb055ZVJLYzE3bTlMdVJxRm02YVIyWVlRbHBqS2dMTUVZY3BpVUVwRWZ0aXciLCJ0eXBlIjoiYWNjZXB0Iiwic2lnbmVkQXQiOjE3MDI5MTg1NTh9.b0JI6RZATUSj72iaU59kgO4CH5DZeEX8LevAY-9Dn-pxAJ_C_cGVHTsncQcx8x_7gK_9uTQySM7NxhHJTBwuU0ZaPdnyiZWSzkO9bDZ5-AR4txPJcMzl675YW18wFhPtQYLVFSQIATUmtdNqWXneGcAQTn-x8pHGxueH7Yh_HH560C8JXD8eoV-eCYOIwVKvqScX-10kUvDpeSU5AfEIEt1ykU_DxUmXq4oIcDCWUQr3Lbn1fwEVwJcTPVt-IaQYtLnhV6DA6dsKDc9_83BsxjZsMyCZyu5lS9kScg-CUGcPOy4vi9T2MJ_ebp-Wl1ft-pCHh9zLYyVTea9gacD1c346Qibe9r1w8gub2hjyoZwB3dmlYxIm_taggfD6vBtmyTOwL_haJZmOyZGmvxGqN2daaco8PiXfnQ0QU-7tObFqKRqSxVl8uc4GetCjUwwKSfHjrjsr8l5936-huukaS3hqxJF_JEYI1ip996byOrpVbzv1zW2nFkmR05wXanTkoyAVO4wPMqwJzgk0YVZwjOHo9k6_nUi2uAr8XjEOA8EbxzDxKJLQsC3ZIn16WnBscmMNxsFWG-jZhLTM4DWNZfKHRjPTS9VsXYteDeaEc3kVEX9iqMSjeFVoChqvF9UDqP2WXyoeo7Z9I9eNOEj9iCPLk_eFporrVWmlyTUF7Ts	2023-12-18 16:55:58.488223+00
$1$1$XjD3glV9m4-Qf3OCxbKo5MA4BMlJQ-zk1wKIs51O_KySFhwJgJg21BJ-KDl5Ir_l32nX1zteE35KszfOo-cdYA	accept	01702331477921460562	r1KCu084pa7bEM1zQSCJe5pehX85Bi4_s7PVtpzNpos	eyJhbGciOiJSUzUxMiIsIng1dCNTMjU2IjoicjFLQ3UwODRwYTdiRU0xelFTQ0plNXBlaFg4NUJpNF9zN1BWdHB6TnBvcyJ9.eyJjb250ZW50SGFzaCI6IiQxJDEkWGpEM2dsVjltNC1RZjNPQ3hiS281TUE0Qk1sSlEtemsxd0tJczUxT19LeVNGaHdKZ0pnMjFCSi1LRGw1SXJfbDMyblgxenRlRTM1S3N6Zk9vLWNkWUEiLCJ0eXBlIjoiYWNjZXB0Iiwic2lnbmVkQXQiOjE3MDI5MTg1NjN9.GWFJOSn-uxGEf45-pA9gz98zF0oA8iteOXBM-Dds3ldwWGaT1X-rw22xpvBRWX3AQ5XtKzOnnuUJmHnCDBRrSsdJtQEencCYseqe5TXeSssSuB7FS1bBVUwfwstVKq4UM5642t_6LMUprTuOmQXfHxscux3J3SUxc2rW0H9jmx_0hZpxhDPP6POSWSRdu46uBPRLJ400xWkZT8luiUO9XaqMyXy7g61YbLbHu4ULY2ScGmazSofOFKOJMBQPI12yO8pO9SMJy-KghR0d-ToiNrOfH3eYXA-DqeoaZSxdjiZZn1FSFVr_jiHYTOqtKlFdRfldXy-JWEM2iGbW5oElraG1TW5Wv3FH3IsTyhrftyYZ0THH8k3bpNXSrcJT4X_yjjyB0aGwtaP5URmSsUxmUZkM2GRwzA-VeohPrBLsWZLEOHMgr7VSl-ejDLiD-SIACWfdabV4W7E7ExNJIk2xMO4tXb9rc13KMaBbm5jcT1xwq2XJ3iXSL10fFYYGf6FBWTbrFmY5l7q66-EZVsyXssjv3mRyQ392IxMFefvYer6lHeFPb3attNVRYasW3MMw0j7XjKOsP7Uy5biq8DQ24He5myNW2TIvQbcf3qyHY1aZldo_Tkz3LPenT4ZNUn0kb7GvIXWutqL9oBQu8DWakNWiC7iOIAk1fYeEKyefz3c	2023-12-18 16:56:03.972818+00
$1$1$Zvb29SLWVveoQuE5PHRLnCqa-wQXF33lP0DpEnUYVl01_bra0Kk2aqFL39tNxp2kwN-0oIe13thCkwjoiDXtBQ	accept	01702331477921460562	r1KCu084pa7bEM1zQSCJe5pehX85Bi4_s7PVtpzNpos	eyJhbGciOiJSUzUxMiIsIng1dCNTMjU2IjoicjFLQ3UwODRwYTdiRU0xelFTQ0plNXBlaFg4NUJpNF9zN1BWdHB6TnBvcyJ9.eyJjb250ZW50SGFzaCI6IiQxJDEkWnZiMjlTTFdWdmVvUXVFNVBIUkxuQ3FhLXdRWEYzM2xQMERwRW5VWVZsMDFfYnJhMEtrMmFxRkwzOXROeHAya3dOLTBvSWUxM3RoQ2t3am9pRFh0QlEiLCJ0eXBlIjoiYWNjZXB0Iiwic2lnbmVkQXQiOjE3MDI5MTg1NzB9.rKNc8Y0hI9SWG8Pm1S4jCiYt51z70Nm9LXCGexJfRPNuqmCVkQS9ew9CNhVRX9k-_lkwEIqXcIZuRBtXXPHZT5uVlQ69lVcNpBl2uVWc6Icnst7gawzssqidKQs0XunkqDAOraHove7BQe5gFFlfytoOvZ7NI0KR8_NBHlT1Y2RCD9YdhAsmRAKLlI4MuHNT-jsQHpzJtZ1hPKz1Bhjj98cz4J1hQycW9E4lwkQ1DH1dCS2kstN6T0cT1ML9oocvqv7XP-hv1-83rePEDm07BZtLd-0GhXEim3N9rUJ3fVCZk3hOWnG367vaHWqohE6FdXUt5zsSxKeIHxlYWOChH__bzegAY6WvrUhEL4SYzPvmVO2aLEi9FlOL1qj8H3bjFg2xDzunQ3h4QqsrwpbLRTzI8IKdkeqW46Gp5ljQkIOboR7_X05FSHsluDGB7O9QjOsgHYalmBYX5mdHFwDZ1k2nGksCEnMRNRe8I1FiaH4mi7FFHKqTMF2VB2Y7MUV2rLl9YvUFp9OI1P6g12qEz_aX-yOOJdrbn6gwo40BzFF88n6e5Rubq72l4UBl5bzplhPflo3SOLEkfe6Et4hlIs_dUls5ujjaNUKlvErsrCZUCqREplccIecmJGMYsUI8VaN32iAg-cM5f62jhnhQh-iqZiRzngIidQfHiKFKBTE	2023-12-18 16:56:10.138124+00
$1$1$Nn5Dmv6qtSbJgh9jfgen_CVzbHrRNyGH-40dpMflAfGwrl21lNbw7Lyty93acnSNHw38Nifq9xdAGi9GkJVErQ	accept	01702331477921460562	r1KCu084pa7bEM1zQSCJe5pehX85Bi4_s7PVtpzNpos	eyJhbGciOiJSUzUxMiIsIng1dCNTMjU2IjoicjFLQ3UwODRwYTdiRU0xelFTQ0plNXBlaFg4NUJpNF9zN1BWdHB6TnBvcyJ9.eyJjb250ZW50SGFzaCI6IiQxJDEkTm41RG12NnF0U2JKZ2g5amZnZW5fQ1Z6YkhyUk55R0gtNDBkcE1mbEFmR3dybDIxbE5idzdMeXR5OTNhY25TTkh3MzhOaWZxOXhkQUdpOUdrSlZFclEiLCJ0eXBlIjoiYWNjZXB0Iiwic2lnbmVkQXQiOjE3MDI5MTg1NzR9.XJbSCGACs3coI2odK7Weix_qVzK9MjW8J2Njyqv63Gdo9A9TN6Q6dt5uoDy-1uYZlmQ2fAaP7qgc2stXTmad92EWRXvDzo28-whFugwyxa1Y4gfpH9wuqFIa59IVyD7TW7o6Ss0bJWZTRDLQ-JGgUxX5eFk4CGNQMzyMxJPrsnZLudehsqbHRIMy0ntHSIwudwGndcugWe5cDzDYUpAEgmYPCAmFqAGayZBxg5E_r9FmLaU75eGe8J_axMCI_uIM81tOvSNr0ItecqxfGd2tZ4XQaQfnUyV7EamzFCuK2cmxrVMNnhWbLwgvmnChGuw96IPbVC14P-KWHQTGqUtIvfEdnF14Wxd7iylMS7lrDuPZJBK0sCv6hgopgOOVJCvAx5EmrL1avnfaS8NAHdCx3IxLxag8AQZONIgAaui8Vk41ZCjTtpoUGSNiJPDCsEaVG0Lzxr6v80Nhi2oKYC9aMx25cTvexrxHjl-xTUzMqfbkdMNwvZaArrchT3ofTvjKSrM7g4p2Gm7RLUfZ9ZJhN2LsJrc9Y1tfwTE1RmKzM3MPLTAtBzirL8_TgnTXTgb3B-s66wvzo_PODPMJaIKVmoQ9Z8HfSQT_FRGzYHLdYuFuQS5apKTG9RMDn_LncfAbjitxua4pUkkziqDAEoE1eXUhZLhMCSWneBDrLF_-jhs	2023-12-18 16:56:14.706325+00
$1$1$VNQO2H2jnGJXqPiwkK9WZtyvUCPwtA8-goq_MjQanGP94NDhr0sWsW1u1VZyRqUv05Q26ouMX2gNIiziRWF-zg	accept	01702331477921460562	r1KCu084pa7bEM1zQSCJe5pehX85Bi4_s7PVtpzNpos	eyJhbGciOiJSUzUxMiIsIng1dCNTMjU2IjoicjFLQ3UwODRwYTdiRU0xelFTQ0plNXBlaFg4NUJpNF9zN1BWdHB6TnBvcyJ9.eyJjb250ZW50SGFzaCI6IiQxJDEkVk5RTzJIMmpuR0pYcVBpd2tLOVdadHl2VUNQd3RBOC1nb3FfTWpRYW5HUDk0TkRocjBzV3NXMXUxVlp5UnFVdjA1UTI2b3VNWDJnTklpemlSV0YtemciLCJ0eXBlIjoiYWNjZXB0Iiwic2lnbmVkQXQiOjE3MDI5MTg1Nzl9.yB4o3jcH1i6-zfTDqKvD3Ow3AlrHOyKyF-ap7S8Q21ja3xDkPNiCxZK6WYwhCCd6wS3gbKvzHqyITv4xKI5iEHyWhhaz1WvkN77cNUAxfuGQDkt9pxm2sUEYuS9uGk5fjVMdgqsrHJciL8FOYT3qRmIDZBbJHTbKpgepwtQfQCrTcqkWdAMEwYaYZqGl9gJ2STWyRRopibk6hOpLOlLVgLSoid2tGWjXeC0rwgjjRwJLvZ75_9FpUpdU_3A3aXXDfQb4xYau23kVBcWmlBir7XuAlH1kmk4qs9bpZwC0yfSdiciEwnalA1tc7OQ3_LA4q1lWKMleqdiwSecUKjxlR-ji5clR39iuD9ERPBGyVweM7X4-6L_a1Sy-2pZCCwcv6RrnQ7eDoD3Lvlt9jfHgr5SsH7sU0EuF4VciTLS0_f8Y6F3sM71dVIMP-KFbIeg-cKnSEz4SGwvkTWjQ2J8baMNc28kNWK1a8FBqMEk58eNr-WVfgzv99Mr-CLfxUxPCZKE1xIu770FjE2aJWfWAi70vq0b_vMcIksMQr3hoYW26Nkd9DS0zv1vy4kpFUEw6e6ZcYu5UaSxWYZdJ1swKnxIIrYCadeNLsaQHrEzrNgnRxv3VnsO_HArPBULyY1wBy63glP1QDN1q2geQdaSQIwLuQkcpIF9cLqYfFca7VJ8	2023-12-18 16:56:19.873295+00
$1$1$vzwG3L60x-8dsA2PZR26mGvB_9CY0LHQZnwXxen2DNyM4kTdyAUFekissCet53ANOW_xAX28OmR_lNS6a3xhMw	accept	01702331477921460562	r1KCu084pa7bEM1zQSCJe5pehX85Bi4_s7PVtpzNpos	eyJhbGciOiJSUzUxMiIsIng1dCNTMjU2IjoicjFLQ3UwODRwYTdiRU0xelFTQ0plNXBlaFg4NUJpNF9zN1BWdHB6TnBvcyJ9.eyJjb250ZW50SGFzaCI6IiQxJDEkdnp3RzNMNjB4LThkc0EyUFpSMjZtR3ZCXzlDWTBMSFFabndYeGVuMkROeU00a1RkeUFVRmVraXNzQ2V0NTNBTk9XX3hBWDI4T21SX2xOUzZhM3hoTXciLCJ0eXBlIjoiYWNjZXB0Iiwic2lnbmVkQXQiOjE3MDI5MTg1ODd9.GI7Cns6b9mkrXLnJDKz69xGLzSs-9WsbV3KmQoP3y1SfzytvBDlzeA-W9G-5EmB_2YWhkwnhy2Mgc9W3xJM9PnTIEHQAwk_brcGrznjEswmRME471Dl7GdeOOiWP9M1jg4j4zKgo39rTpDsOv5-CrfKpVWADYHdeTNhM3xzZDBapa--3dV-rMai_QTZsd9y7n6t3fcss5ucAcMjf2d_eo9u4afq0dHpnMJyAohMdfiKp8JzFaglBMQ6-7xUsU4Y9R5587EhJ_vJNhevc_r2_PqU5CwzlUwX9RE7fqX_0GnJvku_mMMLzaam0_ufKHvaEvGO46CiJhj3tT7NWYOFTsKWmsXbX63st0izz3GJmNHl6-7OFZYpaV9xffB8ix9JgOZ8Hu04ScTISIEv12YhINqMb5_VHKIUWMpu0q1BEUu99qj1Gzu8_FXOgRgStGxdzs7H-42I-Fkx-0F6cWg8BHUdkiPa3mLVkfyb3MIbu26eqcMmA7hRcwu2N-RiMsvbLGkNFFbdujdy50r1XbYq09Qjzi0n7pqYSvEhZMp5yQN5CknL0wxsKZoGYbJKEscDEbb5HzVfLFk7EbOUVesUQAGonTsrHlIJyykph0evgzKYlOklxwHFKg7oTMCqFtLdTEr8S5u71s4ZHNiwBoVabUrCel610DJtp5wjNkWhn4lI	2023-12-18 16:56:27.304842+00
$1$1$9ED-_sP5l6EgUfzgm4NGzmDHeuWpOCmI3SYQ9daKG1wlaZSx9GDLZVnH2VI0AkadRce33AogqYFpUGp5AqcTEQ	accept	01702331477921460562	r1KCu084pa7bEM1zQSCJe5pehX85Bi4_s7PVtpzNpos	eyJhbGciOiJSUzUxMiIsIng1dCNTMjU2IjoicjFLQ3UwODRwYTdiRU0xelFTQ0plNXBlaFg4NUJpNF9zN1BWdHB6TnBvcyJ9.eyJjb250ZW50SGFzaCI6IiQxJDEkOUVELV9zUDVsNkVnVWZ6Z200Tkd6bURIZXVXcE9DbUkzU1lROWRhS0cxd2xhWlN4OUdETFpWbkgyVkkwQWthZFJjZTMzQW9ncVlGcFVHcDVBcWNURVEiLCJ0eXBlIjoiYWNjZXB0Iiwic2lnbmVkQXQiOjE3MDI5MTg1OTR9.DTNi-gyLHQFs8lUXdBFrCgvYOobkhOYTayAPcnbZXxrPxzYWvxqay3GChQicIOM-nm-DFh_t6K5YOwjUqDxqmZ4t0djxdZr3xpUJQoO0DLS-2aBCwykaE0xc3uI4OXBAmSpOOLt6-KKyCeM6WQVULZZ0b39H_hbUrCAO7DYR6OHxdJFbo4zEeI_vyQimXjHz5G_HjiGSBmPVae0GwykvxshgumREoVX1aJuw3KMCffDqDXOm9d2LeALtY9yZf0DThQMwuhprX9JW_5cOHSOynmTJGv3V6bgW2wIAYhVFgenyqD8TjqY6P3B9oe66YwROeDnP3y3O3miNaP5rI-_A7Xui43nDB33VXyCdB4auYruAARDt-vf7LBpni7gsyPe_Poj5QYMoxN0Z6duvjMD0rYDcg2tV-0YqkovN5YLEvNvCzlC5LILI6Kn1JZkl_CwcfxWR_EPUMinoBJGNfcubhT4TISLAZ1bQksTYyGLngsu8ZzNCFYDT2SFjFRY5nPORdxkvnmrdlV8rLnu_toN2mC2QCicETA88cQYTGmd9rxaJQQcn2KzRWGwFmPjIzdiMYbtL2D5Fw96biSXlTHqgW5xWCn6VhsCJVPNXISFgtex2O6a8liLI5g6L23Gz2no9h8BHbKTJYlISFvikDFGGwYWjxqzpN7WN-u54r_sMIrA	2023-12-18 16:56:34.326817+00
\.


--
-- Data for Name: grants_delegated_service_connection; Type: TABLE DATA; Schema: contracts; Owner: -
--

COPY contracts.grants_delegated_service_connection (hash, content_hash, outway_peer_id, delegator_peer_id, service_peer_id, service_name, certificate_thumbprint) FROM stdin;
\.


--
-- Data for Name: grants_delegated_service_publication; Type: TABLE DATA; Schema: contracts; Owner: -
--

COPY contracts.grants_delegated_service_publication (hash, content_hash, directory_peer_id, delegator_peer_id, service_peer_id, service_name, service_protocol) FROM stdin;
\.


--
-- Data for Name: grants_peer_registration; Type: TABLE DATA; Schema: contracts; Owner: -
--

COPY contracts.grants_peer_registration (hash, content_hash, directory_peer_id, peer_id, peer_name) FROM stdin;
\.


--
-- Data for Name: grants_service_connection; Type: TABLE DATA; Schema: contracts; Owner: -
--

COPY contracts.grants_service_connection (hash, content_hash, consumer_peer_id, service_peer_id, service_name, certificate_thumbprint) FROM stdin;
$1$4$gJ69d481g67yStUFCmHeS_oNSgsA5wJrllh6xqD1aj8e6lu7hZgAYtoOx6x70icR8LAzc0y37JHufBheeR6k1g	$1$1$5v4JRb7Mn_Ac-tkrS7cbiYDLcU-Op0rQjy5SmQG3_U8rEwji6Arf638cSNNtox1Y5EwywO4bekhF1TX527Vq7A	01702331477921460562	01702331477921460562	fictief-np-loket-backend	r1KCu084pa7bEM1zQSCJe5pehX85Bi4_s7PVtpzNpos
$1$4$Bg98zPrKXN1mecK8t2d_4RKigsg2-ab4zJdb3LSilbQI1MXQ8tft1nXVDPvGnI5MGoLqyybyiFrpMLpoPzVU1w	$1$1$4BEQlLR2zaiVpYgju94T70jKLKXcSQybCH1eRlTujgVJMEX3asJhyxM1VzFVnlMraEcR-jzV05AJ6ETrQsGSYA	01702331477921460562	01702331477921460562	fictief-bz-loket-backend	r1KCu084pa7bEM1zQSCJe5pehX85Bi4_s7PVtpzNpos
$1$4$jD5XyRCB3QwiUT9ulDWs7sMsnVHn0q3sAvfAhUD88h8UsOBB64VOcclh1C9WUo-vmG1CARcQ7_F3XWt1jZ1JCA	$1$1$dV2tscHYXn0ILfBEQljRvsZqKcq98HyZQ6PeEsDCQFDBrhSRwWwTBCGKL73aGpbFaS8Rhs-l2ybLUFJpOotGsg	01702331477921460562	01702331477921460562	fictief-coa-loket-backend	r1KCu084pa7bEM1zQSCJe5pehX85Bi4_s7PVtpzNpos
$1$4$6_4wcpTdMY8W-Y7HhwElRBp-iWm0dFQWyo8DD5FCbQfPtoFckEocuKpGtsXER8SLkg0ei-hZ8dwCa8NJS_nAeQ	$1$1$DNApszMpCbx2V_ESm76YyOjATkLoBQWGL6myPVZQtQFJpeqtO6NzhDnZoL6PxbhEtsPdIuCFShpRIdYZZKglFw	01702331477921460562	01702331477921460562	fictief-dji-loket-backend	r1KCu084pa7bEM1zQSCJe5pehX85Bi4_s7PVtpzNpos
$1$4$yCGoz_f-LOEQgZnnVWJBxL6Koqr2D3FYOjHPHdA3dySJNvO0QQO9wVB449nBuPAi8akjTTsAOsmA17Uvn36bMg	$1$1$Ygx91nyNS7sy3bVljmd_AL7LLRBdSfumx8LRt46XZF3YLJwirggWgppIf9a5BVjWnATHvFHLyR9GjkYWbNMkAg	01702331477921460562	01702331477921460562	fictief-dvo-loket-backend	r1KCu084pa7bEM1zQSCJe5pehX85Bi4_s7PVtpzNpos
$1$4$0BncA84Iys0ic1UJHUTXYbonJN0gd8WWf8wD26WWbdEPcJFncdYDeGTXOXv2whlVyfPS2_ftS57ZykDRdvP_Aw	$1$1$ngu35eMhPjhASj2w9xRq57cqp_670FmHDDSKQUtd-XyRcYAJRdfy6eJ5NiPV5F_ADX9E5Tsqdjz_RzuW0s2X4w	01702331477921460562	01702331477921460562	fictief-dto-loket-backend	r1KCu084pa7bEM1zQSCJe5pehX85Bi4_s7PVtpzNpos
$1$4$IpXantEUXGzVMkXtiai1p-OmSuKpzJJWKBdfWintntqHkh7lwaUdLmYUfO_W5sNRI3xB8kHNNpqQ6NNyYK-1hg	$1$1$HRpD3UHDfQ8hLlp0P0e93Jw71aOntMIx2pYmVvs9-tDCn4REotFC2Ipcp8IlJPnWhM9ZD4Jbr3c5xJwxnjltLg	01702331477921460562	01702331477921460562	fictief-dtv-loket-backend	r1KCu084pa7bEM1zQSCJe5pehX85Bi4_s7PVtpzNpos
$1$4$nIky4Awj121QdH2P1ItQ3_49uMnbe17EMZEEzfjUYfbKWb-W07GOqUbmQCCB35dUdhlm23QpkGv2Yq2fk1rPSw	$1$1$_lNvG7GvHjnIMMlLgQyHYu9pDh15EFXZ4Lq4llgdzTHSDrIufFVirUp5nysf7bNqMLnmOP9PZQx7P-dNWzt75Q	01702331477921460562	01702331477921460562	fictief-ind-loket-backend	r1KCu084pa7bEM1zQSCJe5pehX85Bi4_s7PVtpzNpos
$1$4$rrDkbN23EnCksjDamyAgAaEPXYpvSMph_z_wss9Etauv4gClMZ9wJFG2XUdsgYjlkliZ1FjV2a-06DxOLVCWTw	$1$1$9HlITyTXcn_OFnYc4dhe72LXnbQtl3r02Oo6qVJbL3WwkvUTKLllaE2tNlgvtgtlzyXTq8GGJSBTNHDL7iEYWw	01702331477921460562	01702331477921460562	fictief-kmar-loket-backend	r1KCu084pa7bEM1zQSCJe5pehX85Bi4_s7PVtpzNpos
$1$4$ZsgN6OOsfoCRGiNKHZKhY8EupzqKSMXLtFLdGo06osRkK8eDgtyl8jcwEXLXPyOIBjlLasJAdvpcBpS_jtBt6g	$1$1$UVM0sS9eypMFT7Naj_QdP6nwphcMPKSkTXrnA2VyI8J_mbutPwPJcY0IydGENvdpNovnSna3ZvYUyeEystbkTw	01702331477921460562	01702331477921460562	fictief-np-sigma-backend	r1KCu084pa7bEM1zQSCJe5pehX85Bi4_s7PVtpzNpos
$1$4$RgvO4fmw9QJGbaicq44lP652FQggNGJCOl3Omlx5S8nxMqvhZBKYvFFKuN_n5erzlKJh6-ztyEwacq-Rh5FalA	$1$1$JcSOrC-GgnLFNTQRysvTsJz5WlKS4qsQD4N5cz2TY0pnAhWKXGueILQ6CWMfqLxm9g5VppBSLKN9G50-7CGJGw	01702331477921460562	01702331477921460562	fictief-np-bvv-backend	r1KCu084pa7bEM1zQSCJe5pehX85Bi4_s7PVtpzNpos
$1$4$GNfCOSQw26qX4D_mo5wroHSyvtRbtSB6s95qIPuPouNFChZlwHGttfFz_CX02Limt2y2IMOch6H4KqznacswEg	$1$1$g9K0KKk2kWCV451HzQ_CPCcilLU1-1BcAAdAevf5gucsjVUYbSapBY2WNGsr-00PBM0uSRk3n__3XVkO_JpRBg	01702331477921460562	01702331477921460562	fictief-bz-sigma-backend	r1KCu084pa7bEM1zQSCJe5pehX85Bi4_s7PVtpzNpos
$1$4$vl16XFXLl0sXDs1ZsjlcviMaxtAl1CnzTpPOY2UK2CS3LwNsPuQubXF7efFapM_YEww56LeTYL_yknjYxx8B2w	$1$1$BX9rNDcVm-7DF2CU8gRcJQDMGJS5bIuqHz9rj71a_BoNyeRKc17m9LuRqFm6aR2YYQlpjKgLMEYcpiUEpEftiw	01702331477921460562	01702331477921460562	fictief-coa-sigma-backend	r1KCu084pa7bEM1zQSCJe5pehX85Bi4_s7PVtpzNpos
$1$4$bwKwzDpwEPZMYwKZjtrFRqWYupbWXvHt2oXeJL3fZfcl269FKMJGSPwbv1zGw3v1GrY206mZwDV48UQ2k6DG1Q	$1$1$XjD3glV9m4-Qf3OCxbKo5MA4BMlJQ-zk1wKIs51O_KySFhwJgJg21BJ-KDl5Ir_l32nX1zteE35KszfOo-cdYA	01702331477921460562	01702331477921460562	fictief-dji-sigma-backend	r1KCu084pa7bEM1zQSCJe5pehX85Bi4_s7PVtpzNpos
$1$4$313lqUHCB5v-Udl_TNcKuYJnkqCgWstrgemWuImf2SdKEM1WVZ_uXxPElosss-CkxmpxCd4LtHznnk43zwul6A	$1$1$Zvb29SLWVveoQuE5PHRLnCqa-wQXF33lP0DpEnUYVl01_bra0Kk2aqFL39tNxp2kwN-0oIe13thCkwjoiDXtBQ	01702331477921460562	01702331477921460562	fictief-dvo-sigma-backend	r1KCu084pa7bEM1zQSCJe5pehX85Bi4_s7PVtpzNpos
$1$4$fommQ2hIejB9S5Wju1uyeF2MTShljLEssn_ujWiZVjbvyDce7JUAGLsU53d7-90BsUSYVLs6cp--thGaemv2KQ	$1$1$Nn5Dmv6qtSbJgh9jfgen_CVzbHrRNyGH-40dpMflAfGwrl21lNbw7Lyty93acnSNHw38Nifq9xdAGi9GkJVErQ	01702331477921460562	01702331477921460562	fictief-dto-sigma-backend	r1KCu084pa7bEM1zQSCJe5pehX85Bi4_s7PVtpzNpos
$1$4$x_JC72hsltfy4zcAkYR-JXjG3O1vCrQTRPoF50v_6ztDv_O_w9uMFvMyNhPklLKowEFTiRNr4wnfS2pVoX9IKQ	$1$1$VNQO2H2jnGJXqPiwkK9WZtyvUCPwtA8-goq_MjQanGP94NDhr0sWsW1u1VZyRqUv05Q26ouMX2gNIiziRWF-zg	01702331477921460562	01702331477921460562	fictief-dtv-sigma-backend	r1KCu084pa7bEM1zQSCJe5pehX85Bi4_s7PVtpzNpos
$1$4$gnUgRTSRsT4S3C7fTN5zsg_jQtnLwlToFpIsvmUoM8FXPpITX62LKC_Z6nvJYCEEVjQ5JaWqb8JLgxjjOZCzmA	$1$1$vzwG3L60x-8dsA2PZR26mGvB_9CY0LHQZnwXxen2DNyM4kTdyAUFekissCet53ANOW_xAX28OmR_lNS6a3xhMw	01702331477921460562	01702331477921460562	fictief-ind-sigma-backend	r1KCu084pa7bEM1zQSCJe5pehX85Bi4_s7PVtpzNpos
$1$4$-n3rSPFBIGs4VHwDZkNzymfD1lC-ieXp6TkSFMnRdEXlh5eLiByp2T38cgRcM8aYdDp4WBeZYfzzgag6d5EavA	$1$1$9ED-_sP5l6EgUfzgm4NGzmDHeuWpOCmI3SYQ9daKG1wlaZSx9GDLZVnH2VI0AkadRce33AogqYFpUGp5AqcTEQ	01702331477921460562	01702331477921460562	fictief-kmar-sigma-backend	r1KCu084pa7bEM1zQSCJe5pehX85Bi4_s7PVtpzNpos
\.


--
-- Data for Name: grants_service_publication; Type: TABLE DATA; Schema: contracts; Owner: -
--

COPY contracts.grants_service_publication (hash, content_hash, directory_peer_id, service_peer_id, service_name, service_protocol) FROM stdin;
$1$3$_FtcXyzQul0kOkHneXmR4wWzje0LxjHLzW9rznsvalsuy1jHUT-Pro28_8-uPqGEMMCCnuDBH7Os-UzZ7i_opA	$1$1$EvDjWbYqRoWOUArY8v64jjGoA4nbA4tniX-Saa3tk_GRZO_hlMJWYwNwXWOtgFTnQOVM9D6geIgxPbVVrivrWQ	12345678901234567899	01702331477921460562	fictief-np-bvv-backend	PROTOCOL_TCP_HTTP_1.1
$1$3$lEtd2H2RlJjwt2f0VUnvMqwCHqS5_9N8cydBs2XtV2EpXf4gtXpcSCJs36zsXVxpHJcCFH0nkz4nzCjxlsQnDw	$1$1$QH89503RLswUpsCJ-2ZNcwxrKl3ppiX643Apy-G0qU0qolWt4kV9aIKIzrfRenaSVrVneloBJHzJhTAyANlbVg	12345678901234567899	01702331477921460562	fictief-np-sigma-backend	PROTOCOL_TCP_HTTP_1.1
\.


--
-- Data for Name: certificates; Type: TABLE DATA; Schema: peers; Owner: -
--

COPY peers.certificates (certificate_thumbprint, peer_id, certificate) FROM stdin;
r1KCu084pa7bEM1zQSCJe5pehX85Bi4_s7PVtpzNpos	01702331477921460562	\\x308206673082044fa003020102021415a4f7f10bcb2f410ad97eaa863de64e4d999009300d06092a864886f70d01010d0500305f310b3009060355040613024e4c311630140603550408130d4e6f6f72642d486f6c6c616e643112301006035504071309416d7374657264616d31163014060355040a130d436f6d6d6f6e2047726f756e64310c300a060355040b13034e4c58301e170d3233313231313231343630305a170d3236313231303231343630305a30819a310b3009060355040613024e4c3110300e06035504081307557472656368743110300e0603550407130755747265636874310f300d060355040a1306736861726564313730350603550403132e6d616e616765722d6673632d6e6c782d6d616e616765722d65787465726e616c2e6d696772617469656b6574656e311d301b06035504051314303137303233333134373739323134363035363230820222300d06092a864886f70d01010105000382020f003082020a0282020100dba78a07eac843512a2b82df33c30f56bde128ff35b5cd6ef6ff02183515eb22460b8672d535a335489d4c96b5ccf69a0f273c3eddbf8cc7914f3f7404e43f94c5031bc7623984407b545f98ddee80f398b595a39c425f56b5f95cc2551b56f7a9dde01d33422bb93450858732048f2de770af2c95bbe8beac93c1621e40db9178ed1f7befa11191131609a1e87072cc251a0e073eb0fbf262665b1c13eb913231e893b3a5ccfce235934bbed108f10b98dbe92bbecb71e1133a3c9115b1f2727c5b823e8a62e1522392bcba52141d7a66e7e589ff2e7296364d0341343f9dbbab1aeb536d6bfd21b022c0fd5771c286752b79434d6780fe342b12f197c44cf074380605bf26d6c5df8b022ba2a889baba11ca4b1a785366b86deb6911e71f2e11516653f42e0a3f5b568ba355f6c89bb41b9ec54149dd849ad76c466b5517bb39f0f4b0d90b8f5351c6acf1520113abe0eaa9f540d2c9a2b14e80a41f6182fa8783de71a1989f6a84eabe24416afa009442f4bffa57f1bbdfaa4cf2401c1a9037baf60977f66af98a6ead7dc4f677f923e783135f9106a6ec1ea71d6cafd846640e3f7494686125e996c5d4c56db7cdd7afbc9d6f971122d8da82631859caa4ce4009327901863aa88c94379906ae6424b1b94a1e239ce916b406b4263dec3fa8c8ddb3512b65be8f6130aa4d8c5ac926ab91ca65e69920203bdd569c10464b0203010001a381de3081db300e0603551d0f0101ff0404030205a0301d0603551d250416301406082b0601050507030106082b06010505070302300c0603551d130101ff04023000301d0603551d0e041604143826a7472b7e3d568ea1774ea93807baf407acdc301f0603551d230418301680145ae041511187acda6887c000c3065ce2792d89fd305c0603551d1104553053822e6d616e616765722d6673632d6e6c782d6d616e616765722d65787465726e616c2e6d696772617469656b6574656e8221696e7761792d6673632d6e6c782d696e7761792e6d696772617469656b6574656e300d06092a864886f70d01010d050003820201009b2d941e1841446a937132c9bc44ed85a31836f990adfb296b861e5aa0d4b51ffc146ea83b64d0df751a35943b849b3f6ff8c67978897f5fd992bc782748134da4075f405e6cb249d39e2bcd9f8b59b10a5735aff0e9ae6d918f8634dec777d3c80c31d075a9939a89f33351460c0bd35bbe5e5e3460d26ff94d084d324e60316cd2b6b716543b6bba61d3f8aa18084e4cab92952275bc81a92d215f086b31c0c3dacce8a85b3c4187686deda0a3277ca0abfada672d6a6f5a02771954147990d817dfba285c96a3a443c71e052a1617c951b20a3e349c64cc3f8d2e055e7baca485356be40599348f8cb9def5707bb386502a051fa69ee942aee06c5010f9652887783706d57af033c93de6375797936e05ac52d5c018fa9398851e5a590dc141e451e101e7f592f0ad01ba6d462444e205ce7b25411a9a17ed51914e5ddad17ac246ae7bfe6c649a2e1160c6784df8be5111652063d605c12c018d0ac874b1a71151b72efce6a6a84a77c607f2f271277eb8dca5395baf183a482df3fd3cf3123ab705818dd02de80af5ff8fb53dfcc3fc87a383459a3dd80c77189f8f21f7ca40d7c44c28588b7b48e1e6e7d170507a937669440a909d18c23644c2d4734f392a994df803ccfb204decf9ddd8d9b325231bc3ec949b218daff99b4486e4223c05ca5e2a53ec498873a9ef14a5b437285c395b17600d92fccbab57057ca108
U8Q1EXydxE--72ESaGOMHy-4PJpJ9LA2lYdHuOPjJqk	12345678901234567899	\\x3082066130820449a00302010202142bf0ba0903a66c72c8b86ee300e9207fae8ee227300d06092a864886f70d01010d0500305f310b3009060355040613024e4c311630140603550408130d4e6f6f72642d486f6c6c616e643112301006035504071309416d7374657264616d31163014060355040a130d436f6d6d6f6e2047726f756e64310c300a060355040b13034e4c58301e170d3233313231313230303130305a170d3236313231303230303130305a3081a8310b3009060355040613024e4c3110300e06035504081307557472656368743110300e060355040713075574726563687431273025060355040a131e72696a6b736469656e73742d7265616c69737469736368652d64656d6f73312d302b060355040313246d616e616765722d6673632d6e6c782d6d616e616765722d65787465726e616c2e727264311d301b06035504051314313233343536373839303132333435363738393930820222300d06092a864886f70d01010105000382020f003082020a0282020100a86e510c06ecffb77972afb65a155bb55b129f5d2bac39288d1e211190427d485e1ae6e73d77773e18282087d817cf66b8c3ea9a35a1891fb9ee0dd0c9c5b19977101f85c9927506c045aca724abdf8c823916f14cdfe5672b182950996dcc9bbb796af5f623ec57c78aaa72bce244d95268c8c5c2900e841ffaf9a8a1520f44f2a38a5882e7be5eb26c44a12c05628d9dd1329470c0df4ce28d60a15b6122afc98cff0e44f3394b2f0156114f9f11a5d9f76ffa061f03a797cd3abdf65fd3859d52cd8e431308db9d2e7a7b97b8e20b10a482285f35c8a970d4c6cd3bbddd0b9121ca3c5a330d8888edadeae32fbb63a8e324d520a2562de63a02c62677e1d8adf875edffbad07084dcc1fd175c0e2702b0a7f556d781ce58f4cbc3f24d458bbcd52ad07b5ce7d89a317a14235ac0221d70a66b1fbc6fa3154aa5e481dd3417c6d1474db45b10eebd4c3fa30e4d370510d5af69326011639902b234d6b68e228bd45170b25cfdd021bbc7cd6e64f1f72be9908a52f9ae2451d439aadae564f756d7a034bfdb7d4aee53ddb7e6bb626239a2a33b8b1519fdae1ec4529cd1dabeb467ae7c1c97c3eb0f751a97830fcf3ee1f178189e04846c1f07fd746074282ea7f46015e3325c5cd78307f4ef242a10a4d427a886d97f043e430e42a3f0050b9b2e7672ff9d8f92e393513f5949727a26b0ad993c525186cdbf8e0b729c76e70203010001a381ca3081c7300e0603551d0f0101ff0404030205a0301d0603551d250416301406082b0601050507030106082b06010505070302300c0603551d130101ff04023000301d0603551d0e041604147c494af41e3480d4cfd407b0bea19c528c4e04a7301f0603551d230418301680145ae041511187acda6887c000c3065ce2792d89fd30480603551d110441303f82246d616e616765722d6673632d6e6c782d6d616e616765722d65787465726e616c2e7272648217696e7761792d6673632d6e6c782d696e7761792e727264300d06092a864886f70d01010d050003820201007dc219c797b930ff772e52f740fd325478b854d1577700b647c02f3aeec9c4bda9d7fb2f87af100fb97e72d1e80f978300c45f0c90226b1f554dbc2e1c91d9c2a1205373dab6a18c251d81e20efd4c569a5f73caacbf4589af156157af771f1a0fcad068d6c1344c798acb9f0c188214e158c1a8a221ba11bc87b026d5baf7d058df6cf5f489780ea64fe8ea265c4ed39e5da8f21d43a2735004b6f0b3dafab158e67f71f71df96d25ec95fabc1fd8b5063001a166506c366843069b03063781c53778ef6437ae5a6f4163e4cfc5f1713242c69cf23a624dcbab87e783ef7617004d986c404436bb144ddabd56eb0b1802193470fd26748e7805709e10e275d01e839e8c7945486a03a762debe5991052abefbf0062420224bc988f72e482a08207b17737fff868f467afbd3af6ed1bf92d206b5f2ed1a9688fe1385729d86ab6bb903c4e4b742ece646c47ec106563ed5880f19e52065f7a69ed75ba2ad135f1b80c49772cf468f542ed4ac51294f9e6a9c2889b32b063ea96403bae416a741cdd71f6c461f94ae88cb458b7355e8c6b1e60f69a5788b5f9411d26dbf20ec25b475b2ed138faee5576fd96a551813512425f3d6cc5bec4ec7392200476c2fdc9c2ad0fe6da05de2c8951af260cfe0c40a655dd54cc45d47a95e8dc290fadba0a1ec0f3c4ad5294b9588fe3adbaf34385a9aef086f7d05c7d6170b3d46d8b712
\.


--
-- Data for Name: peers; Type: TABLE DATA; Schema: peers; Owner: -
--

COPY peers.peers (id, name, manager_address) FROM stdin;
01702331477921460562	shared	https://manager-fsc-nlx-manager-external.migratieketen:8443
12345678901234567899	rijksdienst-realistische-demos	https://manager-fsc-nlx-manager-external.rrd:8443
\.


--
-- Data for Name: schema_migrations; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.schema_migrations (version, dirty) FROM stdin;
1	f
\.


--
-- Name: content content_pk; Type: CONSTRAINT; Schema: contracts; Owner: -
--

ALTER TABLE ONLY contracts.content
    ADD CONSTRAINT content_pk PRIMARY KEY (hash);


--
-- Name: contract_signatures contract_signatures_pk; Type: CONSTRAINT; Schema: contracts; Owner: -
--

ALTER TABLE ONLY contracts.contract_signatures
    ADD CONSTRAINT contract_signatures_pk PRIMARY KEY (content_hash, signature_type, peer_id);


--
-- Name: grants_delegated_service_connection grants_delegated_service_connection_pk; Type: CONSTRAINT; Schema: contracts; Owner: -
--

ALTER TABLE ONLY contracts.grants_delegated_service_connection
    ADD CONSTRAINT grants_delegated_service_connection_pk PRIMARY KEY (hash);


--
-- Name: grants_delegated_service_publication grants_delegated_service_publication_pk; Type: CONSTRAINT; Schema: contracts; Owner: -
--

ALTER TABLE ONLY contracts.grants_delegated_service_publication
    ADD CONSTRAINT grants_delegated_service_publication_pk PRIMARY KEY (hash);


--
-- Name: grants_peer_registration grants_peer_registration_pk; Type: CONSTRAINT; Schema: contracts; Owner: -
--

ALTER TABLE ONLY contracts.grants_peer_registration
    ADD CONSTRAINT grants_peer_registration_pk PRIMARY KEY (hash);


--
-- Name: grants_service_connection grants_service_connection_pk; Type: CONSTRAINT; Schema: contracts; Owner: -
--

ALTER TABLE ONLY contracts.grants_service_connection
    ADD CONSTRAINT grants_service_connection_pk PRIMARY KEY (hash);


--
-- Name: grants_service_publication grants_service_publication_pk; Type: CONSTRAINT; Schema: contracts; Owner: -
--

ALTER TABLE ONLY contracts.grants_service_publication
    ADD CONSTRAINT grants_service_publication_pk PRIMARY KEY (hash);


--
-- Name: certificates certificates_pk; Type: CONSTRAINT; Schema: peers; Owner: -
--

ALTER TABLE ONLY peers.certificates
    ADD CONSTRAINT certificates_pk PRIMARY KEY (certificate_thumbprint);


--
-- Name: peers peers_pk; Type: CONSTRAINT; Schema: peers; Owner: -
--

ALTER TABLE ONLY peers.peers
    ADD CONSTRAINT peers_pk PRIMARY KEY (id);


--
-- Name: schema_migrations schema_migrations_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.schema_migrations
    ADD CONSTRAINT schema_migrations_pkey PRIMARY KEY (version);


--
-- Name: grants_delegated_service_connection_content_hash_idx; Type: INDEX; Schema: contracts; Owner: -
--

CREATE INDEX grants_delegated_service_connection_content_hash_idx ON contracts.grants_delegated_service_connection USING btree (content_hash);


--
-- Name: grants_delegated_service_connection_delegator_peer_id_idx; Type: INDEX; Schema: contracts; Owner: -
--

CREATE INDEX grants_delegated_service_connection_delegator_peer_id_idx ON contracts.grants_delegated_service_connection USING btree (delegator_peer_id);


--
-- Name: grants_delegated_service_connection_outway_peer_id_idx; Type: INDEX; Schema: contracts; Owner: -
--

CREATE INDEX grants_delegated_service_connection_outway_peer_id_idx ON contracts.grants_delegated_service_connection USING btree (outway_peer_id);


--
-- Name: grants_delegated_service_connection_service_peer_id_idx; Type: INDEX; Schema: contracts; Owner: -
--

CREATE INDEX grants_delegated_service_connection_service_peer_id_idx ON contracts.grants_delegated_service_connection USING btree (service_peer_id);


--
-- Name: grants_peer_registration_directory_peer_id_idx; Type: INDEX; Schema: contracts; Owner: -
--

CREATE INDEX grants_peer_registration_directory_peer_id_idx ON contracts.grants_peer_registration USING btree (directory_peer_id);


--
-- Name: grants_peer_registration_peer_id_idx; Type: INDEX; Schema: contracts; Owner: -
--

CREATE INDEX grants_peer_registration_peer_id_idx ON contracts.grants_peer_registration USING btree (peer_id);


--
-- Name: grants_service_connection_consumer_peer_id_idx; Type: INDEX; Schema: contracts; Owner: -
--

CREATE INDEX grants_service_connection_consumer_peer_id_idx ON contracts.grants_service_connection USING btree (consumer_peer_id);


--
-- Name: grants_service_connection_content_hash_idx; Type: INDEX; Schema: contracts; Owner: -
--

CREATE INDEX grants_service_connection_content_hash_idx ON contracts.grants_service_connection USING btree (content_hash);


--
-- Name: grants_service_connection_service_peer_id_idx; Type: INDEX; Schema: contracts; Owner: -
--

CREATE INDEX grants_service_connection_service_peer_id_idx ON contracts.grants_service_connection USING btree (service_peer_id);


--
-- Name: grants_service_publication_directory_peer_id_idx; Type: INDEX; Schema: contracts; Owner: -
--

CREATE INDEX grants_service_publication_directory_peer_id_idx ON contracts.grants_service_publication USING btree (directory_peer_id);


--
-- Name: grants_service_publication_service_peer_id_idx; Type: INDEX; Schema: contracts; Owner: -
--

CREATE INDEX grants_service_publication_service_peer_id_idx ON contracts.grants_service_publication USING btree (service_peer_id);


--
-- Name: certificates_certificate_thumbprint_idx; Type: INDEX; Schema: peers; Owner: -
--

CREATE INDEX certificates_certificate_thumbprint_idx ON peers.certificates USING btree (certificate_thumbprint);


--
-- Name: certificates_peer_id_idx; Type: INDEX; Schema: peers; Owner: -
--

CREATE INDEX certificates_peer_id_idx ON peers.certificates USING btree (peer_id);


--
-- Name: contract_signatures contract_signatures_content_hash_fk; Type: FK CONSTRAINT; Schema: contracts; Owner: -
--

ALTER TABLE ONLY contracts.contract_signatures
    ADD CONSTRAINT contract_signatures_content_hash_fk FOREIGN KEY (content_hash) REFERENCES contracts.content(hash) MATCH FULL ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: contract_signatures contract_signatures_peer_id_fk; Type: FK CONSTRAINT; Schema: contracts; Owner: -
--

ALTER TABLE ONLY contracts.contract_signatures
    ADD CONSTRAINT contract_signatures_peer_id_fk FOREIGN KEY (peer_id) REFERENCES peers.peers(id) MATCH FULL ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: grants_delegated_service_connection grants_delegated_service_connection_content_hash_fk; Type: FK CONSTRAINT; Schema: contracts; Owner: -
--

ALTER TABLE ONLY contracts.grants_delegated_service_connection
    ADD CONSTRAINT grants_delegated_service_connection_content_hash_fk FOREIGN KEY (content_hash) REFERENCES contracts.content(hash) MATCH FULL ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: grants_delegated_service_connection grants_delegated_service_connection_delegator_peer_id_fk; Type: FK CONSTRAINT; Schema: contracts; Owner: -
--

ALTER TABLE ONLY contracts.grants_delegated_service_connection
    ADD CONSTRAINT grants_delegated_service_connection_delegator_peer_id_fk FOREIGN KEY (delegator_peer_id) REFERENCES peers.peers(id) MATCH FULL ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: grants_delegated_service_connection grants_delegated_service_connection_outway_peer_id_fk; Type: FK CONSTRAINT; Schema: contracts; Owner: -
--

ALTER TABLE ONLY contracts.grants_delegated_service_connection
    ADD CONSTRAINT grants_delegated_service_connection_outway_peer_id_fk FOREIGN KEY (outway_peer_id) REFERENCES peers.peers(id) MATCH FULL ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: grants_delegated_service_connection grants_delegated_service_connection_service_peer_id_fk; Type: FK CONSTRAINT; Schema: contracts; Owner: -
--

ALTER TABLE ONLY contracts.grants_delegated_service_connection
    ADD CONSTRAINT grants_delegated_service_connection_service_peer_id_fk FOREIGN KEY (service_peer_id) REFERENCES peers.peers(id) MATCH FULL ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: grants_peer_registration grants_peer_registration_directory_peer_id_fk; Type: FK CONSTRAINT; Schema: contracts; Owner: -
--

ALTER TABLE ONLY contracts.grants_peer_registration
    ADD CONSTRAINT grants_peer_registration_directory_peer_id_fk FOREIGN KEY (directory_peer_id) REFERENCES peers.peers(id) MATCH FULL ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: grants_peer_registration grants_peer_registration_peer_id_fk; Type: FK CONSTRAINT; Schema: contracts; Owner: -
--

ALTER TABLE ONLY contracts.grants_peer_registration
    ADD CONSTRAINT grants_peer_registration_peer_id_fk FOREIGN KEY (peer_id) REFERENCES peers.peers(id) MATCH FULL ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: grants_service_connection grants_service_connection_content_hash_fk; Type: FK CONSTRAINT; Schema: contracts; Owner: -
--

ALTER TABLE ONLY contracts.grants_service_connection
    ADD CONSTRAINT grants_service_connection_content_hash_fk FOREIGN KEY (content_hash) REFERENCES contracts.content(hash) MATCH FULL ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: grants_service_connection grants_service_connections_consumer_peer_id_fk; Type: FK CONSTRAINT; Schema: contracts; Owner: -
--

ALTER TABLE ONLY contracts.grants_service_connection
    ADD CONSTRAINT grants_service_connections_consumer_peer_id_fk FOREIGN KEY (consumer_peer_id) REFERENCES peers.peers(id) MATCH FULL ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: grants_service_connection grants_service_connections_service_peer_id_fk; Type: FK CONSTRAINT; Schema: contracts; Owner: -
--

ALTER TABLE ONLY contracts.grants_service_connection
    ADD CONSTRAINT grants_service_connections_service_peer_id_fk FOREIGN KEY (service_peer_id) REFERENCES peers.peers(id) MATCH FULL ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: grants_service_publication grants_service_publication_directory_peer_id_fk; Type: FK CONSTRAINT; Schema: contracts; Owner: -
--

ALTER TABLE ONLY contracts.grants_service_publication
    ADD CONSTRAINT grants_service_publication_directory_peer_id_fk FOREIGN KEY (directory_peer_id) REFERENCES peers.peers(id) MATCH FULL ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: grants_service_publication grants_service_publication_service_peer_id_fk; Type: FK CONSTRAINT; Schema: contracts; Owner: -
--

ALTER TABLE ONLY contracts.grants_service_publication
    ADD CONSTRAINT grants_service_publication_service_peer_id_fk FOREIGN KEY (service_peer_id) REFERENCES peers.peers(id) MATCH FULL ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: certificates certificates_peer_id_fk; Type: FK CONSTRAINT; Schema: peers; Owner: -
--

ALTER TABLE ONLY peers.certificates
    ADD CONSTRAINT certificates_peer_id_fk FOREIGN KEY (peer_id) REFERENCES peers.peers(id) MATCH FULL ON DELETE CASCADE;


--
-- PostgreSQL database dump complete
--

