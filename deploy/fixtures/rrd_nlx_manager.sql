--
-- PostgreSQL database dump
--

-- Dumped from database version 16.0 (Debian 16.0-1.pgdg110+1)
-- Dumped by pg_dump version 16.0 (Debian 16.0-1.pgdg110+1)

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: contracts; Type: SCHEMA; Schema: -; Owner: -
--

CREATE SCHEMA contracts;


--
-- Name: peers; Type: SCHEMA; Schema: -; Owner: -
--

CREATE SCHEMA peers;


--
-- Name: content_hash_algorithm; Type: TYPE; Schema: contracts; Owner: -
--

CREATE TYPE contracts.content_hash_algorithm AS ENUM (
    'sha3_512'
);


--
-- Name: signature_type; Type: TYPE; Schema: contracts; Owner: -
--

CREATE TYPE contracts.signature_type AS ENUM (
    'accept',
    'reject',
    'revoke'
);


SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- Name: content; Type: TABLE; Schema: contracts; Owner: -
--

CREATE TABLE contracts.content (
    hash character varying(150) NOT NULL,
    hash_algorithm contracts.content_hash_algorithm NOT NULL,
    id uuid NOT NULL,
    group_id character varying(255) NOT NULL,
    valid_not_before timestamp with time zone NOT NULL,
    valid_not_after timestamp with time zone NOT NULL,
    created_at timestamp with time zone NOT NULL
);


--
-- Name: contract_signatures; Type: TABLE; Schema: contracts; Owner: -
--

CREATE TABLE contracts.contract_signatures (
    content_hash character varying(150) NOT NULL,
    signature_type contracts.signature_type NOT NULL,
    peer_id character varying(20) NOT NULL,
    certificate_thumbprint character varying(512) NOT NULL,
    signature character varying(2048) NOT NULL,
    signed_at timestamp with time zone NOT NULL
);


--
-- Name: grants_delegated_service_connection; Type: TABLE; Schema: contracts; Owner: -
--

CREATE TABLE contracts.grants_delegated_service_connection (
    hash character varying(150) NOT NULL,
    content_hash character varying(150) NOT NULL,
    outway_peer_id character varying(20) NOT NULL,
    delegator_peer_id character varying(20) NOT NULL,
    service_peer_id character varying(20) NOT NULL,
    service_name character varying(255) NOT NULL,
    certificate_thumbprint character varying(512) NOT NULL
);


--
-- Name: grants_delegated_service_publication; Type: TABLE; Schema: contracts; Owner: -
--

CREATE TABLE contracts.grants_delegated_service_publication (
    hash character varying(150) NOT NULL,
    content_hash character varying(150) NOT NULL,
    directory_peer_id character varying(20) NOT NULL,
    delegator_peer_id character varying(20) NOT NULL,
    service_peer_id character varying(20) NOT NULL,
    service_name character varying(255) NOT NULL,
    service_protocol character varying(255) NOT NULL
);


--
-- Name: grants_peer_registration; Type: TABLE; Schema: contracts; Owner: -
--

CREATE TABLE contracts.grants_peer_registration (
    hash character varying(150) NOT NULL,
    content_hash character varying(150) NOT NULL,
    directory_peer_id character varying(20) NOT NULL,
    peer_id character varying(20) NOT NULL,
    peer_name character varying(255) NOT NULL
);


--
-- Name: grants_service_connection; Type: TABLE; Schema: contracts; Owner: -
--

CREATE TABLE contracts.grants_service_connection (
    hash character varying(150) NOT NULL,
    content_hash character varying(150) NOT NULL,
    consumer_peer_id character varying(20) NOT NULL,
    service_peer_id character varying(20) NOT NULL,
    service_name character varying(255) NOT NULL,
    certificate_thumbprint character varying(512) NOT NULL
);


--
-- Name: grants_service_publication; Type: TABLE; Schema: contracts; Owner: -
--

CREATE TABLE contracts.grants_service_publication (
    hash character varying(150) NOT NULL,
    content_hash character varying(150) NOT NULL,
    directory_peer_id character varying(20) NOT NULL,
    service_peer_id character varying(20) NOT NULL,
    service_name character varying(255) NOT NULL,
    service_protocol character varying(255) NOT NULL
);


--
-- Name: valid_contracts; Type: VIEW; Schema: contracts; Owner: -
--

CREATE VIEW contracts.valid_contracts AS
 SELECT hash
   FROM contracts.content content
  WHERE ((valid_not_before < now()) AND (valid_not_after > now()) AND (( SELECT count(*) AS count
           FROM contracts.contract_signatures s
          WHERE ((s.signature_type = 'reject'::contracts.signature_type) AND ((s.content_hash)::text = (content.hash)::text))) = 0) AND (( SELECT count(*) AS count
           FROM contracts.contract_signatures s
          WHERE ((s.signature_type = 'revoke'::contracts.signature_type) AND ((s.content_hash)::text = (content.hash)::text))) = 0) AND (( SELECT count(DISTINCT p.id) AS count
           FROM ( SELECT grants_peer_registration.directory_peer_id AS id
                   FROM contracts.grants_peer_registration
                  WHERE ((grants_peer_registration.content_hash)::text = (content.hash)::text)
                UNION
                 SELECT grants_peer_registration.peer_id AS id
                   FROM contracts.grants_peer_registration
                  WHERE ((grants_peer_registration.content_hash)::text = (content.hash)::text)
                UNION
                 SELECT grants_service_publication.directory_peer_id AS id
                   FROM contracts.grants_service_publication
                  WHERE ((grants_service_publication.content_hash)::text = (content.hash)::text)
                UNION
                 SELECT grants_service_publication.service_peer_id AS id
                   FROM contracts.grants_service_publication
                  WHERE ((grants_service_publication.content_hash)::text = (content.hash)::text)
                UNION
                 SELECT grants_service_connection.consumer_peer_id AS id
                   FROM contracts.grants_service_connection
                  WHERE ((grants_service_connection.content_hash)::text = (content.hash)::text)
                UNION
                 SELECT grants_service_connection.service_peer_id AS id
                   FROM contracts.grants_service_connection
                  WHERE ((grants_service_connection.content_hash)::text = (content.hash)::text)
                UNION
                 SELECT grants_service_connection.service_peer_id AS id
                   FROM contracts.grants_service_connection
                  WHERE ((grants_service_connection.content_hash)::text = (content.hash)::text)
                UNION
                 SELECT grants_delegated_service_connection.service_peer_id AS id
                   FROM contracts.grants_delegated_service_connection
                  WHERE ((grants_delegated_service_connection.content_hash)::text = (content.hash)::text)
                UNION
                 SELECT grants_delegated_service_connection.outway_peer_id AS id
                   FROM contracts.grants_delegated_service_connection
                  WHERE ((grants_delegated_service_connection.content_hash)::text = (content.hash)::text)
                UNION
                 SELECT grants_delegated_service_connection.delegator_peer_id AS id
                   FROM contracts.grants_delegated_service_connection
                  WHERE ((grants_delegated_service_connection.content_hash)::text = (content.hash)::text)
                UNION
                 SELECT grants_delegated_service_publication.service_peer_id AS id
                   FROM contracts.grants_delegated_service_publication
                  WHERE ((grants_delegated_service_publication.content_hash)::text = (content.hash)::text)
                UNION
                 SELECT grants_delegated_service_publication.directory_peer_id AS id
                   FROM contracts.grants_delegated_service_publication
                  WHERE ((grants_delegated_service_publication.content_hash)::text = (content.hash)::text)
                UNION
                 SELECT grants_delegated_service_publication.delegator_peer_id AS id
                   FROM contracts.grants_delegated_service_publication
                  WHERE ((grants_delegated_service_publication.content_hash)::text = (content.hash)::text)) p) = ( SELECT count(*) AS count
           FROM contracts.contract_signatures s
          WHERE ((s.signature_type = 'accept'::contracts.signature_type) AND ((s.content_hash)::text = (content.hash)::text)))));


--
-- Name: certificates; Type: TABLE; Schema: peers; Owner: -
--

CREATE TABLE peers.certificates (
    certificate_thumbprint character varying(512) NOT NULL,
    peer_id character varying(20) NOT NULL,
    certificate bytea NOT NULL
);


--
-- Name: peers; Type: TABLE; Schema: peers; Owner: -
--

CREATE TABLE peers.peers (
    id character varying(20) NOT NULL,
    name character varying(60),
    manager_address character varying(255)
);


--
-- Name: schema_migrations; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.schema_migrations (
    version bigint NOT NULL,
    dirty boolean NOT NULL
);


--
-- Data for Name: content; Type: TABLE DATA; Schema: contracts; Owner: -
--

COPY contracts.content (hash, hash_algorithm, id, group_id, valid_not_before, valid_not_after, created_at) FROM stdin;
$1$1$EvDjWbYqRoWOUArY8v64jjGoA4nbA4tniX-Saa3tk_GRZO_hlMJWYwNwXWOtgFTnQOVM9D6geIgxPbVVrivrWQ	sha3_512	018c7db8-a89a-7a5b-a6af-f2acfc332a08	fsc-demo	2023-12-18 00:00:00+00	2023-12-19 00:00:00+00	2023-12-18 16:18:23+00
$1$1$QH89503RLswUpsCJ-2ZNcwxrKl3ppiX643Apy-G0qU0qolWt4kV9aIKIzrfRenaSVrVneloBJHzJhTAyANlbVg	sha3_512	018c7db9-729f-7a5b-a47a-06719915377e	fsc-demo	2023-12-18 00:00:00+00	2023-12-19 00:00:00+00	2023-12-18 16:19:14+00
\.


--
-- Data for Name: contract_signatures; Type: TABLE DATA; Schema: contracts; Owner: -
--

COPY contracts.contract_signatures (content_hash, signature_type, peer_id, certificate_thumbprint, signature, signed_at) FROM stdin;
$1$1$EvDjWbYqRoWOUArY8v64jjGoA4nbA4tniX-Saa3tk_GRZO_hlMJWYwNwXWOtgFTnQOVM9D6geIgxPbVVrivrWQ	accept	01702331477921460562	r1KCu084pa7bEM1zQSCJe5pehX85Bi4_s7PVtpzNpos	eyJhbGciOiJSUzUxMiIsIng1dCNTMjU2IjoicjFLQ3UwODRwYTdiRU0xelFTQ0plNXBlaFg4NUJpNF9zN1BWdHB6TnBvcyJ9.eyJjb250ZW50SGFzaCI6IiQxJDEkRXZEaldiWXFSb1dPVUFyWTh2NjRqakdvQTRuYkE0dG5pWC1TYWEzdGtfR1JaT19obE1KV1l3TndYV090Z0ZUblFPVk05RDZnZUlneFBiVlZyaXZyV1EiLCJ0eXBlIjoiYWNjZXB0Iiwic2lnbmVkQXQiOjE3MDI5MTYzMDN9.StqNFIu6Fk7vXSPgRV5Fh1mW_NrpPuUjvGV0tak9eYF-WsnCPn8yZjgEcibTJD20tgoGMGo1ODa75Vdr_dRTeZKbeEemCgajsIpCySPecjnvc6ZUq4TvcYkyZhF6faX7FfXLeuViyvGoUtmKKme9gxxjcv9Dyfc6bFvwzTHTk8H0iHQN4mO4KR4PWiOPMx-nYHxVvEmablMED7cn7fR4SsKrPrk68I7b1-xsvvOe_knIoz1cWvAhqpLHgUWnwsIZquqJ2E4foMys9vPyxYrmAB52ryIgRPqkNf28ilnBwNoy0CWF0EIzvvUUnhM4bUjd88O3FiqAgwFwKj2kUwhxmi7m9uym-0h5iRFEsTWfmQTXpI8WdoNGQXXQ73Q1xg760nvt6ia50mpnTPr1Mdn86cQIVFBrOYehDBzoo8WB-Jt7B2p-E9w9bKPrKzTv9zJCfe5B-nhsFVvuJpGWO9SVJoERktvdd8aHHK5mrURBxeAWln_prudSr83zYF30JdpF4x9-XY5D5HQg4zYSraLkD4rG-TCLqhCbb78ef4qhM2hk7iiSls6-nMDh_dLX9Eh5bzXr6vIY9j9hrwK7Peu5SU7-eMmLgX_2WqFnn5eK37L-z3463USDq63T7YRW9Ceu0zA1C3NOEMwF_xq7-qZ4rAbOnTgfsrSRZEa5T_RKHbc	2023-12-18 16:18:23+00
$1$1$EvDjWbYqRoWOUArY8v64jjGoA4nbA4tniX-Saa3tk_GRZO_hlMJWYwNwXWOtgFTnQOVM9D6geIgxPbVVrivrWQ	accept	12345678901234567899	U8Q1EXydxE--72ESaGOMHy-4PJpJ9LA2lYdHuOPjJqk	eyJhbGciOiJSUzUxMiIsIng1dCNTMjU2IjoiVThRMUVYeWR4RS0tNzJFU2FHT01IeS00UEpwSjlMQTJsWWRIdU9QakpxayJ9.eyJjb250ZW50SGFzaCI6IiQxJDEkRXZEaldiWXFSb1dPVUFyWTh2NjRqakdvQTRuYkE0dG5pWC1TYWEzdGtfR1JaT19obE1KV1l3TndYV090Z0ZUblFPVk05RDZnZUlneFBiVlZyaXZyV1EiLCJ0eXBlIjoiYWNjZXB0Iiwic2lnbmVkQXQiOjE3MDI5MTYzMDN9.VTwFGERGOqP4r0YUsSftecQRrqfb20t-JmdH3TlrYZNyV_4-2fuYSKjJ-OTjnWHWCxcO6WDtTXLu9TZN5vBQIjE7LvNxz83GUa1D3WwCIKcq_NiWRcNeeQ4DgiF6FQimDMnO7lGW02HiVnepxVABL9rCOIkSPP6lov-_MnSbxZyPT77d6EDtNSuGKN9qXfX3SlKMSkoCzsHZdbTo3Jq7dz6JWxskB4LYsYfOLBadEF80zHwSKuQc2mn1kMbAPahuJz0YscD20OoVd_WnffCb4bO7NqGxsmXbivybK3iGCXjTJJr1EOT_g_i5iEU6NF21msUd7T9An6TAG0nR92Xg3U9W5mfnMshxnKblGXbz7V8cu8Uo7z-r79QYeVLi2Wa0XJjD09CjoT11YZ3lJ0n98iaB8AYePz09KU8E0Myj5RC-tANBGXXh6b2nsXJzNNc5Km5Jtzu6NwlCS0VEvMJ2G0TEqJiDKNmWuZ8dnRtiMsNtTcrOpjqL8f6gb1pp0aHteoOuECBSrHv7vZJVQHmt1F_ZLK6tAJJgxC47x6hi9PGFLtWkHv3FYWenJdOUDyGeH1TdaKBZfMN58nvYnXxImRuCE25jF95aA5fWxGIhDcpl5tSVc3Qu0ATh6TB3TbRJwnHZdI3MCzBSMmhlqBihvzgus4-_4eqM0m2x9QU9xRQ	2023-12-18 16:18:23.210111+00
$1$1$QH89503RLswUpsCJ-2ZNcwxrKl3ppiX643Apy-G0qU0qolWt4kV9aIKIzrfRenaSVrVneloBJHzJhTAyANlbVg	accept	01702331477921460562	r1KCu084pa7bEM1zQSCJe5pehX85Bi4_s7PVtpzNpos	eyJhbGciOiJSUzUxMiIsIng1dCNTMjU2IjoicjFLQ3UwODRwYTdiRU0xelFTQ0plNXBlaFg4NUJpNF9zN1BWdHB6TnBvcyJ9.eyJjb250ZW50SGFzaCI6IiQxJDEkUUg4OTUwM1JMc3dVcHNDSi0yWk5jd3hyS2wzcHBpWDY0M0FweS1HMHFVMHFvbFd0NGtWOWFJS0l6cmZSZW5hU1ZyVm5lbG9CSkh6SmhUQXlBTmxiVmciLCJ0eXBlIjoiYWNjZXB0Iiwic2lnbmVkQXQiOjE3MDI5MTYzNTR9.QY_FeLQJb46Pf5fzPJexv2TH2uWr-b4PV2ZQhshzl3BtqAvwJUCkywg2ChuzHNLe73YFZIE5BaVPa_LewmYUbNP8JRB96n-6JvQ7YdNn0-8KZzDx3AT7CAvGiqtNOO_L3rT9Fufa_XH2L1Fy_J7LPtcQL_u2QE82GXlD9TXI9Ol1hCocCPC7IOVOBtXdE6kD6rnisLRkFqiWkc3aNAq2MMUo1t-jdn-IATJidB-9zU2DpbJRhl0POcyO3ASi6DeroWahzfi7oTfrT0foa8RDXXZmO5O57yeV0wcYffJ4e9g8qvd9dBzOGkyK9dVP3YFNPXMzUKpGxehnTqZ_hCUK4zq-YSyLviZxq_cGq9ciol1EOqKegYe8nCXsDFrEC4qB7N2Q-G5yykj9nm5nyoForoA4HK6G6JqQxlh_SawSlnsBbdSzuOL5fD_TGy0RCTZiyMeoelBgA3oAmXsT0-36TfowrTQli-6aUjTEHsGpR6rRjpj8Olf7jrb6toDHrU7LlsJ_oPzgXEMsz2l5JJE-E9-RmGW-zJXQqVyAPQC-Mc0Vj966EEBv5I2kIqH1_Q4zXgHYzO8M8z4KdAo2sUdpRNklHLxeVxJpmhu81ElQEQiDhODU82iYpEr8Und0EWiCO-05dQ59oHWXJ_Q40CstJoFxTSYI9o0YobHempZPUMI	2023-12-18 16:19:14+00
$1$1$QH89503RLswUpsCJ-2ZNcwxrKl3ppiX643Apy-G0qU0qolWt4kV9aIKIzrfRenaSVrVneloBJHzJhTAyANlbVg	accept	12345678901234567899	U8Q1EXydxE--72ESaGOMHy-4PJpJ9LA2lYdHuOPjJqk	eyJhbGciOiJSUzUxMiIsIng1dCNTMjU2IjoiVThRMUVYeWR4RS0tNzJFU2FHT01IeS00UEpwSjlMQTJsWWRIdU9QakpxayJ9.eyJjb250ZW50SGFzaCI6IiQxJDEkUUg4OTUwM1JMc3dVcHNDSi0yWk5jd3hyS2wzcHBpWDY0M0FweS1HMHFVMHFvbFd0NGtWOWFJS0l6cmZSZW5hU1ZyVm5lbG9CSkh6SmhUQXlBTmxiVmciLCJ0eXBlIjoiYWNjZXB0Iiwic2lnbmVkQXQiOjE3MDI5MTYzNTR9.fUse1Eiw58lFqpO7ruKZLZ_8ct-zC7I1IPQgEmDQBjNxpclETeMpDSH1-kDnh4OyhQtdnZaWvtyZJJQ0ZY7aSBM8d41DuMg5yjA1FutAp85ozcj63xFjm0_ZzGgPs46IwSO-XPfGsaswxnoyiymQzdUIR_GDgBBelf41mndCjarA9gg7Ew6pDbvppGfJIQan5OdrlcfI_Lh1YDZ36e9R00WfvW3YSMUDW_P6hlbz08sKdAKRnxXQfCWM7DJlD91NTnPXSGNc9UHmwWz__puqZ15wNxIgw0ez7JIYHMrC8xgEVB8fRgC3KA6wNAIU7fGGrThm17sFLZXQPHdmxTE-5m2_RlS7FDLazfYcRLTQdJ9z-YZXBzMZDCpkE7omGw7Fs1n3KbjtSenCYeBl69Zi6qgxcqjwGWZ5kwrpq3_U57QoHOIQ4fJNX71-HeKF1Q56vUTcoAo7i2wjt5RqL_qEBVoI_HW9-vYMyWICi97sqMyPVqM3L8yqH6q4jE51GJwfQeT5Hygqjq9zqlbdviAcVlwSR4RY97nn15QF3-uNH0D6jKG4b5eOsB4PrzMO9ia4RXIkBGGAb1xG9rNUMAWMXkjnj-0UDnXzMPKJLWAEiJ2lBCNpEig5D-Lnzg65BFE5YBB4SDYAT7uKbIdcZQXe1kkUXr113bJ_BjSWeWWU82Q	2023-12-18 16:19:14.818231+00
\.


--
-- Data for Name: grants_delegated_service_connection; Type: TABLE DATA; Schema: contracts; Owner: -
--

COPY contracts.grants_delegated_service_connection (hash, content_hash, outway_peer_id, delegator_peer_id, service_peer_id, service_name, certificate_thumbprint) FROM stdin;
\.


--
-- Data for Name: grants_delegated_service_publication; Type: TABLE DATA; Schema: contracts; Owner: -
--

COPY contracts.grants_delegated_service_publication (hash, content_hash, directory_peer_id, delegator_peer_id, service_peer_id, service_name, service_protocol) FROM stdin;
\.


--
-- Data for Name: grants_peer_registration; Type: TABLE DATA; Schema: contracts; Owner: -
--

COPY contracts.grants_peer_registration (hash, content_hash, directory_peer_id, peer_id, peer_name) FROM stdin;
\.


--
-- Data for Name: grants_service_connection; Type: TABLE DATA; Schema: contracts; Owner: -
--

COPY contracts.grants_service_connection (hash, content_hash, consumer_peer_id, service_peer_id, service_name, certificate_thumbprint) FROM stdin;
\.


--
-- Data for Name: grants_service_publication; Type: TABLE DATA; Schema: contracts; Owner: -
--

COPY contracts.grants_service_publication (hash, content_hash, directory_peer_id, service_peer_id, service_name, service_protocol) FROM stdin;
$1$3$_FtcXyzQul0kOkHneXmR4wWzje0LxjHLzW9rznsvalsuy1jHUT-Pro28_8-uPqGEMMCCnuDBH7Os-UzZ7i_opA	$1$1$EvDjWbYqRoWOUArY8v64jjGoA4nbA4tniX-Saa3tk_GRZO_hlMJWYwNwXWOtgFTnQOVM9D6geIgxPbVVrivrWQ	12345678901234567899	01702331477921460562	fictief-np-bvv-backend	PROTOCOL_TCP_HTTP_1.1
$1$3$lEtd2H2RlJjwt2f0VUnvMqwCHqS5_9N8cydBs2XtV2EpXf4gtXpcSCJs36zsXVxpHJcCFH0nkz4nzCjxlsQnDw	$1$1$QH89503RLswUpsCJ-2ZNcwxrKl3ppiX643Apy-G0qU0qolWt4kV9aIKIzrfRenaSVrVneloBJHzJhTAyANlbVg	12345678901234567899	01702331477921460562	fictief-np-sigma-backend	PROTOCOL_TCP_HTTP_1.1
\.


--
-- Data for Name: certificates; Type: TABLE DATA; Schema: peers; Owner: -
--

COPY peers.certificates (certificate_thumbprint, peer_id, certificate) FROM stdin;
U8Q1EXydxE--72ESaGOMHy-4PJpJ9LA2lYdHuOPjJqk	12345678901234567899	\\x3082066130820449a00302010202142bf0ba0903a66c72c8b86ee300e9207fae8ee227300d06092a864886f70d01010d0500305f310b3009060355040613024e4c311630140603550408130d4e6f6f72642d486f6c6c616e643112301006035504071309416d7374657264616d31163014060355040a130d436f6d6d6f6e2047726f756e64310c300a060355040b13034e4c58301e170d3233313231313230303130305a170d3236313231303230303130305a3081a8310b3009060355040613024e4c3110300e06035504081307557472656368743110300e060355040713075574726563687431273025060355040a131e72696a6b736469656e73742d7265616c69737469736368652d64656d6f73312d302b060355040313246d616e616765722d6673632d6e6c782d6d616e616765722d65787465726e616c2e727264311d301b06035504051314313233343536373839303132333435363738393930820222300d06092a864886f70d01010105000382020f003082020a0282020100a86e510c06ecffb77972afb65a155bb55b129f5d2bac39288d1e211190427d485e1ae6e73d77773e18282087d817cf66b8c3ea9a35a1891fb9ee0dd0c9c5b19977101f85c9927506c045aca724abdf8c823916f14cdfe5672b182950996dcc9bbb796af5f623ec57c78aaa72bce244d95268c8c5c2900e841ffaf9a8a1520f44f2a38a5882e7be5eb26c44a12c05628d9dd1329470c0df4ce28d60a15b6122afc98cff0e44f3394b2f0156114f9f11a5d9f76ffa061f03a797cd3abdf65fd3859d52cd8e431308db9d2e7a7b97b8e20b10a482285f35c8a970d4c6cd3bbddd0b9121ca3c5a330d8888edadeae32fbb63a8e324d520a2562de63a02c62677e1d8adf875edffbad07084dcc1fd175c0e2702b0a7f556d781ce58f4cbc3f24d458bbcd52ad07b5ce7d89a317a14235ac0221d70a66b1fbc6fa3154aa5e481dd3417c6d1474db45b10eebd4c3fa30e4d370510d5af69326011639902b234d6b68e228bd45170b25cfdd021bbc7cd6e64f1f72be9908a52f9ae2451d439aadae564f756d7a034bfdb7d4aee53ddb7e6bb626239a2a33b8b1519fdae1ec4529cd1dabeb467ae7c1c97c3eb0f751a97830fcf3ee1f178189e04846c1f07fd746074282ea7f46015e3325c5cd78307f4ef242a10a4d427a886d97f043e430e42a3f0050b9b2e7672ff9d8f92e393513f5949727a26b0ad993c525186cdbf8e0b729c76e70203010001a381ca3081c7300e0603551d0f0101ff0404030205a0301d0603551d250416301406082b0601050507030106082b06010505070302300c0603551d130101ff04023000301d0603551d0e041604147c494af41e3480d4cfd407b0bea19c528c4e04a7301f0603551d230418301680145ae041511187acda6887c000c3065ce2792d89fd30480603551d110441303f82246d616e616765722d6673632d6e6c782d6d616e616765722d65787465726e616c2e7272648217696e7761792d6673632d6e6c782d696e7761792e727264300d06092a864886f70d01010d050003820201007dc219c797b930ff772e52f740fd325478b854d1577700b647c02f3aeec9c4bda9d7fb2f87af100fb97e72d1e80f978300c45f0c90226b1f554dbc2e1c91d9c2a1205373dab6a18c251d81e20efd4c569a5f73caacbf4589af156157af771f1a0fcad068d6c1344c798acb9f0c188214e158c1a8a221ba11bc87b026d5baf7d058df6cf5f489780ea64fe8ea265c4ed39e5da8f21d43a2735004b6f0b3dafab158e67f71f71df96d25ec95fabc1fd8b5063001a166506c366843069b03063781c53778ef6437ae5a6f4163e4cfc5f1713242c69cf23a624dcbab87e783ef7617004d986c404436bb144ddabd56eb0b1802193470fd26748e7805709e10e275d01e839e8c7945486a03a762debe5991052abefbf0062420224bc988f72e482a08207b17737fff868f467afbd3af6ed1bf92d206b5f2ed1a9688fe1385729d86ab6bb903c4e4b742ece646c47ec106563ed5880f19e52065f7a69ed75ba2ad135f1b80c49772cf468f542ed4ac51294f9e6a9c2889b32b063ea96403bae416a741cdd71f6c461f94ae88cb458b7355e8c6b1e60f69a5788b5f9411d26dbf20ec25b475b2ed138faee5576fd96a551813512425f3d6cc5bec4ec7392200476c2fdc9c2ad0fe6da05de2c8951af260cfe0c40a655dd54cc45d47a95e8dc290fadba0a1ec0f3c4ad5294b9588fe3adbaf34385a9aef086f7d05c7d6170b3d46d8b712
r1KCu084pa7bEM1zQSCJe5pehX85Bi4_s7PVtpzNpos	01702331477921460562	\\x308206673082044fa003020102021415a4f7f10bcb2f410ad97eaa863de64e4d999009300d06092a864886f70d01010d0500305f310b3009060355040613024e4c311630140603550408130d4e6f6f72642d486f6c6c616e643112301006035504071309416d7374657264616d31163014060355040a130d436f6d6d6f6e2047726f756e64310c300a060355040b13034e4c58301e170d3233313231313231343630305a170d3236313231303231343630305a30819a310b3009060355040613024e4c3110300e06035504081307557472656368743110300e0603550407130755747265636874310f300d060355040a1306736861726564313730350603550403132e6d616e616765722d6673632d6e6c782d6d616e616765722d65787465726e616c2e6d696772617469656b6574656e311d301b06035504051314303137303233333134373739323134363035363230820222300d06092a864886f70d01010105000382020f003082020a0282020100dba78a07eac843512a2b82df33c30f56bde128ff35b5cd6ef6ff02183515eb22460b8672d535a335489d4c96b5ccf69a0f273c3eddbf8cc7914f3f7404e43f94c5031bc7623984407b545f98ddee80f398b595a39c425f56b5f95cc2551b56f7a9dde01d33422bb93450858732048f2de770af2c95bbe8beac93c1621e40db9178ed1f7befa11191131609a1e87072cc251a0e073eb0fbf262665b1c13eb913231e893b3a5ccfce235934bbed108f10b98dbe92bbecb71e1133a3c9115b1f2727c5b823e8a62e1522392bcba52141d7a66e7e589ff2e7296364d0341343f9dbbab1aeb536d6bfd21b022c0fd5771c286752b79434d6780fe342b12f197c44cf074380605bf26d6c5df8b022ba2a889baba11ca4b1a785366b86deb6911e71f2e11516653f42e0a3f5b568ba355f6c89bb41b9ec54149dd849ad76c466b5517bb39f0f4b0d90b8f5351c6acf1520113abe0eaa9f540d2c9a2b14e80a41f6182fa8783de71a1989f6a84eabe24416afa009442f4bffa57f1bbdfaa4cf2401c1a9037baf60977f66af98a6ead7dc4f677f923e783135f9106a6ec1ea71d6cafd846640e3f7494686125e996c5d4c56db7cdd7afbc9d6f971122d8da82631859caa4ce4009327901863aa88c94379906ae6424b1b94a1e239ce916b406b4263dec3fa8c8ddb3512b65be8f6130aa4d8c5ac926ab91ca65e69920203bdd569c10464b0203010001a381de3081db300e0603551d0f0101ff0404030205a0301d0603551d250416301406082b0601050507030106082b06010505070302300c0603551d130101ff04023000301d0603551d0e041604143826a7472b7e3d568ea1774ea93807baf407acdc301f0603551d230418301680145ae041511187acda6887c000c3065ce2792d89fd305c0603551d1104553053822e6d616e616765722d6673632d6e6c782d6d616e616765722d65787465726e616c2e6d696772617469656b6574656e8221696e7761792d6673632d6e6c782d696e7761792e6d696772617469656b6574656e300d06092a864886f70d01010d050003820201009b2d941e1841446a937132c9bc44ed85a31836f990adfb296b861e5aa0d4b51ffc146ea83b64d0df751a35943b849b3f6ff8c67978897f5fd992bc782748134da4075f405e6cb249d39e2bcd9f8b59b10a5735aff0e9ae6d918f8634dec777d3c80c31d075a9939a89f33351460c0bd35bbe5e5e3460d26ff94d084d324e60316cd2b6b716543b6bba61d3f8aa18084e4cab92952275bc81a92d215f086b31c0c3dacce8a85b3c4187686deda0a3277ca0abfada672d6a6f5a02771954147990d817dfba285c96a3a443c71e052a1617c951b20a3e349c64cc3f8d2e055e7baca485356be40599348f8cb9def5707bb386502a051fa69ee942aee06c5010f9652887783706d57af033c93de6375797936e05ac52d5c018fa9398851e5a590dc141e451e101e7f592f0ad01ba6d462444e205ce7b25411a9a17ed51914e5ddad17ac246ae7bfe6c649a2e1160c6784df8be5111652063d605c12c018d0ac874b1a71151b72efce6a6a84a77c607f2f271277eb8dca5395baf183a482df3fd3cf3123ab705818dd02de80af5ff8fb53dfcc3fc87a383459a3dd80c77189f8f21f7ca40d7c44c28588b7b48e1e6e7d170507a937669440a909d18c23644c2d4734f392a994df803ccfb204decf9ddd8d9b325231bc3ec949b218daff99b4486e4223c05ca5e2a53ec498873a9ef14a5b437285c395b17600d92fccbab57057ca108
\.


--
-- Data for Name: peers; Type: TABLE DATA; Schema: peers; Owner: -
--

COPY peers.peers (id, name, manager_address) FROM stdin;
12345678901234567899	rijksdienst-realistische-demos	https://manager-fsc-nlx-manager-external.rrd:8443
01702331477921460562	shared	https://manager-fsc-nlx-manager-external.migratieketen:8443
\.


--
-- Data for Name: schema_migrations; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.schema_migrations (version, dirty) FROM stdin;
1	f
\.


--
-- Name: content content_pk; Type: CONSTRAINT; Schema: contracts; Owner: -
--

ALTER TABLE ONLY contracts.content
    ADD CONSTRAINT content_pk PRIMARY KEY (hash);


--
-- Name: contract_signatures contract_signatures_pk; Type: CONSTRAINT; Schema: contracts; Owner: -
--

ALTER TABLE ONLY contracts.contract_signatures
    ADD CONSTRAINT contract_signatures_pk PRIMARY KEY (content_hash, signature_type, peer_id);


--
-- Name: grants_delegated_service_connection grants_delegated_service_connection_pk; Type: CONSTRAINT; Schema: contracts; Owner: -
--

ALTER TABLE ONLY contracts.grants_delegated_service_connection
    ADD CONSTRAINT grants_delegated_service_connection_pk PRIMARY KEY (hash);


--
-- Name: grants_delegated_service_publication grants_delegated_service_publication_pk; Type: CONSTRAINT; Schema: contracts; Owner: -
--

ALTER TABLE ONLY contracts.grants_delegated_service_publication
    ADD CONSTRAINT grants_delegated_service_publication_pk PRIMARY KEY (hash);


--
-- Name: grants_peer_registration grants_peer_registration_pk; Type: CONSTRAINT; Schema: contracts; Owner: -
--

ALTER TABLE ONLY contracts.grants_peer_registration
    ADD CONSTRAINT grants_peer_registration_pk PRIMARY KEY (hash);


--
-- Name: grants_service_connection grants_service_connection_pk; Type: CONSTRAINT; Schema: contracts; Owner: -
--

ALTER TABLE ONLY contracts.grants_service_connection
    ADD CONSTRAINT grants_service_connection_pk PRIMARY KEY (hash);


--
-- Name: grants_service_publication grants_service_publication_pk; Type: CONSTRAINT; Schema: contracts; Owner: -
--

ALTER TABLE ONLY contracts.grants_service_publication
    ADD CONSTRAINT grants_service_publication_pk PRIMARY KEY (hash);


--
-- Name: certificates certificates_pk; Type: CONSTRAINT; Schema: peers; Owner: -
--

ALTER TABLE ONLY peers.certificates
    ADD CONSTRAINT certificates_pk PRIMARY KEY (certificate_thumbprint);


--
-- Name: peers peers_pk; Type: CONSTRAINT; Schema: peers; Owner: -
--

ALTER TABLE ONLY peers.peers
    ADD CONSTRAINT peers_pk PRIMARY KEY (id);


--
-- Name: schema_migrations schema_migrations_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.schema_migrations
    ADD CONSTRAINT schema_migrations_pkey PRIMARY KEY (version);


--
-- Name: grants_delegated_service_connection_content_hash_idx; Type: INDEX; Schema: contracts; Owner: -
--

CREATE INDEX grants_delegated_service_connection_content_hash_idx ON contracts.grants_delegated_service_connection USING btree (content_hash);


--
-- Name: grants_delegated_service_connection_delegator_peer_id_idx; Type: INDEX; Schema: contracts; Owner: -
--

CREATE INDEX grants_delegated_service_connection_delegator_peer_id_idx ON contracts.grants_delegated_service_connection USING btree (delegator_peer_id);


--
-- Name: grants_delegated_service_connection_outway_peer_id_idx; Type: INDEX; Schema: contracts; Owner: -
--

CREATE INDEX grants_delegated_service_connection_outway_peer_id_idx ON contracts.grants_delegated_service_connection USING btree (outway_peer_id);


--
-- Name: grants_delegated_service_connection_service_peer_id_idx; Type: INDEX; Schema: contracts; Owner: -
--

CREATE INDEX grants_delegated_service_connection_service_peer_id_idx ON contracts.grants_delegated_service_connection USING btree (service_peer_id);


--
-- Name: grants_peer_registration_directory_peer_id_idx; Type: INDEX; Schema: contracts; Owner: -
--

CREATE INDEX grants_peer_registration_directory_peer_id_idx ON contracts.grants_peer_registration USING btree (directory_peer_id);


--
-- Name: grants_peer_registration_peer_id_idx; Type: INDEX; Schema: contracts; Owner: -
--

CREATE INDEX grants_peer_registration_peer_id_idx ON contracts.grants_peer_registration USING btree (peer_id);


--
-- Name: grants_service_connection_consumer_peer_id_idx; Type: INDEX; Schema: contracts; Owner: -
--

CREATE INDEX grants_service_connection_consumer_peer_id_idx ON contracts.grants_service_connection USING btree (consumer_peer_id);


--
-- Name: grants_service_connection_content_hash_idx; Type: INDEX; Schema: contracts; Owner: -
--

CREATE INDEX grants_service_connection_content_hash_idx ON contracts.grants_service_connection USING btree (content_hash);


--
-- Name: grants_service_connection_service_peer_id_idx; Type: INDEX; Schema: contracts; Owner: -
--

CREATE INDEX grants_service_connection_service_peer_id_idx ON contracts.grants_service_connection USING btree (service_peer_id);


--
-- Name: grants_service_publication_directory_peer_id_idx; Type: INDEX; Schema: contracts; Owner: -
--

CREATE INDEX grants_service_publication_directory_peer_id_idx ON contracts.grants_service_publication USING btree (directory_peer_id);


--
-- Name: grants_service_publication_service_peer_id_idx; Type: INDEX; Schema: contracts; Owner: -
--

CREATE INDEX grants_service_publication_service_peer_id_idx ON contracts.grants_service_publication USING btree (service_peer_id);


--
-- Name: certificates_certificate_thumbprint_idx; Type: INDEX; Schema: peers; Owner: -
--

CREATE INDEX certificates_certificate_thumbprint_idx ON peers.certificates USING btree (certificate_thumbprint);


--
-- Name: certificates_peer_id_idx; Type: INDEX; Schema: peers; Owner: -
--

CREATE INDEX certificates_peer_id_idx ON peers.certificates USING btree (peer_id);


--
-- Name: contract_signatures contract_signatures_content_hash_fk; Type: FK CONSTRAINT; Schema: contracts; Owner: -
--

ALTER TABLE ONLY contracts.contract_signatures
    ADD CONSTRAINT contract_signatures_content_hash_fk FOREIGN KEY (content_hash) REFERENCES contracts.content(hash) MATCH FULL ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: contract_signatures contract_signatures_peer_id_fk; Type: FK CONSTRAINT; Schema: contracts; Owner: -
--

ALTER TABLE ONLY contracts.contract_signatures
    ADD CONSTRAINT contract_signatures_peer_id_fk FOREIGN KEY (peer_id) REFERENCES peers.peers(id) MATCH FULL ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: grants_delegated_service_connection grants_delegated_service_connection_content_hash_fk; Type: FK CONSTRAINT; Schema: contracts; Owner: -
--

ALTER TABLE ONLY contracts.grants_delegated_service_connection
    ADD CONSTRAINT grants_delegated_service_connection_content_hash_fk FOREIGN KEY (content_hash) REFERENCES contracts.content(hash) MATCH FULL ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: grants_delegated_service_connection grants_delegated_service_connection_delegator_peer_id_fk; Type: FK CONSTRAINT; Schema: contracts; Owner: -
--

ALTER TABLE ONLY contracts.grants_delegated_service_connection
    ADD CONSTRAINT grants_delegated_service_connection_delegator_peer_id_fk FOREIGN KEY (delegator_peer_id) REFERENCES peers.peers(id) MATCH FULL ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: grants_delegated_service_connection grants_delegated_service_connection_outway_peer_id_fk; Type: FK CONSTRAINT; Schema: contracts; Owner: -
--

ALTER TABLE ONLY contracts.grants_delegated_service_connection
    ADD CONSTRAINT grants_delegated_service_connection_outway_peer_id_fk FOREIGN KEY (outway_peer_id) REFERENCES peers.peers(id) MATCH FULL ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: grants_delegated_service_connection grants_delegated_service_connection_service_peer_id_fk; Type: FK CONSTRAINT; Schema: contracts; Owner: -
--

ALTER TABLE ONLY contracts.grants_delegated_service_connection
    ADD CONSTRAINT grants_delegated_service_connection_service_peer_id_fk FOREIGN KEY (service_peer_id) REFERENCES peers.peers(id) MATCH FULL ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: grants_peer_registration grants_peer_registration_directory_peer_id_fk; Type: FK CONSTRAINT; Schema: contracts; Owner: -
--

ALTER TABLE ONLY contracts.grants_peer_registration
    ADD CONSTRAINT grants_peer_registration_directory_peer_id_fk FOREIGN KEY (directory_peer_id) REFERENCES peers.peers(id) MATCH FULL ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: grants_peer_registration grants_peer_registration_peer_id_fk; Type: FK CONSTRAINT; Schema: contracts; Owner: -
--

ALTER TABLE ONLY contracts.grants_peer_registration
    ADD CONSTRAINT grants_peer_registration_peer_id_fk FOREIGN KEY (peer_id) REFERENCES peers.peers(id) MATCH FULL ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: grants_service_connection grants_service_connection_content_hash_fk; Type: FK CONSTRAINT; Schema: contracts; Owner: -
--

ALTER TABLE ONLY contracts.grants_service_connection
    ADD CONSTRAINT grants_service_connection_content_hash_fk FOREIGN KEY (content_hash) REFERENCES contracts.content(hash) MATCH FULL ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: grants_service_connection grants_service_connections_consumer_peer_id_fk; Type: FK CONSTRAINT; Schema: contracts; Owner: -
--

ALTER TABLE ONLY contracts.grants_service_connection
    ADD CONSTRAINT grants_service_connections_consumer_peer_id_fk FOREIGN KEY (consumer_peer_id) REFERENCES peers.peers(id) MATCH FULL ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: grants_service_connection grants_service_connections_service_peer_id_fk; Type: FK CONSTRAINT; Schema: contracts; Owner: -
--

ALTER TABLE ONLY contracts.grants_service_connection
    ADD CONSTRAINT grants_service_connections_service_peer_id_fk FOREIGN KEY (service_peer_id) REFERENCES peers.peers(id) MATCH FULL ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: grants_service_publication grants_service_publication_directory_peer_id_fk; Type: FK CONSTRAINT; Schema: contracts; Owner: -
--

ALTER TABLE ONLY contracts.grants_service_publication
    ADD CONSTRAINT grants_service_publication_directory_peer_id_fk FOREIGN KEY (directory_peer_id) REFERENCES peers.peers(id) MATCH FULL ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: grants_service_publication grants_service_publication_service_peer_id_fk; Type: FK CONSTRAINT; Schema: contracts; Owner: -
--

ALTER TABLE ONLY contracts.grants_service_publication
    ADD CONSTRAINT grants_service_publication_service_peer_id_fk FOREIGN KEY (service_peer_id) REFERENCES peers.peers(id) MATCH FULL ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: certificates certificates_peer_id_fk; Type: FK CONSTRAINT; Schema: peers; Owner: -
--

ALTER TABLE ONLY peers.certificates
    ADD CONSTRAINT certificates_peer_id_fk FOREIGN KEY (peer_id) REFERENCES peers.peers(id) MATCH FULL ON DELETE CASCADE;


--
-- PostgreSQL database dump complete
--

